#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Full Modem
# Generated: Thu Sep 26 22:09:29 2019
##################################################

from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import fft
from gnuradio import gr
from gnuradio import uhd
from gnuradio.eng_option import eng_option
from gnuradio.fft import window
from gnuradio.filter import firdes
from optparse import OptionParser
import ettus
import marmote
import marmote3
import math
import numpy as np
import random


class full_modem(gr.top_block):

    def __init__(self, center_freq=1e9, cpu_fifo=65536, enable_recording=0, invert_spectrum=0, lo_offset=42e6, num_channels=32, radio_id=-1, rfnoc_fifo=4*65536, rx_gain=24, samp_rate=50000000, tx_gain=24, tx_power=0):
        gr.top_block.__init__(self, "Full Modem")

        ##################################################
        # Parameters
        ##################################################
        self.center_freq = center_freq
        self.cpu_fifo = cpu_fifo
        self.enable_recording = enable_recording
        self.invert_spectrum = invert_spectrum
        self.lo_offset = lo_offset
        self.num_channels = num_channels
        self.radio_id = radio_id
        self.rfnoc_fifo = rfnoc_fifo
        self.rx_gain = rx_gain
        self.samp_rate = samp_rate
        self.tx_gain = tx_gain
        self.tx_power = tx_power

        ##################################################
        # Variables
        ##################################################
        self.max_data_len = max_data_len = 1506
        self.max_code_len = max_code_len = 2 * max_data_len
        self.padding_len = padding_len = 8
        self.max_symb_len = max_symb_len = 8 * max_code_len
        self.fbmc_fft_len = fbmc_fft_len = int(num_channels * 5 / 4)
        self.chirp_len = chirp_len = 128
        self.subcarrier_map = subcarrier_map = range(fbmc_fft_len-num_channels/2, fbmc_fft_len) + range(0, num_channels/2)
        self.max_frame_len = max_frame_len = 40 + padding_len + chirp_len + padding_len + max_symb_len
        self.fbmc_tx_stride = fbmc_tx_stride = {16: 22, 20: 28, 24: 34, 32: 46, 40: 58}[num_channels]
        self.chirp = chirp = marmote3.get_chirp_taps(chirp_len)
        self.device3 = variable_uhd_device3_0 = ettus.device3(uhd.device_addr_t( ",".join(('type=x300', "")) ))
        self.modem_monitor = marmote3.modem_monitor(1, "tcp://127.0.0.1:7557", 2, "tcp://127.0.0.1:7555")
        self.subcarrier_inv = subcarrier_inv = [subcarrier_map.index(x) if x in subcarrier_map else -1 for x in range(fbmc_fft_len)]
        self.spectrogram_rate = spectrogram_rate = 1000
        self.spectrogram_fft = spectrogram_fft = 256
        self.postamble = postamble = True
        self.max_frame_time = max_frame_time = float(max_frame_len) / (samp_rate / fbmc_tx_stride)
        self.fbmc_tx_taps = fbmc_tx_taps = marmote3.get_fbmc_taps8(fbmc_fft_len, fbmc_tx_stride, "tx")
        self.fbmc_rx_taps = fbmc_rx_taps = marmote3.get_fbmc_taps8(fbmc_fft_len, fbmc_tx_stride, "rx")
        self.fbmc_rx_stride = fbmc_rx_stride = fbmc_tx_stride/2
        self.dc_chirp = dc_chirp = chirp + np.ones(chirp_len) * 0.5
        self.chirp_fft_len = chirp_fft_len = 64
        self.active_channels = active_channels = random.sample(range(num_channels), 2)

        ##################################################
        # Blocks
        ##################################################
        self.z1_modem_sender = marmote3.modem_sender2(max_data_len, max_code_len, max_symb_len, 5000, 1, 4.0, 5e5, False, 4, radio_id, (1534,1000), 2, False, False)
        self.z1_modem_sender.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.vector_peak_probe_synt = marmote3.vector_peak_probe(fbmc_fft_len, 2, 15, -30.0, 2.0)
        self.vector_peak_probe_synt.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.vector_peak_probe_chan = marmote3.vector_peak_probe(fbmc_fft_len, 1, 30, -60.0, 2.0)
        self.vector_peak_probe_chan.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.msg_connect((self.modem_monitor, 'unused'), (self.modem_monitor, 'unused'))
        self.synt_subcarrier_remapper = marmote3.subcarrier_remapper(num_channels, (subcarrier_inv), 32)
        self.synt_polyphase_synt_filter = marmote3.polyphase_synt_filter_ccf(fbmc_fft_len, (fbmc_tx_taps), fbmc_tx_stride or fbmc_fft_len)
        self.synt_polyphase_synt_filter.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        (self.synt_polyphase_synt_filter).set_min_output_buffer(8192)
        self.synt_polyphase_rotator = marmote3.polyphase_rotator_vcc(fbmc_fft_len, (subcarrier_map), fbmc_tx_stride)
        self.synt_fft = fft.fft_vcc(fbmc_fft_len, False, (), False, 1)
        self.subcarrier_serializer2 = marmote3.subcarrier_serializer2(num_channels, 2 * chirp_len / chirp_fft_len, 12.0, 2 * (padding_len + 40 + padding_len + chirp_len) + 3, 2 * (padding_len + 40 + padding_len) + 1, postamble, 2 * (padding_len + 40 + padding_len + chirp_len + padding_len), 2 * (padding_len + max_frame_len + padding_len), (range(num_channels)))
        self.subcarrier_serializer2.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.subcarrier_allocator = marmote3.subcarrier_allocator(num_channels, (active_channels), (), 3, getattr(self, '') if '' else marmote3.vector_peak_probe_sptr(), (), getattr(self, 'z1_modem_sender') if 'z1_modem_sender' else marmote3.modem_sender2_sptr(), False)
        self.subcarrier_allocator.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.spectrum_vector_to_stream_0 = blocks.vector_to_stream(gr.sizeof_float*1, spectrogram_fft)
        self.spectrum_radar_detector = marmote3.radar_detector(spectrogram_fft, 0.05, 50.0, 10, 10000, False)
        self.spectrum_radar_detector.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.spectrum_nlog10 = blocks.nlog10_ff(10, spectrogram_fft, 6 + 10*math.log10(spectrogram_rate) - 10*math.log10(samp_rate) - rx_gain)
        self.spectrum_keep_large_segments = marmote3.keep_large_segments(gr.sizeof_char * 1, enable_recording != 0, 0, 256 * spectrogram_rate, 256 * spectrogram_rate, 256 * spectrogram_rate * 1800, 0)
        self.spectrum_keep_large_segments.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.spectrum_float_to_char = blocks.float_to_char(1, 4.0)
        self.spectrum_file_sink = blocks.file_sink(gr.sizeof_char*1, '/logs/spectrogram.dat', True)
        self.spectrum_file_sink.set_unbuffered(False)
        self.spectrum_fft_and_magnitude = marmote3.fft_and_magnitude(spectrogram_fft, spectrogram_fft / 2, True, (window.hann(spectrogram_fft)), True)
        self.spectrum_block_sum = marmote3.block_sum_vxx(1 * spectrogram_fft, samp_rate / ( spectrogram_fft / 2) / spectrogram_rate, samp_rate / (spectrogram_fft / 2) / spectrogram_rate)
        self.spectrum_add_const = blocks.add_const_vff((60, ))
        self.samples_keep_large_segments = marmote3.keep_large_segments(gr.sizeof_gr_complex * 1, enable_recording != 0, random.randrange(10, 600) * samp_rate, samp_rate, samp_rate, samp_rate, 1)
        self.samples_keep_large_segments.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.samples_file_sink = blocks.file_sink(gr.sizeof_gr_complex*1, '/logs/samples2.dat', True)
        self.samples_file_sink.set_unbuffered(False)
        self.packet_flow_splitter = marmote3.packet_flow_splitter(gr.sizeof_gr_complex * 1, 3, 2 * (padding_len + max_frame_len + padding_len))
        self.packet_flow_splitter.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.packet_flow_merger = marmote3.packet_flow_merger(gr.sizeof_char * 1, 3, max_data_len)
        self.packet_flow_merger.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.output_rfnoc_txsrc = marmote.txsrc(
                  self.device3,
                  uhd.stream_args( # TX Stream Args
                        cpu_format="fc32",
                        otw_format="sc16",
                        args="gr_vlen={0},{1}".format(1, "" if 1 == 1 else "spp={0}".format(1)),
                  ),
                  uhd.stream_args( # RX Stream Args
                        cpu_format="fc32",
                        otw_format="sc16",
                        args="gr_vlen={0},{1}".format(1, "" if 1 == 1 else "spp={0}".format(1)),
                  ),
                  -1,
                  -1
          )
        self.output_rfnoc_txsrc.set_sample_rate(samp_rate)

        self.output_rfnoc_txbox = marmote.txbox(
                  self.device3,
                  uhd.stream_args( # TX Stream Args
                        cpu_format="fc32",
                        otw_format="sc16",
                        args="gr_vlen={0},{1}".format(1, "" if 1 == 1 else "spp={0}".format(1)),
                  ),
                  uhd.stream_args( # RX Stream Args
                        cpu_format="fc32",
                        otw_format="sc16",
                        args="gr_vlen={0},{1}".format(1, "" if 1 == 1 else "spp={0}".format(1)),
                  ),
                  -1,
                  -1
          )
        self.output_rfnoc_txbox.set_arg("fbmc_fft_size", fbmc_fft_len)
        self.output_rfnoc_txbox.set_arg("invert", invert_spectrum != 0)

        self.output_rfnoc_radio = ettus.rfnoc_radio(
            self.device3,
            uhd.stream_args( # Tx Stream Args
                cpu_format="fc32",
                otw_format="sc16",
        	args='',
            ),
            uhd.stream_args( # Rx Stream Args
                cpu_format="fc32",
                otw_format="sc16",
                args="", # empty
            ),
            0, -1
        )
        self.output_rfnoc_radio.set_rate(200e6)
        for i in xrange(1):
            self.output_rfnoc_radio.set_tx_freq(center_freq - lo_offset, i)
            self.output_rfnoc_radio.set_tx_gain(tx_gain, i)
            self.output_rfnoc_radio.set_tx_dc_offset(False, i)

        self.output_rfnoc_radio.set_tx_antenna("TX/RX", 0)

        self.output_rfnoc_radio.set_clock_source("internal")
        self.output_rfnoc_duc = ettus.rfnoc_generic(
            self.device3,
            uhd.stream_args( # TX Stream Args
                cpu_format="fc32", # TODO: This must be made an option
                otw_format="sc16",
                args="input_rate={},output_rate={},fullscale={},freq={}".format(50e6, 200e6, 1.0, lo_offset),
            ),
            uhd.stream_args( # RX Stream Args
                cpu_format="fc32", # TODO: This must be made an option
                otw_format="sc16",
                args="",
            ),
            "DUC", -1, -1,
        )
        self.output_rfnoc_dma_fifo = ettus.rfnoc_generic(
            self.device3,
            uhd.stream_args( # TX Stream Args
                cpu_format="fc32",
                otw_format="sc16",
                args="gr_vlen={0},{1}".format(1, "" if 1 == 1 else "spp={0}".format(1)),
            ),
            uhd.stream_args( # RX Stream Args
                cpu_format="fc32",
                otw_format="sc16",
                args="gr_vlen={0},{1}".format(1, "" if 1 == 1 else "spp={0}".format(1)),
            ),
            "DmaFIFO", -1, -1,
        )
        self.output_rfnoc_dma_fifo.set_arg("base_addr",0,0)
        self.output_rfnoc_dma_fifo.set_arg("depth",rfnoc_fifo,0)
        self.output_rfnoc_dma_fifo.set_arg("base_addr",4194304,1)
        self.output_rfnoc_dma_fifo.set_arg("depth",4194304,1)
        self.modem_receiver = marmote3.modem_receiver(4, getattr(self, 'z1_modem_sender') if 'z1_modem_sender' else marmote3.modem_sender2_sptr(), False)
        self.modem_receiver.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_modem_control_0 = marmote3.modem_control('center_freq', 'samp_rate', 'rx_gain', 'tx_gain', 'tx_power', 'invert_spectrum', 'enable_recording')
        self.marmote3_modem_control_0.set_center_freq(center_freq)
        self.marmote3_modem_control_0.set_samp_rate(samp_rate)
        self.marmote3_modem_control_0.set_rx_gain(rx_gain)
        self.marmote3_modem_control_0.set_tx_gain(tx_gain)
        self.marmote3_modem_control_0.set_tx_power(tx_power)
        self.marmote3_modem_control_0.set_invert_spectrum(invert_spectrum)
        self.marmote3_modem_control_0.set_enable_recording(enable_recording)
        self.marmote3_modem_control_0.set_num_channels(num_channels)
        self.marmote3_modem_control_0.set_num_subcarriers(fbmc_fft_len)
        self._modem_setter = marmote3.ModemSetter(self)
        self.marmote3_modem_control_0.set_modem_setter(self._modem_setter)
        self.marmote3_modem_control_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.input_rfnoc_rxsrc = marmote.rxsrc(
                  self.device3,
                  uhd.stream_args( # TX Stream Args
                        cpu_format="fc32",
                        otw_format="sc16",
                        args="gr_vlen={0},{1}".format(1, "" if 1 == 1 else "spp={0}".format(1)),
                  ),
                  uhd.stream_args( # RX Stream Args
                        cpu_format="fc32",
                        otw_format="sc16",
                        args="gr_vlen={0},{1}".format(1, "" if 1 == 1 else "spp={0}".format(1)),
                  ),
                  -1,
                  -1
          )
        self.input_rfnoc_rxsrc.set_sample_rate(samp_rate)
        self.input_rfnoc_rxsrc.set_arg("invert", invert_spectrum != 0)

        self.input_rfnoc_radio = ettus.rfnoc_radio(
            self.device3,
            uhd.stream_args( # Tx Stream Args
                cpu_format="fc32",
                otw_format="sc16",
                args="", # empty
            ),
            uhd.stream_args( # Rx Stream Args
                cpu_format="fc32",
                otw_format="sc16",
        	args='',
            ),
            0, -1
        )
        self.input_rfnoc_radio.set_rate(200e6)
        for i in xrange(1):
            self.input_rfnoc_radio.set_rx_freq(center_freq + lo_offset, i)
            self.input_rfnoc_radio.set_rx_gain(rx_gain, i)
            self.input_rfnoc_radio.set_rx_dc_offset(False, i)

        self.input_rfnoc_radio.set_rx_bandwidth(0, 0)

        self.input_rfnoc_radio.set_rx_antenna("RX2", 0)

        self.input_rfnoc_radio.set_clock_source("internal")
        self.input_rfnoc_ddc = ettus.rfnoc_generic(
            self.device3,
            uhd.stream_args( # TX Stream Args
                cpu_format="fc32", # TODO: This must be made an option
                otw_format="sc16",
                channels=range(1),
                args="input_rate={},output_rate={},fullscale={},freq={},gr_vlen={},{}".format(200e6, 50e6, 1.0, lo_offset, 1, "" if 1 == 1 else "spp={}".format(1)),
            ),
            uhd.stream_args( # RX Stream Args
                cpu_format="fc32", # TODO: This must be made an option
                otw_format="sc16",
                channels=range(1),
                args="gr_vlen={},{}".format(1, "" if 1 == 1 else "spp={}".format(1)),
            ),
            "DDC", -1, -1,
        )
        for chan in xrange(1):
            self.input_rfnoc_ddc.set_arg("input_rate", float(200e6), chan)
            self.input_rfnoc_ddc.set_arg("output_rate", float(50e6), chan)
            self.input_rfnoc_ddc.set_arg("fullscale", 1.0, chan)
            self.input_rfnoc_ddc.set_arg("freq", lo_offset, chan)
        self.input_hardware_agc = marmote3.hardware_agc(self, "rx_gain", 0, 24, 0.03, 0.2, 1)
        self.input_copy = marmote3.spectrum_inverter(False)
        self.input_copy.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        (self.input_copy).set_min_output_buffer(32768)
        self.header_decompressor = marmote3.header_decompressor(4, True)
        self.header_decompressor.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.header_compressor = marmote3.header_compressor(4, True)
        self.header_compressor.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.fixed_tuntap_pdu = marmote3.fixed_tuntap_pdu('tap0', 1500, False)
        self.encode_symb_mod = marmote3.packet_symb_mod(marmote3.MOD_MCS or 1, max_symb_len)
        self.encode_ra_encoder = marmote3.packet_ra_encoder(-1 if True else max_data_len, max_code_len)
        self.encode_preamb_insert = marmote3.packet_preamb_insert((np.concatenate([dc_chirp[-padding_len:],dc_chirp,dc_chirp[:padding_len]])), True, tx_power, marmote3.HDR_SRC_MCS_LEN_CRC, postamble, max_frame_len)
        self.decode_symb_demod_2 = marmote3.packet_symb_demod(marmote3.MOD_MCS or 1, 36.0, max_code_len * 8)
        self.decode_symb_demod_1 = marmote3.packet_symb_demod(marmote3.MOD_MCS or 1, 36.0, max_code_len * 8)
        self.decode_symb_demod_0 = marmote3.packet_symb_demod(marmote3.MOD_MCS or 1, 36.0, max_code_len * 8)
        self.decode_ra_decoder_2 = marmote3.packet_ra_decoder(max_data_len, -1 if True else 0, 1, 15)
        self.decode_ra_decoder_1 = marmote3.packet_ra_decoder(max_data_len, -1 if True else 0, 1, 15)
        self.decode_ra_decoder_0 = marmote3.packet_ra_decoder(max_data_len, -1 if True else 0, 1, 15)
        self.decode_preamb_freqcorr_2 = marmote3.packet_preamb_freqcorr2(2 * (padding_len + 40 + padding_len), 2 * chirp_len, postamble, 16, 0.25, 2 * (padding_len + max_frame_len + padding_len))
        self.decode_preamb_freqcorr_1 = marmote3.packet_preamb_freqcorr2(2 * (padding_len + 40 + padding_len), 2 * chirp_len, postamble, 16, 0.25, 2 * (padding_len + max_frame_len + padding_len))
        self.decode_preamb_freqcorr_0 = marmote3.packet_preamb_freqcorr2(2 * (padding_len + 40 + padding_len), 2 * chirp_len, postamble, 16, 0.25, 2 * (padding_len + max_frame_len + padding_len))
        self.decode_preamb_equalizer_2 = marmote3.packet_preamb_equalizer((marmote3.get_expected_signal(dc_chirp, 41)), 2, padding_len, marmote3.HDR_SRC_MCS_LEN_CRC, postamble, max_symb_len, 0.2, False, False)
        self.decode_preamb_equalizer_2.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.decode_preamb_equalizer_1 = marmote3.packet_preamb_equalizer((marmote3.get_expected_signal(dc_chirp, 41)), 2, padding_len, marmote3.HDR_SRC_MCS_LEN_CRC, postamble, max_symb_len, 0.2, False, False)
        self.decode_preamb_equalizer_1.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.decode_preamb_equalizer_0 = marmote3.packet_preamb_equalizer((marmote3.get_expected_signal(dc_chirp, 41)), 2, padding_len, marmote3.HDR_SRC_MCS_LEN_CRC, postamble, max_symb_len, 0.2, False, False)
        self.decode_preamb_equalizer_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.decode_freq_corrector_2 = marmote3.packet_freq_corrector(marmote3.MOD_MCS or 1, 64, postamble, max_symb_len)
        self.decode_freq_corrector_1 = marmote3.packet_freq_corrector(marmote3.MOD_MCS or 1, 64, postamble, max_symb_len)
        self.decode_freq_corrector_0 = marmote3.packet_freq_corrector(marmote3.MOD_MCS or 1, 64, postamble, max_symb_len)
        self.decode_ampl_corrector_2 = marmote3.packet_ampl_corrector(512, max_symb_len)
        self.decode_ampl_corrector_1 = marmote3.packet_ampl_corrector(512, max_symb_len)
        self.decode_ampl_corrector_0 = marmote3.packet_ampl_corrector(512, max_symb_len)
        self.dechirp_transpose_1 = marmote3.transpose_vxx(gr.sizeof_float, chirp_fft_len, num_channels)
        self.dechirp_transpose_0 = marmote3.transpose_vxx(gr.sizeof_gr_complex, num_channels, chirp_fft_len)
        self.dechirp_periodic_multiply_const = marmote3.periodic_multiply_const_cc(num_channels, (np.conj(chirp)))
        self.dechirp_keep_one_in_n = blocks.keep_one_in_n(gr.sizeof_gr_complex*num_channels, 2)
        self.dechirp_fft = fft.fft_vcc(chirp_fft_len, True, (), False, 1)
        self.dechirp_complex_to_mag = blocks.complex_to_mag_squared(chirp_fft_len)
        self.dechirp_chirp_frame_timing = marmote3.chirp_frame_timing(chirp_len, chirp_fft_len, num_channels, 0.0)
        self.dechirp_block_sum = marmote3.block_sum_vxx(1 * num_channels, 1, 2)
        self.chan_subcarrier_remapper = marmote3.subcarrier_remapper(fbmc_fft_len, (subcarrier_map), 0)
        self.chan_polyphase_rotator = marmote3.polyphase_rotator_vcc(fbmc_fft_len, (subcarrier_map), -fbmc_rx_stride)
        self.chan_polyphase_chan_filter = marmote3.polyphase_chan_filter_ccf(fbmc_fft_len, (fbmc_rx_taps), fbmc_rx_stride or fbmc_fft_len)
        self.chan_polyphase_chan_filter.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.chan_periodic_multiply_const = marmote3.periodic_multiply_const_cc(1, (np.exp(-1.0j * np.pi / fbmc_fft_len * np.arange(0, 2 * fbmc_fft_len))))
        self.chan_fft = fft.fft_vcc(fbmc_fft_len, True, (), False, 1)
        self.a1_marmote3_set_core_affinities = marmote3.set_core_affinities(self, 'decode_preamb_freqcorr_2,decode_preamb_equalizer_2,decode_ampl_corrector_2,decode_freq_corrector_2,decode_symb_demod_2,decode_ra_decoder_2\ninput_hardware_agc,samples_keep_large_segments,samples_file_sink\ninput_rfnoc_radio,input_rfnoc_ddc\ninput_rfnoc_rxsrc\ninput_copy\nspectrum_fft_and_magnitude\nspectrum_block_sum,spectrum_radar_detector\nchan_periodic_multiply_const\nchan_polyphase_chan_filter\nchan_fft,vector_peak_probe_chan\nchan_subcarrier_remapper,chan_polyphase_rotator\ndechirp_keep_one_in_n,chan_periodic_multiply_const,dechirp_transpose_0\ndechirp_fft,dechirp_complex_to_mag\ndechirp_chirp_frame_timing,dechirp_transpose_1,dechirp_block_sum\nsubcarrier_serializer2,packet_flow_splitter\nsubcarrier_allocator,synt_polyphase_rotator,synt_subcarrier_remapper\nvector_peak_probe_synt,synt_fft\nsynt_polyphase_synt_filter\noutput_rfnoc_dma_fifo,output_rfnoc_txbox,output_rfnoc_txsrc,output_rfnoc_duc,output_rfnoc_radio\npacket_flow_merger,modem_receiver,header_decompressor,fixed_tuntap_pdu,header_compressor,z1_modem_sender\nencode_ra_encoder,encode_symb_mod,encode_preamb_insert\nspectrum_nlog10,spectrum_vector_to_stream_0,spectrum_add_const,spectrum_float_to_char,spectrum_keep_large_segments,spectrum_file_sink\ndecode_preamb_freqcorr_0,decode_preamb_equalizer_0,decode_ampl_corrector_0,decode_freq_corrector_0,decode_symb_demod_0,decode_ra_decoder_0\ndecode_preamb_freqcorr_1,decode_preamb_equalizer_1,decode_ampl_corrector_1,decode_freq_corrector_1,decode_symb_demod_1,decode_ra_decoder_1\n#spectrum_stream_to_vector,spectrum_fft,spectrum_complex_to_mag,spectrum_block_sum\n#input_usrp_source,input_spectrum_inverter\n#synt_periodic_multiply_const,synt_spectrum_inverter,output_usrp_sink')



        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.fixed_tuntap_pdu, 'out'), (self.header_compressor, 'in'))
        self.msg_connect((self.header_compressor, 'out'), (self.z1_modem_sender, 'in'))
        self.msg_connect((self.header_decompressor, 'out'), (self.fixed_tuntap_pdu, 'in'))
        self.msg_connect((self.modem_receiver, 'out'), (self.header_decompressor, 'in'))
        self.connect((self.chan_fft, 0), (self.chan_subcarrier_remapper, 0))
        self.connect((self.chan_fft, 0), (self.vector_peak_probe_chan, 0))
        self.connect((self.chan_periodic_multiply_const, 0), (self.chan_polyphase_chan_filter, 0))
        self.connect((self.chan_polyphase_chan_filter, 0), (self.chan_fft, 0))
        self.connect((self.chan_polyphase_rotator, 0), (self.dechirp_keep_one_in_n, 0))
        self.connect((self.chan_polyphase_rotator, 0), (self.subcarrier_serializer2, 0))
        self.connect((self.chan_subcarrier_remapper, 0), (self.chan_polyphase_rotator, 0))
        self.connect((self.dechirp_block_sum, 0), (self.subcarrier_serializer2, 1))
        self.connect((self.dechirp_chirp_frame_timing, 0), (self.dechirp_transpose_1, 0))
        self.connect((self.dechirp_complex_to_mag, 0), (self.dechirp_chirp_frame_timing, 0))
        self.connect((self.dechirp_fft, 0), (self.dechirp_complex_to_mag, 0))
        self.connect((self.dechirp_keep_one_in_n, 0), (self.dechirp_periodic_multiply_const, 0))
        self.connect((self.dechirp_periodic_multiply_const, 0), (self.dechirp_transpose_0, 0))
        self.connect((self.dechirp_transpose_0, 0), (self.dechirp_fft, 0))
        self.connect((self.dechirp_transpose_1, 0), (self.dechirp_block_sum, 0))
        self.connect((self.decode_ampl_corrector_0, 0), (self.decode_freq_corrector_0, 0))
        self.connect((self.decode_ampl_corrector_1, 0), (self.decode_freq_corrector_1, 0))
        self.connect((self.decode_ampl_corrector_2, 0), (self.decode_freq_corrector_2, 0))
        self.connect((self.decode_freq_corrector_0, 0), (self.decode_symb_demod_0, 0))
        self.connect((self.decode_freq_corrector_1, 0), (self.decode_symb_demod_1, 0))
        self.connect((self.decode_freq_corrector_2, 0), (self.decode_symb_demod_2, 0))
        self.connect((self.decode_preamb_equalizer_0, 0), (self.decode_ampl_corrector_0, 0))
        self.connect((self.decode_preamb_equalizer_1, 0), (self.decode_ampl_corrector_1, 0))
        self.connect((self.decode_preamb_equalizer_2, 0), (self.decode_ampl_corrector_2, 0))
        self.connect((self.decode_preamb_freqcorr_0, 0), (self.decode_preamb_equalizer_0, 0))
        self.connect((self.decode_preamb_freqcorr_1, 0), (self.decode_preamb_equalizer_1, 0))
        self.connect((self.decode_preamb_freqcorr_2, 0), (self.decode_preamb_equalizer_2, 0))
        self.connect((self.decode_ra_decoder_0, 0), (self.packet_flow_merger, 2))
        self.connect((self.decode_ra_decoder_1, 0), (self.packet_flow_merger, 1))
        self.connect((self.decode_ra_decoder_2, 0), (self.packet_flow_merger, 0))
        self.connect((self.decode_symb_demod_0, 0), (self.decode_ra_decoder_0, 0))
        self.connect((self.decode_symb_demod_1, 0), (self.decode_ra_decoder_1, 0))
        self.connect((self.decode_symb_demod_2, 0), (self.decode_ra_decoder_2, 0))
        self.connect((self.encode_preamb_insert, 0), (self.subcarrier_allocator, 0))
        self.connect((self.encode_ra_encoder, 0), (self.encode_symb_mod, 0))
        self.connect((self.encode_symb_mod, 0), (self.encode_preamb_insert, 0))
        self.connect((self.input_copy, 0), (self.chan_periodic_multiply_const, 0))
        self.connect((self.input_copy, 0), (self.input_hardware_agc, 0))
        self.connect((self.input_copy, 0), (self.samples_keep_large_segments, 0))
        self.connect((self.input_copy, 0), (self.spectrum_fft_and_magnitude, 0))
        self.connect((self.packet_flow_merger, 0), (self.modem_receiver, 0))
        self.connect((self.packet_flow_splitter, 2), (self.decode_preamb_freqcorr_0, 0))
        self.connect((self.packet_flow_splitter, 1), (self.decode_preamb_freqcorr_1, 0))
        self.connect((self.packet_flow_splitter, 0), (self.decode_preamb_freqcorr_2, 0))
        self.connect((self.samples_keep_large_segments, 0), (self.samples_file_sink, 0))
        self.connect((self.spectrum_add_const, 0), (self.spectrum_float_to_char, 0))
        self.connect((self.spectrum_block_sum, 0), (self.spectrum_nlog10, 0))
        self.connect((self.spectrum_fft_and_magnitude, 0), (self.spectrum_block_sum, 0))
        self.connect((self.spectrum_fft_and_magnitude, 0), (self.spectrum_radar_detector, 0))
        self.connect((self.spectrum_float_to_char, 0), (self.spectrum_keep_large_segments, 0))
        self.connect((self.spectrum_keep_large_segments, 0), (self.spectrum_file_sink, 0))
        self.connect((self.spectrum_nlog10, 0), (self.spectrum_vector_to_stream_0, 0))
        self.connect((self.spectrum_vector_to_stream_0, 0), (self.spectrum_add_const, 0))
        self.connect((self.subcarrier_allocator, 0), (self.synt_polyphase_rotator, 0))
        self.connect((self.subcarrier_serializer2, 0), (self.packet_flow_splitter, 0))
        self.connect((self.synt_fft, 0), (self.synt_polyphase_synt_filter, 0))
        self.connect((self.synt_polyphase_rotator, 0), (self.synt_subcarrier_remapper, 0))
        self.connect((self.synt_subcarrier_remapper, 0), (self.synt_fft, 0))
        self.connect((self.synt_subcarrier_remapper, 0), (self.vector_peak_probe_synt, 0))
        self.connect((self.z1_modem_sender, 0), (self.encode_ra_encoder, 0))
        self.connect((self.synt_polyphase_synt_filter, 0), (self.output_rfnoc_dma_fifo, 0))
        self.connect((self.input_rfnoc_rxsrc, 0), (self.input_copy, 0))
        self.device3.connect(self.input_rfnoc_ddc.get_block_id(), 0, self.input_rfnoc_rxsrc.get_block_id(), 0)
        self.device3.connect(self.input_rfnoc_radio.get_block_id(), 0, self.input_rfnoc_ddc.get_block_id(), 0)
        self.device3.connect(self.output_rfnoc_dma_fifo.get_block_id(), 0, self.output_rfnoc_txbox.get_block_id(), 0)
        self.device3.connect(self.output_rfnoc_duc.get_block_id(), 0, self.output_rfnoc_radio.get_block_id(), 0)
        self.device3.connect(self.output_rfnoc_txbox.get_block_id(), 0, self.output_rfnoc_txsrc.get_block_id(), 0)
        self.device3.connect(self.output_rfnoc_txsrc.get_block_id(), 0, self.output_rfnoc_duc.get_block_id(), 0)

    def get_center_freq(self):
        return self.center_freq

    def set_center_freq(self, center_freq):
        self.center_freq = center_freq
        for i in xrange(1):
            self.output_rfnoc_radio.set_tx_freq(self.center_freq - self.lo_offset, i)
        self.marmote3_modem_control_0.set_center_freq(self.center_freq)
        for i in xrange(1):
            self.input_rfnoc_radio.set_rx_freq(self.center_freq + self.lo_offset, i)

    def get_cpu_fifo(self):
        return self.cpu_fifo

    def set_cpu_fifo(self, cpu_fifo):
        self.cpu_fifo = cpu_fifo

    def get_enable_recording(self):
        return self.enable_recording

    def set_enable_recording(self, enable_recording):
        self.enable_recording = enable_recording
        self.spectrum_keep_large_segments.set_enabled(self.enable_recording != 0)
        self.samples_keep_large_segments.set_enabled(self.enable_recording != 0)
        self.marmote3_modem_control_0.set_enable_recording(self.enable_recording)

    def get_invert_spectrum(self):
        return self.invert_spectrum

    def set_invert_spectrum(self, invert_spectrum):
        self.invert_spectrum = invert_spectrum
        self.output_rfnoc_txbox.set_arg("invert", self.invert_spectrum != 0)
        self.marmote3_modem_control_0.set_invert_spectrum(self.invert_spectrum)
        self.input_rfnoc_rxsrc.set_arg("invert", self.invert_spectrum != 0)

    def get_lo_offset(self):
        return self.lo_offset

    def set_lo_offset(self, lo_offset):
        self.lo_offset = lo_offset
        for i in xrange(1):
            self.output_rfnoc_radio.set_tx_freq(self.center_freq - self.lo_offset, i)
        self.output_rfnoc_duc.set_arg("freq", self.lo_offset)
        for i in xrange(1):
            self.input_rfnoc_radio.set_rx_freq(self.center_freq + self.lo_offset, i)
        for i in xrange(1):
            self.input_rfnoc_ddc.set_arg("freq", self.lo_offset, i)

    def get_num_channels(self):
        return self.num_channels

    def set_num_channels(self, num_channels):
        self.num_channels = num_channels
        self.set_subcarrier_map(range(self.fbmc_fft_len-self.num_channels/2, self.fbmc_fft_len) + range(0, self.num_channels/2))
        self.set_fbmc_tx_stride({16: 22, 20: 28, 24: 34, 32: 46, 40: 58}[self.num_channels])
        self.set_fbmc_fft_len(int(self.num_channels * 5 / 4))
        self.set_active_channels(random.sample(range(self.num_channels), 2))
        self.subcarrier_serializer2.set_enabled((range(self.num_channels)))
        self.marmote3_modem_control_0.set_num_channels(self.num_channels)

    def get_radio_id(self):
        return self.radio_id

    def set_radio_id(self, radio_id):
        self.radio_id = radio_id

    def get_rfnoc_fifo(self):
        return self.rfnoc_fifo

    def set_rfnoc_fifo(self, rfnoc_fifo):
        self.rfnoc_fifo = rfnoc_fifo

    def get_rx_gain(self):
        return self.rx_gain

    def set_rx_gain(self, rx_gain):
        self.rx_gain = rx_gain
        self.marmote3_modem_control_0.set_rx_gain(self.rx_gain)
        for i in xrange(1):
            self.input_rfnoc_radio.set_rx_gain(self.rx_gain, i)

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.output_rfnoc_txsrc.set_sample_rate(self.samp_rate)
        self.set_max_frame_time(float(self.max_frame_len) / (self.samp_rate / self.fbmc_tx_stride))
        self.marmote3_modem_control_0.set_samp_rate(self.samp_rate)
        self.input_rfnoc_rxsrc.set_sample_rate(self.samp_rate)

    def get_tx_gain(self):
        return self.tx_gain

    def set_tx_gain(self, tx_gain):
        self.tx_gain = tx_gain
        for i in xrange(1):
            self.output_rfnoc_radio.set_tx_gain(self.tx_gain, i)
        self.marmote3_modem_control_0.set_tx_gain(self.tx_gain)

    def get_tx_power(self):
        return self.tx_power

    def set_tx_power(self, tx_power):
        self.tx_power = tx_power
        self.marmote3_modem_control_0.set_tx_power(self.tx_power)
        self.encode_preamb_insert.set_power(self.tx_power)

    def get_max_data_len(self):
        return self.max_data_len

    def set_max_data_len(self, max_data_len):
        self.max_data_len = max_data_len
        self.set_max_code_len(2 * self.max_data_len)

    def get_max_code_len(self):
        return self.max_code_len

    def set_max_code_len(self, max_code_len):
        self.max_code_len = max_code_len
        self.set_max_symb_len(8 * self.max_code_len)

    def get_padding_len(self):
        return self.padding_len

    def set_padding_len(self, padding_len):
        self.padding_len = padding_len
        self.set_max_frame_len(40 + self.padding_len + self.chirp_len + self.padding_len + self.max_symb_len)

    def get_max_symb_len(self):
        return self.max_symb_len

    def set_max_symb_len(self, max_symb_len):
        self.max_symb_len = max_symb_len
        self.set_max_frame_len(40 + self.padding_len + self.chirp_len + self.padding_len + self.max_symb_len)

    def get_fbmc_fft_len(self):
        return self.fbmc_fft_len

    def set_fbmc_fft_len(self, fbmc_fft_len):
        self.fbmc_fft_len = fbmc_fft_len
        self.set_subcarrier_map(range(self.fbmc_fft_len-self.num_channels/2, self.fbmc_fft_len) + range(0, self.num_channels/2))
        self.set_subcarrier_inv([subcarrier_map.index(x) if x in self.subcarrier_map else -1 for x in range(self.fbmc_fft_len)])
        self.set_fbmc_tx_taps(marmote3.get_fbmc_taps8(self.fbmc_fft_len, self.fbmc_tx_stride, "tx"))
        self.set_fbmc_rx_taps(marmote3.get_fbmc_taps8(self.fbmc_fft_len, self.fbmc_tx_stride, "rx"))
        self.output_rfnoc_txbox.set_arg("fbmc_fft_size", self.fbmc_fft_len)
        self.marmote3_modem_control_0.set_num_subcarriers(self.fbmc_fft_len)
        self.chan_periodic_multiply_const.set_vals((np.exp(-1.0j * np.pi / self.fbmc_fft_len * np.arange(0, 2 * self.fbmc_fft_len))))

    def get_chirp_len(self):
        return self.chirp_len

    def set_chirp_len(self, chirp_len):
        self.chirp_len = chirp_len
        self.set_max_frame_len(40 + self.padding_len + self.chirp_len + self.padding_len + self.max_symb_len)
        self.set_dc_chirp(self.chirp + np.ones(self.chirp_len) * 0.5)
        self.set_chirp(marmote3.get_chirp_taps(self.chirp_len))

    def get_subcarrier_map(self):
        return self.subcarrier_map

    def set_subcarrier_map(self, subcarrier_map):
        self.subcarrier_map = subcarrier_map
        self.set_subcarrier_inv([subcarrier_map.index(x) if x in self.subcarrier_map else -1 for x in range(self.fbmc_fft_len)])

    def get_max_frame_len(self):
        return self.max_frame_len

    def set_max_frame_len(self, max_frame_len):
        self.max_frame_len = max_frame_len
        self.set_max_frame_time(float(self.max_frame_len) / (self.samp_rate / self.fbmc_tx_stride))

    def get_fbmc_tx_stride(self):
        return self.fbmc_tx_stride

    def set_fbmc_tx_stride(self, fbmc_tx_stride):
        self.fbmc_tx_stride = fbmc_tx_stride
        self.set_fbmc_tx_taps(marmote3.get_fbmc_taps8(self.fbmc_fft_len, self.fbmc_tx_stride, "tx"))
        self.set_fbmc_rx_taps(marmote3.get_fbmc_taps8(self.fbmc_fft_len, self.fbmc_tx_stride, "rx"))
        self.set_fbmc_rx_stride(self.fbmc_tx_stride/2)
        self.set_max_frame_time(float(self.max_frame_len) / (self.samp_rate / self.fbmc_tx_stride))

    def get_chirp(self):
        return self.chirp

    def set_chirp(self, chirp):
        self.chirp = chirp
        self.set_dc_chirp(self.chirp + np.ones(self.chirp_len) * 0.5)
        self.dechirp_periodic_multiply_const.set_vals((np.conj(self.chirp)))

    def get_variable_uhd_device3_0(self):
        return self.variable_uhd_device3_0

    def set_variable_uhd_device3_0(self, variable_uhd_device3_0):
        self.variable_uhd_device3_0 = variable_uhd_device3_0

    def get_variable_marmote3_modem_monitor_0(self):
        return self.variable_marmote3_modem_monitor_0

    def set_variable_marmote3_modem_monitor_0(self, variable_marmote3_modem_monitor_0):
        self.variable_marmote3_modem_monitor_0 = variable_marmote3_modem_monitor_0

    def get_subcarrier_inv(self):
        return self.subcarrier_inv

    def set_subcarrier_inv(self, subcarrier_inv):
        self.subcarrier_inv = subcarrier_inv

    def get_spectrogram_rate(self):
        return self.spectrogram_rate

    def set_spectrogram_rate(self, spectrogram_rate):
        self.spectrogram_rate = spectrogram_rate

    def get_spectrogram_fft(self):
        return self.spectrogram_fft

    def set_spectrogram_fft(self, spectrogram_fft):
        self.spectrogram_fft = spectrogram_fft

    def get_postamble(self):
        return self.postamble

    def set_postamble(self, postamble):
        self.postamble = postamble

    def get_max_frame_time(self):
        return self.max_frame_time

    def set_max_frame_time(self, max_frame_time):
        self.max_frame_time = max_frame_time

    def get_fbmc_tx_taps(self):
        return self.fbmc_tx_taps

    def set_fbmc_tx_taps(self, fbmc_tx_taps):
        self.fbmc_tx_taps = fbmc_tx_taps

    def get_fbmc_rx_taps(self):
        return self.fbmc_rx_taps

    def set_fbmc_rx_taps(self, fbmc_rx_taps):
        self.fbmc_rx_taps = fbmc_rx_taps

    def get_fbmc_rx_stride(self):
        return self.fbmc_rx_stride

    def set_fbmc_rx_stride(self, fbmc_rx_stride):
        self.fbmc_rx_stride = fbmc_rx_stride

    def get_dc_chirp(self):
        return self.dc_chirp

    def set_dc_chirp(self, dc_chirp):
        self.dc_chirp = dc_chirp

    def get_chirp_fft_len(self):
        return self.chirp_fft_len

    def set_chirp_fft_len(self, chirp_fft_len):
        self.chirp_fft_len = chirp_fft_len

    def get_active_channels(self):
        return self.active_channels

    def set_active_channels(self, active_channels):
        self.active_channels = active_channels
        self.subcarrier_allocator.set_active_channels((self.active_channels))


def argument_parser():
    parser = OptionParser(usage="%prog: [options]", option_class=eng_option)
    parser.add_option(
        "-f", "--center-freq", dest="center_freq", type="eng_float", default=eng_notation.num_to_str(1e9),
        help="Set frequency [default=%default]")
    parser.add_option(
        "", "--cpu-fifo", dest="cpu_fifo", type="intx", default=65536,
        help="Set CPU output buffer size in bytes [default=%default]")
    parser.add_option(
        "", "--enable-recording", dest="enable_recording", type="intx", default=0,
        help="Set enable recording [default=%default]")
    parser.add_option(
        "", "--invert-spectrum", dest="invert_spectrum", type="intx", default=0,
        help="Set Invert Spectrum [default=%default]")
    parser.add_option(
        "", "--lo-offset", dest="lo_offset", type="eng_float", default=eng_notation.num_to_str(42e6),
        help="Set LO offset [default=%default]")
    parser.add_option(
        "-n", "--num-channels", dest="num_channels", type="intx", default=32,
        help="Set number of channels (must be multiple of 4) [default=%default]")
    parser.add_option(
        "", "--radio-id", dest="radio_id", type="intx", default=-1,
        help="Set radio id [default=%default]")
    parser.add_option(
        "", "--rfnoc-fifo", dest="rfnoc_fifo", type="intx", default=4*65536,
        help="Set RFNoC FIFO depth [default=%default]")
    parser.add_option(
        "", "--rx-gain", dest="rx_gain", type="eng_float", default=eng_notation.num_to_str(24),
        help="Set RX gain [default=%default]")
    parser.add_option(
        "-r", "--samp-rate", dest="samp_rate", type="long", default=50000000,
        help="Set sample rate [default=%default]")
    parser.add_option(
        "", "--tx-gain", dest="tx_gain", type="eng_float", default=eng_notation.num_to_str(24),
        help="Set TX gain [default=%default]")
    parser.add_option(
        "", "--tx-power", dest="tx_power", type="eng_float", default=eng_notation.num_to_str(0),
        help="Set TX power in dB [default=%default]")
    return parser


def main(top_block_cls=full_modem, options=None):
    if options is None:
        options, _ = argument_parser().parse_args()

    tb = top_block_cls(center_freq=options.center_freq, cpu_fifo=options.cpu_fifo, enable_recording=options.enable_recording, invert_spectrum=options.invert_spectrum, lo_offset=options.lo_offset, num_channels=options.num_channels, radio_id=options.radio_id, rfnoc_fifo=options.rfnoc_fifo, rx_gain=options.rx_gain, samp_rate=options.samp_rate, tx_gain=options.tx_gain, tx_power=options.tx_power)
    tb.start()
    tb.wait()


if __name__ == '__main__':
    main()
