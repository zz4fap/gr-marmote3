#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Split Record Test
# Generated: Mon Feb  4 23:09:42 2019
##################################################

from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio import uhd
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from optparse import OptionParser
import ettus
import marmote3
import random


class split_record_test(gr.top_block):

    def __init__(self, samp_rate=50000000):
        gr.top_block.__init__(self, "Split Record Test")

        ##################################################
        # Parameters
        ##################################################
        self.samp_rate = samp_rate

        ##################################################
        # Variables
        ##################################################
        self.device3 = variable_uhd_device3_0 = ettus.device3(uhd.device_addr_t( ",".join(('type=x300', "")) ))

        ##################################################
        # Blocks
        ##################################################
        self.uhd_rfnoc_streamer_radio_0 = ettus.rfnoc_radio(
            self.device3,
            uhd.stream_args( # Tx Stream Args
                cpu_format="fc32",
                otw_format="sc16",
                args="", # empty
            ),
            uhd.stream_args( # Rx Stream Args
                cpu_format="fc32",
                otw_format="sc16",
        	args='',
            ),
            0, -1
        )
        self.uhd_rfnoc_streamer_radio_0.set_rate(200e6)
        for i in xrange(1):
            self.uhd_rfnoc_streamer_radio_0.set_rx_freq(1e9, i)
            self.uhd_rfnoc_streamer_radio_0.set_rx_gain(20, i)
            self.uhd_rfnoc_streamer_radio_0.set_rx_dc_offset(False, i)

        self.uhd_rfnoc_streamer_radio_0.set_rx_bandwidth(56e6, 0)

        self.uhd_rfnoc_streamer_radio_0.set_rx_antenna("RX2", 0)

        self.uhd_rfnoc_streamer_radio_0.set_clock_source("internal")
        self.uhd_rfnoc_streamer_ddc_0 = ettus.rfnoc_generic(
            self.device3,
            uhd.stream_args( # TX Stream Args
                cpu_format="fc32", # TODO: This must be made an option
                otw_format="sc16",
                channels=range(1),
                args="input_rate={},output_rate={},fullscale={},freq={},gr_vlen={},{}".format(200e6, samp_rate, 1.0, 0.0, 1, "" if 1 == 1 else "spp={}".format(1)),
            ),
            uhd.stream_args( # RX Stream Args
                cpu_format="fc32", # TODO: This must be made an option
                otw_format="sc16",
                channels=range(1),
                args="gr_vlen={},{}".format(1, "" if 1 == 1 else "spp={}".format(1)),
            ),
            "DDC", -1, -1,
        )
        for chan in xrange(1):
            self.uhd_rfnoc_streamer_ddc_0.set_arg("input_rate", float(200e6), chan)
            self.uhd_rfnoc_streamer_ddc_0.set_arg("output_rate", float(samp_rate), chan)
            self.uhd_rfnoc_streamer_ddc_0.set_arg("fullscale", 1.0, chan)
            self.uhd_rfnoc_streamer_ddc_0.set_arg("freq", 0.0, chan)
        self.uhd_rfnoc_split_stream_0 = ettus.rfnoc_generic(
            self.device3,
            uhd.stream_args( # TX Stream Args
                cpu_format="fc32",
                otw_format="sc16",
                args="gr_vlen={0},{1}".format(1, "" if 1 == 1 else "spp={0}".format(1)),
            ),
            uhd.stream_args( # RX Stream Args
                cpu_format="fc32",
                otw_format="sc16",
        	    channels=(0,1),
                args="gr_vlen={0},{1}".format(1, "" if 1 == 1 else "spp={0}".format(1)),
            ),
            "SplitStream", -1, -1,
        )
        self.marmote3_keep_large_segments_1_0 = marmote3.keep_large_segments(gr.sizeof_gr_complex * 1, 1 != 0, random.randrange(3, 10) * samp_rate, samp_rate, samp_rate, samp_rate * 2, 1)
        self.marmote3_keep_large_segments_1_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_keep_large_segments_1 = marmote3.keep_large_segments(gr.sizeof_gr_complex * 1, 1 != 0, random.randrange(2, 10) * samp_rate, samp_rate, samp_rate, samp_rate * 2, 0)
        self.marmote3_keep_large_segments_1.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.blocks_null_sink_1 = blocks.null_sink(gr.sizeof_gr_complex*1)
        self.blocks_null_sink_0 = blocks.null_sink(gr.sizeof_gr_complex*1)
        self.blocks_file_sink_0_0_0 = blocks.file_sink(gr.sizeof_gr_complex*1, '/logs/samples2.dat', False)
        self.blocks_file_sink_0_0_0.set_unbuffered(False)
        self.blocks_file_sink_0_0 = blocks.file_sink(gr.sizeof_gr_complex*1, '/logs/samples1.dat', False)
        self.blocks_file_sink_0_0.set_unbuffered(False)
        self.blocks_copy_0_0 = blocks.copy(gr.sizeof_gr_complex*1)
        self.blocks_copy_0_0.set_enabled(True)
        self.blocks_copy_0 = blocks.copy(gr.sizeof_gr_complex*1)
        self.blocks_copy_0.set_enabled(True)



        ##################################################
        # Connections
        ##################################################
        self.connect((self.blocks_copy_0, 0), (self.blocks_null_sink_0, 0))
        self.connect((self.blocks_copy_0, 0), (self.marmote3_keep_large_segments_1, 0))
        self.connect((self.blocks_copy_0_0, 0), (self.blocks_null_sink_1, 0))
        self.connect((self.blocks_copy_0_0, 0), (self.marmote3_keep_large_segments_1_0, 0))
        self.connect((self.marmote3_keep_large_segments_1, 0), (self.blocks_file_sink_0_0, 0))
        self.connect((self.marmote3_keep_large_segments_1_0, 0), (self.blocks_file_sink_0_0_0, 0))
        self.connect((self.uhd_rfnoc_split_stream_0, 0), (self.blocks_copy_0, 0))
        self.connect((self.uhd_rfnoc_split_stream_0, 1), (self.blocks_copy_0_0, 0))
        self.device3.connect(self.uhd_rfnoc_streamer_ddc_0.get_block_id(), 0, self.uhd_rfnoc_split_stream_0.get_block_id(), 0)
        self.device3.connect(self.uhd_rfnoc_streamer_radio_0.get_block_id(), 0, self.uhd_rfnoc_streamer_ddc_0.get_block_id(), 0)

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        for i in xrange(1):
            self.uhd_rfnoc_streamer_ddc_0.set_arg("output_rate", float(self.samp_rate), i)

    def get_variable_uhd_device3_0(self):
        return self.variable_uhd_device3_0

    def set_variable_uhd_device3_0(self, variable_uhd_device3_0):
        self.variable_uhd_device3_0 = variable_uhd_device3_0


def argument_parser():
    parser = OptionParser(usage="%prog: [options]", option_class=eng_option)
    parser.add_option(
        "-r", "--samp-rate", dest="samp_rate", type="long", default=50000000,
        help="Set sample rate [default=%default]")
    return parser


def main(top_block_cls=split_record_test, options=None):
    if options is None:
        options, _ = argument_parser().parse_args()

    tb = top_block_cls(samp_rate=options.samp_rate)
    tb.start()
    try:
        raw_input('Press Enter to quit: ')
    except EOFError:
        pass
    tb.stop()
    tb.wait()


if __name__ == '__main__':
    main()
