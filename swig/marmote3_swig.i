/* -*- c++ -*- */

#define MARMOTE3_API

%include <gnuradio.i>			// the common stuff
%include <exception.i>			// for standard exceptions

//load generated python docstrings
%include "marmote3_swig_doc.i"

%{
#include "marmote3/constants.h"
#include "marmote3/tagged_stream_block2.h"
#include "marmote3/fec_ra_xcoder.h"
#include "marmote3/fec_conv_xcoder.h"
#include "marmote3/polyphase_synt_filter_ccf.h"
#include "marmote3/polyphase_chan_filter_ccf.h"
#include "marmote3/vector_fir_filter_ccc.h"
#include "marmote3/periodic_multiply_const_cc.h"
#include "marmote3/subcarrier_allocator.h"
#include "marmote3/transpose_vxx.h"
#include "marmote3/block_sum_vxx.h"
#include "marmote3/vector_fir_filter_fff.h"
#include "marmote3/chirp_frame_timing.h"
#include "marmote3/subcarrier_remapper.h"
#include "marmote3/polyphase_rotator_vcc.h"
#include "marmote3/packet_preamb_equalizer.h"
#include "marmote3/packet_freq_corrector.h"
#include "marmote3/packet_symb_mod.h"
#include "marmote3/packet_symb_demod.h"
#include "marmote3/packet_preamb_insert.h"
#include "marmote3/packet_ra_encoder.h"
#include "marmote3/packet_ra_decoder.h"
#include "marmote3/packet_flow_splitter.h"
#include "marmote3/message_to_packet.h"
#include "marmote3/packet_to_message.h"
#include "marmote3/keep_large_segments.h"
#include "marmote3/packet_flow_merger.h"
#include "marmote3/fast_dc_blocker.h"
#include "marmote3/zmq_packet_sink.h"
#include "marmote3/fixed_tuntap_pdu.h"
#include "marmote3/packet_debug.h"
#include "marmote3/modem_bert_sender.h"
#include "marmote3/test_message_source.h"
#include "marmote3/test_message_sink.h"
#include "marmote3/modem_receiver.h"
#include "marmote3/modem_bert_receiver.h"
#include "marmote3/header_compressor.h"
#include "marmote3/header_decompressor.h"
#include "marmote3/packet_rep_encoder.h"
#include "marmote3/packet_rep_decoder.h"
#include "marmote3/scenario_emulator_simple.h"
#include "marmote3/packet_ampl_corrector.h"
#include "marmote3/peak_probe.h"
#include "marmote3/subcarrier_serializer2.h"
#include "marmote3/vector_peak_probe.h"
#include "marmote3/packet_preamb_freqcorr.h"
#include "marmote3/packet_preamb_freqcorr2.h"
#include "marmote3/spectrum_inverter.h"
#include "marmote3/modem_monitor.h"
#include "marmote3/modem_control.h"
#include "marmote3/modem_sender2.h"
#include "marmote3/radar_detector.h"
#include "marmote3/fft_and_magnitude.h"
%}

%include "marmote3/constants.h"
%include "marmote3/tagged_stream_block2.h"
%include "marmote3/fec_ra_xcoder.h"
%include "marmote3/fec_conv_xcoder.h"
%include "marmote3/modem_monitor.h"
%include "marmote3/modem_sender2.h"
%include "marmote3/vector_peak_probe.h"
%include "marmote3/modem_control.h"

%exception {
    try {
        $action
    } catch(const std::exception &e) {
        SWIG_exception(SWIG_RuntimeError, e.what());
    } catch (...) {
        SWIG_exception(SWIG_RuntimeError, "unknown exception");
    }
}

%include "marmote3/polyphase_synt_filter_ccf.h"
GR_SWIG_BLOCK_MAGIC2(marmote3, polyphase_synt_filter_ccf);
%include "marmote3/polyphase_chan_filter_ccf.h"
GR_SWIG_BLOCK_MAGIC2(marmote3, polyphase_chan_filter_ccf);
%include "marmote3/vector_fir_filter_ccc.h"
GR_SWIG_BLOCK_MAGIC2(marmote3, vector_fir_filter_ccc);
%include "marmote3/periodic_multiply_const_cc.h"
GR_SWIG_BLOCK_MAGIC2(marmote3, periodic_multiply_const_cc);
%include "marmote3/subcarrier_allocator.h"
GR_SWIG_BLOCK_MAGIC2(marmote3, subcarrier_allocator);
%include "marmote3/transpose_vxx.h"
GR_SWIG_BLOCK_MAGIC2(marmote3, transpose_vxx);
%include "marmote3/block_sum_vxx.h"
GR_SWIG_BLOCK_MAGIC2(marmote3, block_sum_vxx);
%include "marmote3/vector_fir_filter_fff.h"
GR_SWIG_BLOCK_MAGIC2(marmote3, vector_fir_filter_fff);
%include "marmote3/chirp_frame_timing.h"
GR_SWIG_BLOCK_MAGIC2(marmote3, chirp_frame_timing);
%include "marmote3/subcarrier_remapper.h"
GR_SWIG_BLOCK_MAGIC2(marmote3, subcarrier_remapper);
%include "marmote3/polyphase_rotator_vcc.h"
GR_SWIG_BLOCK_MAGIC2(marmote3, polyphase_rotator_vcc);
%include "marmote3/packet_preamb_equalizer.h"
GR_SWIG_BLOCK_MAGIC2(marmote3, packet_preamb_equalizer);
%include "marmote3/packet_freq_corrector.h"
GR_SWIG_BLOCK_MAGIC2(marmote3, packet_freq_corrector);
%include "marmote3/packet_symb_mod.h"
GR_SWIG_BLOCK_MAGIC2(marmote3, packet_symb_mod);
%include "marmote3/packet_symb_demod.h"
GR_SWIG_BLOCK_MAGIC2(marmote3, packet_symb_demod);
%include "marmote3/packet_preamb_insert.h"
GR_SWIG_BLOCK_MAGIC2(marmote3, packet_preamb_insert);
%include "marmote3/packet_ra_encoder.h"
GR_SWIG_BLOCK_MAGIC2(marmote3, packet_ra_encoder);
%include "marmote3/packet_ra_decoder.h"
GR_SWIG_BLOCK_MAGIC2(marmote3, packet_ra_decoder);
%include "marmote3/packet_flow_splitter.h"
GR_SWIG_BLOCK_MAGIC2(marmote3, packet_flow_splitter);
%include "marmote3/message_to_packet.h"
GR_SWIG_BLOCK_MAGIC2(marmote3, message_to_packet);
%include "marmote3/packet_to_message.h"
GR_SWIG_BLOCK_MAGIC2(marmote3, packet_to_message);
%include "marmote3/keep_large_segments.h"
GR_SWIG_BLOCK_MAGIC2(marmote3, keep_large_segments);
%include "marmote3/packet_flow_merger.h"
GR_SWIG_BLOCK_MAGIC2(marmote3, packet_flow_merger);
%include "marmote3/fast_dc_blocker.h"
GR_SWIG_BLOCK_MAGIC2(marmote3, fast_dc_blocker);
%include "marmote3/zmq_packet_sink.h"
GR_SWIG_BLOCK_MAGIC2(marmote3, zmq_packet_sink);
%include "marmote3/fixed_tuntap_pdu.h"
GR_SWIG_BLOCK_MAGIC2(marmote3, fixed_tuntap_pdu);
%include "marmote3/packet_debug.h"
GR_SWIG_BLOCK_MAGIC2(marmote3, packet_debug);
%include "marmote3/modem_bert_sender.h"
GR_SWIG_BLOCK_MAGIC2(marmote3, modem_bert_sender);
%include "marmote3/test_message_source.h"
GR_SWIG_BLOCK_MAGIC2(marmote3, test_message_source);
%include "marmote3/test_message_sink.h"
GR_SWIG_BLOCK_MAGIC2(marmote3, test_message_sink);
%include "marmote3/modem_receiver.h"
GR_SWIG_BLOCK_MAGIC2(marmote3, modem_receiver);
%include "marmote3/modem_bert_receiver.h"
GR_SWIG_BLOCK_MAGIC2(marmote3, modem_bert_receiver);
%include "marmote3/header_compressor.h"
GR_SWIG_BLOCK_MAGIC2(marmote3, header_compressor);
%include "marmote3/header_decompressor.h"
GR_SWIG_BLOCK_MAGIC2(marmote3, header_decompressor);
%include "marmote3/packet_rep_encoder.h"
GR_SWIG_BLOCK_MAGIC2(marmote3, packet_rep_encoder);
%include "marmote3/packet_rep_decoder.h"
GR_SWIG_BLOCK_MAGIC2(marmote3, packet_rep_decoder);
%include "marmote3/scenario_emulator_simple.h"
GR_SWIG_BLOCK_MAGIC2(marmote3, scenario_emulator_simple);
%include "marmote3/packet_ampl_corrector.h"
GR_SWIG_BLOCK_MAGIC2(marmote3, packet_ampl_corrector);
%include "marmote3/peak_probe.h"
GR_SWIG_BLOCK_MAGIC2(marmote3, peak_probe);
%include "marmote3/subcarrier_serializer2.h"
GR_SWIG_BLOCK_MAGIC2(marmote3, subcarrier_serializer2);
%include "marmote3/vector_peak_probe.h"
GR_SWIG_BLOCK_MAGIC2(marmote3, vector_peak_probe);
%include "marmote3/packet_preamb_freqcorr.h"
GR_SWIG_BLOCK_MAGIC2(marmote3, packet_preamb_freqcorr);
%include "marmote3/packet_preamb_freqcorr2.h"
GR_SWIG_BLOCK_MAGIC2(marmote3, packet_preamb_freqcorr2);
%include "marmote3/spectrum_inverter.h"
GR_SWIG_BLOCK_MAGIC2(marmote3, spectrum_inverter);
%include "marmote3/modem_monitor.h"
GR_SWIG_BLOCK_MAGIC2(marmote3, modem_monitor);
%include "marmote3/modem_sender2.h"
GR_SWIG_BLOCK_MAGIC2(marmote3, modem_sender2);
%include "marmote3/radar_detector.h"
GR_SWIG_BLOCK_MAGIC2(marmote3, radar_detector);
%include "marmote3/fft_and_magnitude.h"
GR_SWIG_BLOCK_MAGIC2(marmote3, fft_and_magnitude);
