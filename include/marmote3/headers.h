/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_MARMOTE3_HEADERS_H
#define INCLUDED_MARMOTE3_HEADERS_H

#include <cstdint>
#include <marmote3/api.h>

namespace gr {
namespace marmote3 {

struct MARMOTE3_API __attribute__((packed)) arq_header_t {
  arq_header_t() : extra(0), flow_seqno(0) {}

  unsigned char extra : 4;      // amount of extra padding bytes before packet
  unsigned char flow_seqno : 4; // incremented for each sent packets
  unsigned char srcdst_seqno;   // incremented for each sent packets

  static constexpr int flow_seqno_width = 4;
  static constexpr int extra_width = 4;
  static constexpr int initial_seqno = -1; // initialize with this

  static void increment_flow_seqno(int &last_sent_seqno); // call before send
  static int get_flow_missed_count(int rcvd_seqno, int last_rcvd_seqno);
};

struct MARMOTE3_API __attribute__((packed)) mmt_header_t {
  mmt_header_t() : flags(0), src(0), dst(0), flowid(0) {}

  uint16_t flags;
  uint8_t src;     // eth last byte of src MAC addr
  uint8_t dst;     // eth last byte of dst MAC addr
  uint16_t flowid; // dst port of UDP or TCP

  void load(const uint8_t *ptr);
  void save(uint8_t *ptr) const;
  static int len() { return 6; }

  bool operator==(const mmt_header_t &mmt) const;
  long get_hash() const;

  // returns zero for incorrect packets
  static long get_zip_payload_len(const uint8_t *data_ptr, int data_len,
                                  int payload_mode);
};

std::ostream &operator<<(std::ostream &str, const mmt_header_t &mmt);

enum {
  FLAGS_ETH_MAC = 0x0001, // MACs are 12:34:56:78:90:xx or ff:ff:ff:ff:ff:ff
  FLAGS_ETH_IP4 = 0x0002, // ethertype is 0x0800, no 802.1Q tag
  FLAGS_ETH_MSK = 0x0003,

  FLAGS_IP4_IHL = 0x0004, // ihl is 5, no options
  FLAGS_IP4_LEN = 0x0008, // total len can be calculated from payload len
  FLAGS_IP4_FRG = 0x0010, // DF is set, fragment offset is 0 (0x4000)
  FLAGS_IP4_TTL = 0x0020, // TTL is 254
  FLAGS_IP4_UDP = 0x0040, // protocol is UDP (0x11)
  FLAGS_IP4_ADR = 0x0080, // IP addresses are 192.168.*.*
  FLAGS_IP4_SPC = 0x0100, // IP addresses are 192.168.src/dst.*
  FLAGS_IP4_MSK = 0x01FC,

  FLAGS_UDP_PRT = 0x0200, // dst_port = src_port + 1000
  FLAGS_UDP_LEN = 0x0400, // udp len calculated from ip4
  FLAGS_UDP_CRC = 0x0800, // crc calculated from payload
  FLAGS_UDP_MSK = 0x0E00,

  FLAGS_TCP_PRT = 0x0200, // dst_port = src_port + 1000
  FLAGS_TCP_URG = 0x0400, // urg is 0
  FLAGS_TCP_CRC = 0x0800, // crc calculated from payload
  FLAGS_TCP_MSK = 0x0E00,

  FLAGS_MGN_VLD = 0x1000, // valid MGEN packet
  FLAGS_MGN_TOS = 0x2000, // ver = 4, tos = ip4.tos
  FLAGS_MGN_FRG = 0x4000, // flags = 0xc, frag = 0
  FLAGS_MGN_GPS = 0x8000, // constant GPS data
  FLAGS_MGN_MSK = 0xF000,
};

/*!
 * \brief <+description+>
 */
struct MARMOTE3_API __attribute__((packed)) eth_header_t {
  eth_header_t() : tci(0xffff) {}

  uint8_t dst[6]; // dst mac address
  uint8_t src[6]; // src mac address
  uint16_t tci;   // 802.1Q tag, 0xffff if not tagged
  uint16_t typ;   // ethertype, this is never 0x8100

  int eth_raw_load(const uint8_t *ptr, int len);
  int eth_raw_len() const;
  int eth_raw_save(uint8_t *ptr) const;

  int eth_zip_load(const mmt_header_t &mmt, const uint8_t *ptr, int len);
  int eth_zip_save1(mmt_header_t &mmt) const;
  int eth_zip_save2(const mmt_header_t &mmt, uint8_t *ptr) const;

  bool operator==(const eth_header_t &eth) const;
};

MARMOTE3_API uint16_t ip4_crc(const uint8_t *ptr, int len);

/*!
 * \brief <+description+>
 */
struct MARMOTE3_API __attribute__((packed)) ip4_header_t {
  ip4_header_t() : ihl(5) {}

  uint8_t ihl;     // ihl, must be at least 5 and at most 15
  uint8_t tos;     // type of service
  uint16_t tlen;   // total length
  uint16_t id;     // identification
  uint16_t frag;   // flags and fragment offset
  uint8_t ttl;     // time to live
  uint8_t prot;    // protocol
  uint8_t src[4];  // src ip address
  uint8_t dst[4];  // dst ip address
  uint8_t opt[40]; // options of 4*(ihl-5) size

  int ip4_raw_load(const uint8_t *ptr, int len);
  int ip4_raw_len() const;
  int ip4_raw_save(uint8_t *ptr) const;

  int ip4_zip_load1(const mmt_header_t &mmt, const uint8_t *ptr, int len);
  void ip4_zip_load2(const mmt_header_t &mmt, int payload_len);
  int ip4_zip_save1(mmt_header_t &mmt, int payload_len) const;
  int ip4_zip_save2(const mmt_header_t &mmt, uint8_t *ptr) const;

  bool operator==(const ip4_header_t &ip4) const;
};

/*!
 * \brief <+description+>
 */
struct MARMOTE3_API __attribute__((packed)) udp_header_t {
  udp_header_t() {}

  uint16_t src_port;
  uint16_t dst_port;
  uint16_t ulen; // must be between 8 and 65507
  uint16_t crc;

  int udp_raw_load(const uint8_t *ptr, int len);
  static int udp_raw_len() { return 8; }
  void udp_raw_save(uint8_t *ptr) const;

  static uint16_t udp_crc(const ip4_header_t &ip4, const udp_header_t &udp,
                          const uint8_t *ptr, int len);
  // mmt must be valid or set with udp_zip_save1
  static int udp_zip_len(const mmt_header_t &mmt);

  void udp_zip_load1(const mmt_header_t &mmt, const uint8_t *ptr);
  void udp_zip_load2(const mmt_header_t &mmt, const ip4_header_t &ip4,
                     const uint8_t *payload, int payload_len);
  void udp_zip_save1(mmt_header_t &mmt, const ip4_header_t &ip4,
                     const uint8_t *payload, int payload_len) const;
  void udp_zip_save2(const mmt_header_t &mmt, uint8_t *ptr) const;

  bool operator==(const udp_header_t &udp) const;
};

/*!
 * \brief <+description+>
 */
struct MARMOTE3_API __attribute__((packed)) tcp_header_t {
  tcp_header_t() : data_off(5) {}

  uint16_t src_port;
  uint16_t dst_port;
  uint32_t seq_no;
  uint32_t ack_no;
  uint8_t data_off; // must be between 5 and 15
  uint16_t flags;   // must be at most 0x0fff
  uint16_t win_size;
  uint16_t crc;
  uint16_t urg;
  uint8_t opt[40]; // options of 4*(data_off-5) size

  int tcp_raw_load(const uint8_t *ptr, int len);
  int tcp_raw_len() const;
  int tcp_raw_save(uint8_t *ptr) const;

  int tcp_zip_load1(const mmt_header_t &mmt, const uint8_t *ptr, int len);
  void tcp_zip_load2(const mmt_header_t &mmt, const ip4_header_t &ip4,
                     const uint8_t *payload, int payload_len);
  int tcp_zip_save1(mmt_header_t &mmt, const ip4_header_t &ip4,
                    const uint8_t *payload, int payload_len) const;
  int tcp_zip_save2(const mmt_header_t &mmt, uint8_t *ptr) const;

  bool operator==(const tcp_header_t &tcp) const;
};

/*!
 * \brief <+description+>
 */
struct MARMOTE3_API __attribute__((packed)) mgn_header_t {
  mgn_header_t() : valid(false) {}

  bool valid;
  bool valid_gps;

  uint16_t messageSize;
  uint8_t mgenVersion;
  uint8_t mgenFlags;
  uint32_t mgenSeqno;
  uint32_t mgenFrag;
  uint32_t txTimeSeconds;
  uint32_t txTimeMicroseconds; // must be between 0 and 999999
  uint8_t mgenTos;

  int mgn_raw_load(const ip4_header_t &ip4, uint16_t dst_port,
                   const uint8_t *ptr, int len);
  int mgn_raw_len() const;
  int mgn_raw_save(const ip4_header_t &ip4, uint16_t dst_port,
                   uint8_t *ptr) const;

  // mmt must be valid or set with udp_zip_save1
  static int mgn_zip_len(const mmt_header_t &mmt);

  int mgn_zip_load(const mmt_header_t &mmt, const ip4_header_t &ip4,
                   const uint8_t *ptr, int len);
  int mgn_zip_save1(mmt_header_t &mmt, const ip4_header_t &ip4) const;
  int mgn_zip_save2(const mmt_header_t &mmt, uint8_t *ptr) const;

  bool operator==(const mgn_header_t &mgn) const;
};

} // namespace marmote3
} // namespace gr

namespace std {

template <> struct hash<gr::marmote3::mmt_header_t> {
  typedef gr::marmote3::mmt_header_t argument_type;
  typedef long result_type;
  result_type operator()(argument_type const &mmt) const {
    return mmt.get_hash();
  }
};

} // namespace std

#endif /* INCLUDED_MARMOTE3_HEADERS_H */
