/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_MARMOTE3_SUBCARRIER_ALLOCATOR_H
#define INCLUDED_MARMOTE3_SUBCARRIER_ALLOCATOR_H

#include <gnuradio/block.h>
#include <marmote3/api.h>
#include <marmote3/modem_monitor.h>
#include <marmote3/modem_sender2.h>
#include <marmote3/vector_peak_probe.h>

namespace gr {
namespace marmote3 {

/*!
 * \brief <+description of block+>
 * \ingroup marmote3
 *
 */
class MARMOTE3_API subcarrier_allocator : virtual public gr::block,
                                          virtual public modem_monitored {
public:
  typedef boost::shared_ptr<subcarrier_allocator> sptr;

  /*!
   * \brief Return a shared_ptr to a new instance of
   * marmote3::subcarrier_allocator.
   *
   * To avoid accidental use of raw pointers, marmote3::subcarrier_allocator's
   * constructor is in a private implementation
   * class. marmote3::subcarrier_allocator::make is the public interface for
   * creating new instances.
   */
  static sptr make(int vlen, const std::vector<int> &active_channels,
                   const std::vector<int> &backup_channels, int max_xmitting,
                   vector_peak_probe::sptr cca_peak_probe,
                   const std::vector<int> &subcarrier_map,
                   modem_sender2::sptr modem_sender, bool debug_scheduler);

  gr::block *get_this_block() override { return this; }

  virtual void set_active_channels(const std::vector<int> &active_channels) = 0;
  virtual void set_backup_channels(const std::vector<int> &backup_channels) = 0;
};

} // namespace marmote3
} // namespace gr

#endif /* INCLUDED_MARMOTE3_SUBCARRIER_ALLOCATOR_H */
