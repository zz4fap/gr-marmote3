/* -*- c++ -*- */
/* 
 * Copyright 2017 Peter Horvath.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_MARMOTE3_SCENARIO_EMULATOR_SIMPLE_H
#define INCLUDED_MARMOTE3_SCENARIO_EMULATOR_SIMPLE_H

#include <marmote3/api.h>
#include <gnuradio/sync_block.h>

namespace gr
{
namespace marmote3
{

/*!
     * \brief Simple scenario emulator
     * \ingroup marmote3
     *
     */
class MARMOTE3_API scenario_emulator_simple : virtual public gr::sync_block
{
public:
  typedef boost::shared_ptr<scenario_emulator_simple> sptr;

  /*!
       * \brief Return a shared_ptr to a new instance of marmote3::scenario_emulator_simple.
       *
       * To avoid accidental use of raw pointers, marmote3::scenario_emulator_simple's
       * constructor is in a private implementation
       * class. marmote3::scenario_emulator_simple::make is the public interface for
       * creating new instances.
       */
  static sptr make(float fs, float fdopp, float update_interval, float K_fact = 0.0, int no_sines = 8);
};

} // namespace marmote3
} // namespace gr

#endif /* INCLUDED_MARMOTE3_SCENARIO_EMULATOR_SIMPLE_H */
