/*
 * Copyright (c) 2018 Team MarmotE
 * Author: Miklos Maroti
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

syntax = "proto3";
package gr.marmote3;

import "google/protobuf/wrappers.proto";

message PerformanceCounter {
  string alias = 1;
  float pc_work_time_total = 2;
  float pc_work_time_avg = 3;
  float pc_noutput_items = 6;
  float pc_noutput_items_avg = 7;
  float pc_nproduced = 8;
  float pc_nproduced_avg = 9;
  int32 work_counter = 26;
  repeated float pc_input_buffers_full_avg = 4; // one per input
  repeated uint64 input_nitems_read = 10;
  repeated int32 input_buff_size = 14;
  repeated int32 input_item_size = 15;
  repeated int32 input_items_min = 22;
  repeated int32 input_items_max = 23;
  repeated float pc_output_buffers_full_avg = 5; // one per output
  repeated uint64 output_nitems_written = 12;
  repeated int32 output_buff_size = 16;
  repeated int32 output_item_size = 17;
  repeated int32 output_space_min = 24;
  repeated int32 output_space_max = 25;
}

message HeaderCompression {
  uint32 valid_messages = 1;
  uint32 invalid_messages = 2;
  uint64 uncompressed_bytes = 3;
  uint64 compressed_bytes = 4;
}

message TestMessageSource { uint32 produced = 1; }

message TestMessageSink {
  uint32 received = 2;
  uint32 dropped = 3;
}

message PacketFlowReport {
  repeated uint32 packets = 1;
  uint32 dropped = 2;
}

/**
 * The histogram contains the number of times the measured average power was at
 * the given power level. The first entry corresponds to the power level below
 * histogram_low_db, then every entry corresponds to one level higher by
 * histogram_step_db.
 */
message PeakProbeCarrier {
  float average_db = 1;
  float maximum_db = 2;
  repeated uint32 histogram = 3;
}

/**
 * Assuming we have 16 channels (numbered from 0 till 15) and 20 subcarriers
 * (numbered from 0 till 19), then we have the following correspondences:
 * chan_to_subc = [12, 13, 14, 15, 16, 17, 18, 19, 0, 1, 2, 3, 4, 5, 6, 7]
 * subc_to_chan = [8, 9, 10, 11, 12, 13, 14, 15, -1, -1, -1, -1,
 *                -1, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7]
 */
message PeakProbeReport {
  uint64 samples = 1;
  repeated PeakProbeCarrier carriers = 2;
  float histogram_low_db = 3;  // lowest measured power
  float histogram_step_db = 4; // positive step db value
}

message ModemSenderFlow {
  sint32 source = 1;      // key, radio id
  sint32 destination = 2; // key, radio id
  sint32 flowid = 3;      // key, dst port

  float max_latency_s = 14;      // this is the mandate
  float max_throughput_bps = 15; // this is the mandate
  bool is_file_mandate = 22;     // this is the mandate
  uint32 resend_count = 24;      // this is the mandate

  uint32 packets_load = 10;     // offered load
  uint32 packets_sent = 11;     // packets sent
  uint32 packets_dropped = 12;  // packets dropped
  uint32 packets_requeued = 19; // requeued (missing) packets

  uint64 payload_load = 16;     // payload bytes offered
  uint64 payload_sent = 17;     // payload bytes sent
  uint64 payload_dropped = 18;  // payload bytes dropped
  uint64 payload_requeued = 20; // requeued (missing) paylod bytes

  uint32 queued_packets = 13; // current queue size
  uint64 queued_payload = 21; // current queue size in bytes
  float queued_max_wait = 23; // oldest unsent packet
  uint64 unsent_payload = 25; // unsent payload bytes in queue
}

message ModemSenderMcs {
  sint32 destination = 1; // key, radio_id

  sint32 mcs = 2; // mcs index (or -1 for default value)
}

/**
 * Specifies the performance metrics for the specific flow. Packets that cannot
 * be sent under the max_latency_s requirement will be dropped. The
 * max_throughput_bps field limits the amount of messages sent (counting only
 * UDP/TCP payload sizes), when there is higher load than we need to forward.
 * If file mandate is true, then this limit applies to each arrival time
 * measurement period. If the file mandate is false, then this limit applies
 * to each sending time measurement period.
 */
message ModemSenderMandate {
  sint32 flowid = 1; // key

  float max_latency_s = 10;      // -1.0 if not specified
  float max_throughput_bps = 11; // -1.0 if not specified
  bool is_file_mandate = 12;
  uint32 resend_count = 13;      // zero means single send
}

message ModemSenderReport {
  repeated ModemSenderFlow flows = 1;
  sint32 default_mcs = 2;
  repeated ModemSenderMcs mcs_map = 3;
  float default_max_latency_s = 4;
  float default_max_throughput_bps = 5;
  bool default_is_file_mandate = 8;
  repeated ModemSenderMandate mandates = 6;
  uint32 mcs_count = 7; // number MCS values
}

message ModemReceiverFlow {
  sint32 source = 1;      // key, radio id
  sint32 destination = 2; // key, radio id
  sint32 flowid = 3;      // key, dst port
  sint32 channel = 4;     // key
  sint32 mcs = 5;         // key
  bool crc = 6;           // key, passed CRC

  uint32 packets = 10; // number of packets
  double power = 11;   // total signal power from equalizer (divide by packets)
  double snr = 12;     // total signal srn from equalizer
  double freq_err0 = 13; // total freq error of preamble
  double freq_err1 = 14; // total freq error of packet
  double phase_err = 15; // total phase error

  // reserved for latency
  sint64 bytes = 17; // total received bytes in period

  uint32 packets_missing = 19; // some of the detected missing packets
}

message ModemReceiverRate {
  sint32 mcs = 1; // key
  sint32 snr = 2; // key, rounded to nearest integer

  uint32 passed = 10; // number of packets passed CRC check
  uint32 failed = 11; // number of packets failed CRC check
}

message ModemReceiverReport {
  repeated ModemReceiverFlow flows = 1; // flow based statistics
  repeated ModemReceiverRate rates = 2; // packet error rates
}

message SubcarrierAllocatorFlow {
  sint32 channel = 1;     // key
  sint32 source = 2;      // key
  sint32 destination = 3; // key
  sint32 mcs = 4;         // key

  uint64 frames = 10;     // total number of triggered frames
  uint32 length_min = 11; // minimum frame length in samples
  uint32 length_max = 12; // maximum frame length in samples
  int32 cca_bin_min = 13; // minimum CCA bin from peak probe
  int32 cca_bin_max = 14; // maximum CCA bin from peak probe
}

message SubcarrierAllocatorInhibit {
  double start_time = 1;  // start time of inhibit
  float duration = 2;     // duration of inhibit
}

message SubcarrierAllocatorReport {
  repeated SubcarrierAllocatorFlow flows = 1;
  repeated sint32 active_channels = 2;
  repeated sint32 backup_channels = 3;
  repeated SubcarrierAllocatorInhibit inhibits = 5;
}

message SubcarrierSerializerFlow {
  sint32 channel = 1; // key

  uint64 frames = 10;     // total number of triggered frames
  uint64 dropped = 11;    // number of dropped frames
  float trigger_avg = 12; // average trigger value
  uint32 length_min = 13; // minimum frame length in samples
  uint32 length_max = 14; // maximum frame length in samples
}

message SubcarrierSerializerReport {
  repeated SubcarrierSerializerFlow flows = 1;
}

message ModemControlReport {
  double center_freq = 1;
  double samp_rate = 2;
  float rx_gain = 3;
  float tx_gain = 4;  // static baseline tx power
  float tx_power = 5; // non-glich tx gain offset
  bool invert_spectrum = 6;
  bool enable_recording = 7;
  double rf_bandwidth = 8;
  sint32 num_channels = 10; // usable subcarriers
  bool enable_arq = 11;
  sint32 num_subcarriers = 12; // FBMC FFT length
  sint32 radio_id = 13;        // used for ARQ
  float nominal_rx_gain = 14;
  float nominal_tx_gain = 15;
}

message PreambleEqualizer {
  uint32 total_packets = 1;
  uint32 header_crc_error = 2;
  uint32 invalid_mcs_index = 3;
  uint32 invalid_data_len = 4;
  uint32 frame_too_short = 5;
  uint32 frame_too_long = 6;
  uint32 frame_truncated = 7;
}

message KeepSegmentsReport {
  sint32 report_id = 1;
  uint64 consumed = 2;
  uint64 produced = 3;
}

message RadarDetectorItem {
  uint32 offset = 1;    // burst sample offset
  uint32 magnitude = 2; // burst magnitude
}

message RadarDetectorReport {
  uint64 start = 1; // start sample
  uint32 count = 2; // number of samples in report
  repeated RadarDetectorItem items = 3;
}

message ModemReport {
  double time = 3;        // when the report was created
  uint32 sequence_no = 4; // sequence number

  repeated PerformanceCounter performance_counters = 10;
  HeaderCompression header_compressor = 11;
  HeaderCompression header_decompressor = 12;
  TestMessageSource test_message_source = 13;
  TestMessageSink test_message_sink = 14;
  PacketFlowReport packet_flow_splitter = 15;
  PreambleEqualizer preamble_equalizer = 27;
  PacketFlowReport packet_flow_merger = 16;
  PeakProbeReport receive_peak_probe = 17;
  PeakProbeReport transmit_peak_probe = 26;
  PeakProbeReport single_peak_probe = 19;
  ModemSenderReport modem_sender = 22;
  ModemReceiverReport modem_receiver = 18;
  SubcarrierAllocatorReport subcarrier_allocator = 21;
  SubcarrierSerializerReport subcarrier_serializer = 20;
  ModemControlReport modem_control = 25;
  repeated KeepSegmentsReport keep_segments = 28;
  RadarDetectorReport radar_detector = 29;
}

message ModemControlCommand {
  google.protobuf.DoubleValue center_freq = 1;
  // google.protobuf.DoubleValue samp_rate = 2;
  google.protobuf.FloatValue rx_gain = 3;
  google.protobuf.FloatValue tx_gain = 4;
  google.protobuf.FloatValue tx_power = 5;
  google.protobuf.BoolValue invert_spectrum = 6;
  google.protobuf.BoolValue enable_recording = 7;
  google.protobuf.DoubleValue rf_bandwidth = 8; // actual value can get smaller
  google.protobuf.BoolValue enable_arq = 11;
}

message SubcarrierAllocatorCommand {
  repeated sint32 active_channels = 1;
  repeated sint32 backup_channels = 2; // if active channels are congested
}

message SubcarrierInhibitCommand {
  repeated SubcarrierAllocatorInhibit inhibits = 1; // must be ordered by start time
}

message ModemSenderMandateCommand { repeated ModemSenderMandate mandates = 1; }

message ModemSenderMcsCommand { repeated ModemSenderMcs assignment = 1; }

message ModemCommand {
  ModemControlCommand modem_control = 14;
  SubcarrierAllocatorCommand subcarrier_allocator = 15; // full replacement
  ModemSenderMandateCommand set_mandates = 16;          // full replacement
  ModemSenderMcsCommand set_mcs = 18;                   // full replacement
  SubcarrierInhibitCommand subcarrier_inhibit = 19;     // full replacement
}
