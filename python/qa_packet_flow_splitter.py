#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2017 Miklos Maroti.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

from gnuradio import gr, gr_unittest
from gnuradio import blocks
import marmote3_swig as marmote3


class qa_packet_flow_splitter (gr_unittest.TestCase):

    def gen_test1(self, port):
        data = range(50000)
        top = gr.top_block()
        splitter = marmote3.packet_flow_splitter(gr.sizeof_int, 2, 100)
        splitter.set_max_output_buffer(2048)
        sink = blocks.vector_sink_i()
        top.connect(
            blocks.vector_source_i(data, False),
            blocks.stream_to_tagged_stream(
                gr.sizeof_int, 1, 100, "packet_len"),
            splitter
        )
        top.connect(
            (splitter, port),
            blocks.throttle(gr.sizeof_int, 1e4),
            blocks.null_sink(gr.sizeof_int)
        )
        top.connect((splitter, 1 - port), sink)
        top.run()
        print len(sink.data())
        self.assertTrue(len(sink.data()) > 40000)

    def test0(self):
        self.gen_test1(0)

    def test1(self):
        self.gen_test1(1)

    def gen_test2(self, total):
        data = range(total * 100)
        top = gr.top_block()
        splitter = marmote3.packet_flow_splitter(gr.sizeof_int, 2, 100)
        sink1 = blocks.vector_sink_i()
        sink2 = blocks.vector_sink_i()
        top.connect(
            blocks.vector_source_i(data, False),
            blocks.stream_to_tagged_stream(
                gr.sizeof_int, 1, 100, "packet_len"),
            splitter
        )
        top.connect((splitter, 0), sink1)
        top.connect((splitter, 1), sink2)
        top.run()
        print len(sink1.data()), len(sink2.data())
        self.assertTrue(len(sink1.data()) + len(sink2.data()) == total * 100)

    def xtest2(self):
        self.gen_test2(100)

    def xtest3(self):
        self.gen_test2(1)

    def xtest4(self):
        self.gen_test2(0)


if __name__ == '__main__':
    gr_unittest.run(qa_packet_flow_splitter, "qa_packet_flow_splitter.xml")
