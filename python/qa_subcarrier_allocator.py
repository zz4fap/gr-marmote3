#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2017 Miklos Maroti.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

from gnuradio import gr, gr_unittest
from gnuradio import blocks
import numpy as np
import marmote3_swig as marmote3


class qa_subcarrier_allocator (gr_unittest.TestCase):

    def test1(self):
        top = gr.top_block()
        sink = blocks.vector_sink_c()
        top.connect(
            blocks.vector_source_c(
                [1, 2, 3, 4, 5, 6], False),
            blocks.stream_to_tagged_stream(
                gr.sizeof_gr_complex, 1, 2, "packet_len"),
            marmote3.subcarrier_allocator(
                3, [0, 1], [], 2, marmote3.vector_peak_probe_sptr(), [],
                marmote3.modem_sender2_sptr(), False),
            blocks.vector_to_stream(gr.sizeof_gr_complex, 3),
            blocks.head(gr.sizeof_gr_complex, 1024 * 200),
            sink)
        top.run()
        d = list(sink.data())
        try:
            i = d.index(1)
        except ValueError:
            print "DID NOT PRODUCE"
            self.assertTrue(d == [0] * len(d))
            self.assertTrue(False)

        self.assertTrue(i >= 0)
        d = d[i:i + 15]
        print i, d
        self.assertTrue(
            np.allclose(d, [1, 3, 0, 2, 4, 0, 5, 0, 0, 6, 0, 0, 0, 0, 0]))


if __name__ == '__main__':
    gr_unittest.run(qa_subcarrier_allocator, "qa_subcarrier_allocator.xml")
