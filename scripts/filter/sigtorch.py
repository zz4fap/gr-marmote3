#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2018 Miklos Maroti.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

import math
import numpy as np
import scipy.signal as signal
import torch
import torch.autograd as autograd


DEVICE = torch.device('cuda') if torch.cuda.is_available() \
    else torch.device('cpu')


def is_complex(comp):
    return comp.dim() > 0 and comp.size(-1) == 2


def real(comp):
    assert is_complex(comp)
    return comp.select(-1, 0)


def imag(comp):
    assert is_complex(comp)
    return comp.select(-1, 1)


def complex(real=None, imag=None):
    if imag is None:
        imag = torch.zeros_like(real)
    elif real is None:
        real = torch.zeros_like(imag)
    assert real.dim() == imag.dim()
    return torch.stack([real, imag], dim=real.dim())


def mul_cc(comp1, comp2):
    assert is_complex(comp1) and is_complex(comp2)
    real1 = real(comp1)
    imag1 = imag(comp1)
    real2 = real(comp2)
    imag2 = imag(comp2)
    real3 = (real1 * real2) - (imag1 * imag2)
    imag3 = (real1 * imag2) + (imag1 * real2)
    return complex(real3, imag3)


def mul_cr(comp1, real2):
    assert is_complex(comp1)
    return comp1 * real2.unsqueeze(-1)


def conj(comp):
    assert is_complex(comp)
    return complex(real(comp), -imag(comp))


def abs2(comp):
    return (comp * comp).sum(dim=-1)


def exp(comp):
    assert is_complex(comp)
    r = real(comp)
    i = imag(comp)
    return r.exp().unsqueeze(-1) * complex(i.cos(), i.sin())


class FftFunction(autograd.Function):

    @staticmethod
    def forward(ctx, c, dim=0, norm="ortho"):
        assert is_complex(c) and 0 <= dim and dim < c.dim() - 1
        ctx.dim = dim
        ctx.norm = norm
        d = np.ascontiguousarray(c.detach().numpy())
        if d.dtype == np.float32:
            d = d.view(dtype=np.complex64)
            d = np.fft.fft(d, axis=dim, norm=norm)
            d = d.astype(np.complex64, order="C")
            d = d.view(dtype=np.float32)
        elif d.dtype == np.float64:
            d = d.view(dtype=np.complex128)
            d = np.fft.fft(d, axis=dim, norm=norm)
            d = d.astype(np.complex128, order="C")
            d = d.view(dtype=np.float64)
        else:
            raise TypeError()
        return torch.from_numpy(d)

    @staticmethod
    def backward(ctx, c):
        if ctx.norm == "ortho":
            return IfftFunction.apply(c, ctx.dim, "ortho"), None, None
        else:
            return IfftFunction.apply(c, ctx.dim, None) * c.size(ctx.dim), None, None


class IfftFunction(autograd.Function):

    @staticmethod
    def forward(ctx, c, dim=0, norm="ortho"):
        assert is_complex(c) and 0 <= dim and dim < c.dim() - 1
        ctx.dim = dim
        ctx.norm = norm
        d = np.ascontiguousarray(c.detach().numpy())
        if d.dtype == np.float32:
            d = d.view(dtype=np.complex64)
            d = np.fft.ifft(d, axis=dim, norm=norm)
            d = d.astype(np.complex64, order="C")
            d = d.view(dtype=np.float32)
        elif d.dtype == np.float64:
            d = d.view(dtype=np.complex128)
            d = np.fft.ifft(d, axis=dim, norm=norm)
            d = d.astype(np.complex128, order="C")
            d = d.view(dtype=np.float64)
        else:
            raise TypeError()
        return torch.from_numpy(d)

    @staticmethod
    def backward(ctx, c):
        if ctx.norm == "ortho":
            return FftFunction.apply(c, ctx.dim, "ortho"), None, None
        else:
            return FftFunction.apply(c, ctx.dim, None) / c.size(ctx.dim), None, None


if True:
    def fft(comp, dim=0, norm="ortho"):
        assert is_complex(comp)
        return torch.fft(comp, signal_ndim=1, normalized=(norm == "ortho"))

    def ifft(comp, dim=0, norm="ortho"):
        assert is_complex(comp)
        return torch.ifft(comp, signal_ndim=1, normalized=(norm == "ortho"))
else:
    fft = FftFunction.apply
    ifft = IfftFunction.apply

if False:
    print(autograd.gradcheck(fft, (autograd.Variable(
        torch.randn([4, 3, 2]).double(), requires_grad=True),)))


def sfft(r, dim=0, norm="ortho"):
    """Performs FFT/IFFT for a symmetric vector of reals."""
    return real(fft(complex(r, None), dim, norm))


def pad(sig, left=0, right=0, dim=0):
    assert left >= 0 and right >= 0
    if left <= 0 and right <= 0:
        return sig
    tensors = []
    size = list(sig.size())
    if left > 0:
        size[dim] = left
        zero = torch.zeros(size).double().to(DEVICE)
        if isinstance(sig, autograd.Variable):
            zero = autograd.Variable(zero)
        tensors.append(zero)
    tensors.append(sig)
    if right > 0:
        size[dim] = right
        zero = torch.zeros(size).double().to(DEVICE)
        if isinstance(sig, autograd.Variable):
            zero = autograd.Variable(zero)
        tensors.append(zero)
    return torch.cat(tensors, dim=dim)


def convolve_rr(vec1, vec2, dim=-1):
    """Calculates the full convolution of real vectors."""
    assert dim < 0 and -dim <= vec1.dim() and -dim <= vec2.dim()
    # print(vec1.size(), vec2.size())
    vec2 = pad(vec2, vec1.size(dim) - 1, vec1.size(dim) - 1, dim=dim)
    vec2 = vec2.unfold(dim, vec1.size(dim), 1)
    if dim == -1:
        vec1 = vec1.unsqueeze(-2)
    else:
        vec1 = vec1.unsqueeze(-1)
        vec1 = vec1.transpose(-1, dim - 1)
    return (vec1 * vec2).sum(-1)


def decimate(sig, rate, dim=0, offset=None):
    """Return the decimated signal so that samples at offset are included."""
    assert rate >= 1
    if offset is None:
        assert sig.size(dim) % 2 == 1
        offset = (sig.size(dim) - 1) / 2
    offset = offset % rate
    if offset != 0:
        sig = sig.narrow(dim, offset, sig.size(dim) - offset)
    return sig.unfold(dim, 1, rate)


def interpolate(sig, rate, dim=0):
    assert rate >= 1
    if rate <= 1:
        return sig
    idx = [0] * ((sig.size(dim) - 1) * rate + 1)
    idx[0::rate] = range(1, sig.size(dim) + 1)
    idx = torch.LongTensor(idx).to(DEVICE)
    if isinstance(sig, autograd.Variable):
        idx = autograd.Variable(idx)
    sig = pad(sig, left=1, dim=dim)
    return sig.index_select(dim, idx)


def firwin(numtaps, cutoff, window="hamming"):
    """Returns the given bandpass filter."""
    taps = signal.firwin(numtaps, cutoff, window=window)
    taps /= math.sqrt(np.sum(np.square(taps)))
    return torch.from_numpy(taps).to(DEVICE)


def log10(sig):
    return (1.0 / math.log(10.0)) * torch.log(sig)


def normalize(sig, dim=0):
    return sig / torch.sum(sig * sig, dim=dim, keepdim=True).sqrt()
