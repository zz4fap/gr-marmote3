#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2017 Peter Horvath.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

"""
This module checks the performance of the BPSK, QPSK, 8PSK, 16QAM and 64QAM
modulation and demdulation with a simple flowgraph.
"""

from __future__ import print_function

import marmote3
from marmote3 import comm_performance
import numpy as np
import matplotlib.pyplot as plt

from test_ber_soft_demod import test_ber_soft_demod


def simulate_ber(snrs, M, repetition, data_len):

    fg = test_ber_soft_demod(data_len, M, repetition, snrs)
    fg.run()
    return fg.marmote3_modem_bert_receiver_0.get_ber()


class test_softbit_acc(object):

    def test_bpsk(self):  # BPSK
        snrs = np.arange(0, 8)
        ber_sim = np.zeros((len(snrs), ))
        rep = 2
        for idx, snr in enumerate(snrs):
            ber_sim[idx] = simulate_ber(snr, 1, rep, 1500)

        ber_theor = marmote3.calc_mpsk_awgn_ber_vs_ebn0(
            2, 10**(snrs / 10.0))
        ber_theor_nx = marmote3.calc_mpsk_awgn_ber_vs_ebn0(
            2, rep * 10**(snrs / 10.0))

        plt.figure()
        plt.semilogy(snrs, ber_sim, 'b-o')
        plt.semilogy(snrs, ber_theor, 'r--')
        plt.semilogy(snrs, ber_theor_nx, 'g--')

        plt.legend(('sim', 'theor.', 'theor. rep.'))
        plt.title('BPSK')
        plt.grid()
        plt.show()

    def test_qpsk(self):  # QPSK
        snrs = np.arange(0, 12)
        ber_sim = np.zeros((len(snrs), ))
        rep = 2
        for idx, snr in enumerate(snrs):
            ber_sim[idx] = simulate_ber(snr, 2, rep, 1500)

        ber_theor = marmote3.calc_mpsk_awgn_ber_vs_ebn0(
            4, 10**(snrs / 10.0) / 2)
        ber_theor_nx = marmote3.calc_mpsk_awgn_ber_vs_ebn0(
            4, rep * 10**(snrs / 10.0) / 2)

        plt.figure()
        plt.semilogy(snrs, ber_sim, 'b-o')
        plt.semilogy(snrs, ber_theor, 'r--')
        plt.semilogy(snrs, ber_theor_nx, 'g--')
        plt.legend(('sim', 'theor.', 'theor. rep.'))
        plt.title('QPSK')
        plt.grid()
        plt.show()

    def test_8psk(self):  # 8PSK
        snrs = np.arange(7, 18)
        ber_sim = np.zeros((len(snrs), ))
        rep = 2
        for idx, snr in enumerate(snrs):
            ber_sim[idx] = simulate_ber(snr, 3, rep, 1500)

        ber_theor = marmote3.calc_mpsk_awgn_ber_vs_ebn0(
            8, 10**(snrs / 10.0) / 3)
        ber_theor_nx = marmote3.calc_mpsk_awgn_ber_vs_ebn0(
            8, rep * 10**(snrs / 10.0) / 3)

        plt.figure()
        plt.semilogy(snrs, ber_sim, 'b-o')
        plt.semilogy(snrs, ber_theor, 'r--')
        plt.semilogy(snrs, ber_theor_nx, 'g--')
        plt.legend(('sim', 'theor.', 'theor. rep.'))
        plt.title('8PSK')
        plt.grid()
        plt.show()

    def test_16qam(self):  # 16QAM
        snrs = np.arange(5, 20)
        ber_sim = np.zeros((len(snrs), ))
        rep = 2
        for idx, snr in enumerate(snrs):
            ber_sim[idx] = simulate_ber(snr, 4, rep, 1500)

        ber_theor = marmote3.calc_qam_awgn_ber_vs_ebn0(
            16, 10**(snrs / 10.0) / 4)
        ber_theor_nx = marmote3.calc_qam_awgn_ber_vs_ebn0(
            16, rep * 10**(snrs / 10.0) / 4)

        plt.figure()
        plt.semilogy(snrs, ber_sim, 'b-o')
        plt.semilogy(snrs, ber_theor, 'r--')
        plt.semilogy(snrs, ber_theor_nx, 'g--')
        plt.legend(('sim', 'theor.', 'theor. rep.'))
        plt.title('16QAM')
        plt.grid()
        plt.show()

    def test_64qam(self):  # 64QAM
        snrs = np.arange(15, 26)
        ber_sim = np.zeros((len(snrs), ))
        ber_sim_2x = np.zeros((len(snrs), ))
        ber_sim_3x = np.zeros((len(snrs), ))
        for idx, snr in enumerate(snrs):
            ber_sim[idx] = simulate_ber(snr, 6, 1, 1500)
            ber_sim_2x[idx] = simulate_ber(snr, 6, 2, 1500)
            ber_sim_3x[idx] = simulate_ber(snr, 6, 3, 1500)

        ber_theor = marmote3.calc_qam_awgn_ber_vs_ebn0(
            64, 10**(snrs / 10.0) / 6)
        ber_theor_2x = marmote3.calc_qam_awgn_ber_vs_ebn0(
            64, 2 * 10**(snrs / 10.0) / 6)
        ber_theor_3x = marmote3.calc_qam_awgn_ber_vs_ebn0(
            64, 3 * 10**(snrs / 10.0) / 6)

        plt.figure()
        plt.semilogy(snrs, ber_sim, 'b-o', label="sim")
        plt.semilogy(snrs, ber_sim_2x, 'r-o', label="sim 2x")
        plt.semilogy(snrs, ber_sim_3x, 'g-o', label="sim 3x")
        plt.semilogy(snrs, ber_theor, 'b--', label="theory")
        plt.semilogy(snrs, ber_theor_2x, 'r--', label="theory 2x")
        plt.semilogy(snrs, ber_theor_3x, 'g--', label="theory 3x")
        plt.legend()
        plt.title('64QAM')
        plt.grid()
        plt.show()

    def test_all(self):
        snrs = np.arange(10, 30 + 1)
        rep = 1
        # ber_sim_bpsk = np.zeros((len(snrs), ))
        ber_sim_qpsk = np.zeros((len(snrs), ))
        ber_sim_8psk = np.zeros((len(snrs), ))
        ber_sim_16qam = np.zeros((len(snrs), ))
        ber_sim_64qam = np.zeros((len(snrs), ))
        for idx, snr in enumerate(snrs):
            print("SNR:", snr)
            # ber_sim_bpsk[idx] = simulate_ber(snr, 1, rep, 1500)
            ber_sim_qpsk[idx] = simulate_ber(snr, 2, rep, 1500)
            ber_sim_8psk[idx] = simulate_ber(snr, 3, rep, 1500)
            ber_sim_16qam[idx] = simulate_ber(snr, 4, rep, 1500)
            ber_sim_64qam[idx] = simulate_ber(snr, 6, rep, 1500)

        plt.figure()
        # plt.semilogy(snrs, ber_sim_bpsk, 'b-o', label="BPSK sim")
        plt.semilogy(snrs, ber_sim_qpsk, 'r-o', label="QPSK sim")
        plt.semilogy(snrs, comm_performance.calc_mpsk_awgn_ber_vs_ebn0(
            4, 10**(snrs/10.0)/2.0), 'r-.', label="QPSK th.")
        plt.semilogy(snrs, ber_sim_8psk, 'g-o', label="8PSK sim")
        plt.semilogy(snrs, comm_performance.calc_mpsk_awgn_ber_vs_ebn0(
            8, 10**(snrs/10.0)/3.0), 'g-.', label="8PSK th.")
        plt.semilogy(snrs, ber_sim_16qam, 'k-o', label="16QAM sim")
        plt.semilogy(snrs, comm_performance.calc_qam_awgn_ber_vs_ebn0(
            16, 10**(snrs/10.0)/4.0), 'k-.', label="16QAM approx.")
        plt.semilogy(snrs, ber_sim_64qam, 'c-o', label="64QAM sim")
        plt.semilogy(snrs, comm_performance.calc_qam_awgn_ber_vs_ebn0(
            64, 10**(snrs/10.0)/6.0), 'c-.', label="64QAM approx.")
        plt.xlabel("SNR (dB)")
        plt.ylabel("BER")
        plt.ylim([1e-7, 0.5])
        plt.legend()
        plt.grid()
        plt.show()


if __name__ == '__main__':
    qa = test_softbit_acc()
    qa.test_all()
    # qa.test_64qam()
