/* -*- c++ -*- */
/*
 * Copyright 2017 Peter Horvath, Miklos Maroti
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "packet_rep_encoder_impl.h"
#include <gnuradio/io_signature.h>
#include <random>

namespace gr {
namespace marmote3 {

packet_rep_encoder::sptr packet_rep_encoder::make(int data_len,
                                                  int repetition) {
  return gnuradio::get_initial_sptr(
      new packet_rep_encoder_impl(data_len, repetition));
}

packet_rep_encoder_impl::packet_rep_encoder_impl(int data_len, int repetition)
    : tagged_stream_block2("packet_rep_encoder",
                           gr::io_signature::make(1, 1, sizeof(unsigned char)),
                           gr::io_signature::make(1, 1, sizeof(unsigned char)),
                           data_len * repetition),
      repetition(repetition) {}

packet_rep_encoder_impl::~packet_rep_encoder_impl() {}

int packet_rep_encoder_impl::work(int noutput_items,
                                  gr_vector_int &ninput_items,
                                  gr_vector_const_void_star &input_items,
                                  gr_vector_void_star &output_items) {
  const uint8_t *in = (const uint8_t *)input_items[0];
  uint8_t *out = (uint8_t *)output_items[0];
  int len = ninput_items[0];
  
  if (len * repetition > noutput_items) {
    std::cout << "####### packet_rep_encoder: packet is too long\n";
    return 0;
  }

  std::minstd_rand engine(1); // TODO: set seed
  std::uniform_int_distribution<> rand_byte(0, 255);

  for (int i = 0; i < len; ++i) {
    uint8_t a = *(in++);
    for (int r = 0; r < repetition; ++r)
      *(out++) = a ^ rand_byte(engine);
  }

  return len * repetition;
}

} /* namespace marmote3 */
} /* namespace gr */
