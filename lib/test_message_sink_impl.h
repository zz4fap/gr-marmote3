/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_MARMOTE3_TEST_MESSAGE_SINK_IMPL_H
#define INCLUDED_MARMOTE3_TEST_MESSAGE_SINK_IMPL_H

#include <marmote3/test_message_sink.h>
#include <unordered_set>
#include <vector>

namespace gr {
namespace marmote3 {

class test_message_sink_impl : public test_message_sink {
private:
  const int framing_type;

  int received = 0;
  int dropped = 0;
  int total_received = 0;
  int total_dropped = 0;
  gr::thread::mutex mutex;

  bool check_payload(const uint8_t *packet_ptr, int packet_len,
                     int payload_len);

public:
  test_message_sink_impl(int framing_type);
  ~test_message_sink_impl();

  void receive(pmt::pmt_t msg);
  bool stop() override;

  void collect_block_report(ModemReport *modem_report) override;
};

} // namespace marmote3
} // namespace gr

#endif /* INCLUDED_MARMOTE3_TEST_MESSAGE_SINK_IMPL_H */
