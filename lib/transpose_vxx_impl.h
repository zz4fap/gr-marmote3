/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_MARMOTE3_TRANSPOSE_VXX_IMPL_H
#define INCLUDED_MARMOTE3_TRANSPOSE_VXX_IMPL_H

#include <marmote3/transpose_vxx.h>

namespace gr {
namespace marmote3 {

class transpose_vxx_impl : public transpose_vxx {
private:
  const int item_size;
  const int in_vlen;
  const int out_vlen;
  const int block_size;

public:
  transpose_vxx_impl(int item_size, int in_vlen, int out_vlen);
  ~transpose_vxx_impl();

  int fixed_rate_ninput_to_noutput(int ninput);
  int fixed_rate_noutput_to_ninput(int noutput);
  void forecast(int noutput_items, gr_vector_int &ninput_items_required);

  int general_work(int noutput_items, gr_vector_int &ninput_items,
                   gr_vector_const_void_star &input_items,
                   gr_vector_void_star &output_items);

private:
  template <class elem_t> void transpose_4x4(const elem_t *in, elem_t *out);
  template <class elem_t> void transpose(const elem_t *in, elem_t *out);

  void transpose_complex(const gr_complex *in, gr_complex *out);
};

} // namespace marmote3
} // namespace gr

#endif /* INCLUDED_MARMOTE3_TRANSPOSE_VXX_IMPL_H */
