/* -*- c++ -*- */
/*
 * Copyright 2017-2018 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <algorithm>
#include <arpa/inet.h>
#include <boost/crc.hpp>
#include <boost/thread/thread.hpp>
#include <gnuradio/io_signature.h>
#include <limits>
#include <sstream>

#include "modem_sender2_impl.h"
#include <marmote3/constants.h>
#include <marmote3/marmote3.pb.h>

namespace gr {
namespace marmote3 {

const int MAC_BROADCAST_FLOWID = 4776;
const float MAX_LATENCY_S = 180.0f;
const float MAX_THROUGHPUT_BPS = 100e6f;
const int MAX_LATE_MESSAGE_TIME_MS = 20; // not really missing, just delayed
const int MAX_MISSING_REPORTS = 10;
const int MAX_MCS_BOOST_LEVEL = 4;
const int MAX_MCS_BOOST_COUNT = 5;
const int INIT_ROUND_TRIP_MS = 100;
const int EXTRA_ROUND_TRIP_MS = 30;
const int URGENT_CUTOFF_TIME_MS = 250;
const int MINIMUM_ONEWAY_TIME_MS = 30; // min time for signal processing
const int RANDOM_DELAY_TRIGGER_COUNT = 5;

modem_sender2::sptr modem_sender2::make(
    int max_data_len, int max_code_len, int max_symb_len, int max_queue_size,
    int default_mcs, float default_max_latency_s,
    float default_max_throughput_bps, bool default_is_file_mandate,
    int payload_mode, int radio_id, const std::vector<int> &blocked_flowids,
    int max_inflight_packets, bool debug_scheduler, bool debug_arq) {
  return gnuradio::get_initial_sptr(new modem_sender2_impl(
      max_data_len, max_code_len, max_symb_len, max_queue_size, default_mcs,
      default_max_latency_s, default_max_throughput_bps,
      default_is_file_mandate, payload_mode, radio_id, blocked_flowids,
      max_inflight_packets, debug_scheduler, debug_arq));
}

modem_sender2_impl::time_point_t
modem_sender2_impl::get_time_point_floor(time_point_t time) {
  return std::chrono::system_clock::time_point(
      std::chrono::duration_cast<std::chrono::seconds>(
          time.time_since_epoch()));
}

double modem_sender2_impl::get_time_point_double(time_point_t time) {
  return 0.000001 * std::chrono::duration_cast<std::chrono::microseconds>(
                        time.time_since_epoch())
                        .count();
}

modem_sender2_impl::modem_sender2_impl(
    int max_data_len, int max_code_len, int max_symb_len, int max_queue_size,
    int default_mcs, float default_max_latency_s,
    float default_max_throughput_bps, bool default_is_file_mandate,
    int payload_mode, int radio_id, const std::vector<int> &blocked_flowids,
    int max_inflight_packets, bool debug_scheduler, bool debug_arq)
    : tagged_stream_block2("modem_sender2", gr::io_signature::make(0, 0, 0),
                           gr::io_signature::make(1, 1, sizeof(uint8_t)),
                           max_data_len),
      max_data_len(max_data_len), max_code_len(max_code_len),
      max_symb_len(max_symb_len), max_queue_size(max_queue_size),
      port_id(pmt::mp("in")), payload_mode(payload_mode),
      blocked_flowids(blocked_flowids),
      max_inflight_packets(max_inflight_packets),
      debug_scheduler(debug_scheduler), debug_arq(debug_arq),
      radio_id(radio_id), default_mcs(default_mcs), mcs_assignment(256),
      default_mandate(default_max_latency_s, default_max_throughput_bps,
                      default_is_file_mandate, 0) {
  if (max_data_len <= 0 || max_data_len % 2 != 0)
    throw std::out_of_range("modem_sender2: invalid max data length");

  if (max_code_len < max_data_len)
    throw std::out_of_range("modem_sender2: invalid max code length");

  // assuming 256 QAM
  if (max_symb_len < max_code_len)
    throw std::out_of_range("modem_sender2: invalid max symb length");

  if (max_queue_size <= 0)
    throw std::out_of_range("modem_sender2: invalid maximum queue size");

  if (payload_mode < 0 || payload_mode > 4)
    throw std::out_of_range("modem_sender2: invalid payload mode");

  if (max_inflight_packets < 0)
    throw std::out_of_range("modem_sender2: invalid max inflight packets");

  if (!blocked_flowids.empty()) {
    std::stringstream msg;
    msg << "modem_sender2: blocked flowids [";
    for (int i = 0; i < blocked_flowids.size(); i++) {
      if (i != 0)
        msg << ", ";
      msg << blocked_flowids[i];
    }
    msg << "]" << std::endl;
    std::cout << msg.str();
  }

  rand_engine.seed(radio_id +
                   std::chrono::system_clock::now().time_since_epoch().count());

  clear_mcs_assignment();

  message_port_register_in(port_id);
}

modem_sender2_impl::~modem_sender2_impl() {}

// ------- MUTEX -------

int modem_sender2_impl::work(int noutput_items, gr_vector_int &ninput_items,
                             gr_vector_const_void_star &input_items,
                             gr_vector_void_star &output_items) {
  gr::thread::scoped_lock lock(mutex);
  time_point_t now = std::chrono::system_clock::now();

  // read all messages from queue
  bool received_new_messages = false;
  for (;;) {
    pmt::pmt_t msg(delete_head_nowait(port_id));
    if (msg.get() != NULL) {
      enqueue(now, msg);
      received_new_messages = true;
    } else
      break;
  }

  arq_collect_missing_seqnos(now);

  // remove old packets
  for (flow_t &flow : flows)
    flow.remove_old_packets(now);

  if (!received_new_messages && !last_work_produced) {
    lock.unlock(); // wait up to 5 milliseconds unlocked
    pmt::pmt_t msg(delete_head_blocking(port_id, 5));
    lock.lock();

    now = std::chrono::system_clock::now();
    if (msg.get() != NULL) {
      enqueue(now, msg);
      received_new_messages = true;
    }

    arq_collect_missing_seqnos(now);
  }

  // hack, but should work just fine
  if (max_inflight_packets > 0 && inflight_packets >= max_inflight_packets) {
    lock.unlock();
    boost::this_thread::sleep_for(boost::chrono::milliseconds(1));
    return 0;
  }

  pmt::pmt_t payload = dequeue(now);
  if (!pmt::is_u8vector(payload) || selected_flow == NULL) {
    last_work_produced = false;
    return 0; // queue is really empty
  }

  size_t payload_len = 0;
  const uint8_t *payload_ptr = pmt::u8vector_elements(payload, payload_len);

  int parity = (sizeof(arq_header_t) + payload_len + sizeof(uint16_t)) % 2;
  int extra = (1 << arq_header_t::extra_width) - 2 + parity;
  std::vector<uint8_t> report;
  if (enable_arq)
    report = arq_generate_missing_report(extra);
  if (report.size() % 2 != parity)
    report.push_back(0);
  extra = report.size();

  int data_len = sizeof(arq_header_t) + extra + payload_len + sizeof(uint16_t);
  assert(data_len % 2 == 0);

  mmt_header_t mmt;
  mmt.load(payload_ptr);

  output_dict = PMT_EMPTY_DICT;
  set_output_long(PMT_PACKET_ID, ++packet_id);
  set_output_long(PMT_DATA_LEN, data_len);
  set_output_long(PMT_PACKET_SRC, mmt.src);
  set_output_long(PMT_PACKET_DST, mmt.dst);
  set_output_long(PMT_FLOW_ID, mmt.flowid);
  set_output_long(PMT_LOAD_TIME,
                  1e6 * get_time_point_double(selected_flow->selected->time));
  set_output_long(PMT_SCHD_TIME, 1e6 * get_time_point_double(now));

  uint8_t *out = (uint8_t *)output_items[0];

  arq_header_t::increment_flow_seqno(selected_flow->arq_flow_seqno);
  arq_dst_t::sent_t entry = arq_create_sent_seqno(now, mmt.dst, mmt.flowid);

  arq_header_t arq_header;
  arq_header.extra = extra;
  arq_header.flow_seqno = selected_flow->arq_flow_seqno;
  arq_header.srcdst_seqno = entry.seqno;
  std::memcpy(out, &arq_header, sizeof(arq_header_t));
  int mcs_boost = selected_flow->get_selected_mcs_boost();
  selected_flow->mark_selected_sent(entry.marker, now);

  std::memcpy(out + sizeof(arq_header_t), report.data(), extra);
  std::memcpy(out + sizeof(arq_header_t) + extra, payload_ptr, payload_len);

  boost::crc_16_type crc16;
  crc16.process_bytes(out, data_len - sizeof(uint16_t));
  uint16_t crc = crc16.checksum() ^ 0x713A;
  *(uint16_t *)(out + data_len - sizeof(uint16_t)) = htons(crc);

  int mcs = default_mcs;
  if (0 <= mmt.dst && mmt.dst < mcs_assignment.size() &&
      mcs_assignment[mmt.dst] >= 0)
    mcs = mcs_assignment[mmt.dst];

  if (mmt.dst == 255)
    mcs = std::max(0, mcs - broadcast_mcs_boost(rand_engine));
  else {
    if (enable_arq)
      mcs = std::max(0, mcs - mcs_boost);
    if (!last_work_produced)
      mcs = std::max(0, mcs - not_produced_mcs_boost(rand_engine));
  }

  mcs = get_usable_mcs_index(data_len, mcs);
  set_output_long(PMT_PACKET_MCS, mcs);

  last_work_produced = true;

  if (debug_arq) {
    std::stringstream str;
    str << alias() << ": arq " << std::fixed << std::setprecision(3)
        << get_time_point_double(now) << " sending src " << (int)mmt.src
        << " seqno " << (int)arq_header.srcdst_seqno << " flow "
        << (int)mmt.flowid << " dst " << (int)mmt.dst << " data";
    for (uint8_t a : report)
      str << " " << (int)a;
    str << std::endl;
    std::cout << str.str();
  }

  if (mmt.src != radio_id) {
    int old_radio_id = radio_id;
    radio_id = mmt.src;
    if (debug_arq) {
      std::stringstream str;
      str << alias() << ": arq switching radio id from " << old_radio_id
          << " to " << radio_id << std::endl;
      std::cout << str.str();
    }
  }

  // to simulate packet loss
  if (false) {
    if (radio_id == 101 && arq_header.srcdst_seqno % 10 >= 7)
      return 0;

    if (radio_id == 102 && arq_header.srcdst_seqno % 7 == 3)
      return 0;
  }

  inflight_packets += 1;
  return data_len;
}

void modem_sender2_impl::collect_block_report(ModemReport *modem_report) {
  ModemSenderReport *report = modem_report->mutable_modem_sender();
  ModemControlReport *report2 = modem_report->mutable_modem_control();

  gr::thread::scoped_lock lock(mutex);

  for (int index = flows.size() - 1; index >= 0; index--) {
    flow_t &flow = flows[index];

    ModemSenderFlow *entry = report->add_flows();
    entry->set_source(flow.source);
    entry->set_destination(flow.destination);
    entry->set_flowid(flow.flowid);

    entry->set_max_latency_s(flow.mandate.max_latency_s);
    entry->set_max_throughput_bps(flow.mandate.max_throughput_bps);
    entry->set_is_file_mandate(flow.mandate.is_file_mandate);
    entry->set_resend_count(flow.mandate.resend_count);

    entry->set_packets_load(flow.packets_load);
    entry->set_packets_sent(flow.packets_sent);
    entry->set_packets_dropped(flow.packets_dropped);
    entry->set_packets_requeued(flow.packets_requeued);

    entry->set_payload_load(flow.bytes_load);
    entry->set_payload_sent(flow.bytes_sent);
    entry->set_payload_dropped(flow.bytes_dropped);
    entry->set_payload_requeued(flow.bytes_requeued);

    entry->set_queued_packets(flow.packets.size());
    entry->set_queued_payload(flow.queued_bytes);
    entry->set_queued_max_wait(flow.queued_max_wait);
    entry->set_unsent_payload(flow.unsent_bytes);

    if (flow.packets.empty())
      remove_flow(index);
    else {
      flow.avg_bytes_load = flow.avg_bytes_load * 0.9f + flow.bytes_load * 0.1f;
      flow.packets_load = 0;
      flow.packets_sent = 0;
      flow.packets_dropped = 0;
      flow.packets_requeued = 0;
      flow.bytes_load = 0;
      flow.bytes_sent = 0;
      flow.bytes_dropped = 0;
      flow.bytes_requeued = 0;
    }
  }

  report->set_default_mcs(default_mcs);
  report->set_mcs_count(MCSCHEME_NUM);

  for (int dst = 0; dst < mcs_assignment.size(); dst++) {
    if (mcs_assignment[dst] >= 0) {
      ModemSenderMcs *mcs = report->add_mcs_map();
      mcs->set_destination(dst);
      mcs->set_mcs(mcs_assignment[dst]);
    }
  }

  report->set_default_max_latency_s(default_mandate.max_latency_s);
  report->set_default_max_throughput_bps(default_mandate.max_throughput_bps);
  report->set_default_is_file_mandate(default_mandate.is_file_mandate);

  for (const std::pair<int, mandate_t> &pair : mandates) {
    ModemSenderMandate *mandate = report->add_mandates();
    mandate->set_flowid(pair.first);
    mandate->set_max_latency_s(pair.second.max_latency_s);
    mandate->set_max_throughput_bps(pair.second.max_throughput_bps);
    mandate->set_is_file_mandate(pair.second.is_file_mandate);
    mandate->set_resend_count(pair.second.resend_count);
  }

  report2->set_enable_arq(enable_arq);
  report2->set_radio_id(radio_id);
}

void modem_sender2_impl::process_block_command(
    const ModemCommand *modem_command) {
  if (modem_command->has_set_mandates() || modem_command->has_set_mcs() ||
      modem_command->has_modem_control()) {
    gr::thread::scoped_lock lock(mutex);

    if (modem_command->has_set_mandates()) {
      const ModemSenderMandateCommand &command = modem_command->set_mandates();
      mandates.clear();
      for (const ModemSenderMandate &mandate : command.mandates())
        set_mandate(mandate.flowid(), mandate.max_latency_s(),
                    mandate.max_throughput_bps(), mandate.is_file_mandate(),
                    mandate.resend_count());
    }

    if (modem_command->has_set_mcs()) {
      const ModemSenderMcsCommand &command = modem_command->set_mcs();
      clear_mcs_assignment();
      for (const ModemSenderMcs &entry : command.assignment()) {
        if (0 <= entry.destination() &&
            entry.destination() < mcs_assignment.size())
          mcs_assignment[entry.destination()] =
              std::min(std::max(entry.mcs(), -1), MCSCHEME_NUM - 1);
      }
    }

    if (modem_command->has_modem_control()) {
      const ModemControlCommand &command = modem_command->modem_control();
      if (command.has_enable_arq()) {
        enable_arq = command.enable_arq().value();
      }
    }
  }
}

void modem_sender2_impl::process_received_arq_data(
    int source, int destination, int link_seqno,
    const std::vector<uint8_t> &report) {
  gr::thread::scoped_lock lock(mutex);
  time_point_t now = std::chrono::system_clock::now();

  if (debug_arq && source != radio_id) {
    std::stringstream str;
    str << alias() << ": arq " << std::fixed << std::setprecision(3)
        << get_time_point_double(now) << " process_received_arq_data src "
        << source << " seqno " << link_seqno << " dst " << destination
        << " data";
    for (uint8_t a : report)
      str << " " << (int)a;
    str << std::endl;
    std::cout << str.str();
  }

  if (destination == radio_id)
    arq_process_rcvd_seqno(now, source, link_seqno);

  if (report.size() >= 2 && enable_arq) {
    if (report[0] == radio_id && report[1] != 0)
      arq_reschedule_recent(now, source, report[1]);

    int r = 0;
    for (int i = 2; i < report.size(); i++) {
      uint8_t a = report[i];
      if (r <= 0) // select target radio
        r = a;
      else if (a <= 0) // end of stream
        r = 0;
      else if (r == radio_id) // its for us
        arq_reschedule_missing(now, source, a);
    }
  }
}

// ------- INFLIGHT -------

void modem_sender2_impl::received_inflight_packet() {
  int num;
  {
    gr::thread::scoped_lock lock(mutex);
    assert(inflight_packets > 0);
    num = inflight_packets;
    inflight_packets -= 1;
  }

  if (false) {
    std::stringstream str;
    str << alias() << ": inflight_packets " << num << std::endl;
    std::cout << str.str();
  }
}

// ------- MCS --------

void modem_sender2_impl::clear_mcs_assignment() {
  assert(mcs_assignment.size() == 256);
  for (int i = 0; i < mcs_assignment.size(); i++)
    mcs_assignment[i] = -1;
  mcs_assignment[255] = 1; // for broadcast, will be boosted to 0 50% of time
}

int modem_sender2_impl::get_usable_mcs_index(int data_len, int index) const {
  index = std::min(std::max(index, 0), MCSCHEME_NUM - 1);
  for (; index < MCSCHEME_NUM - 1; index++) {
    const mcscheme_t &mcs = get_mcscheme(index);
    if (mcs.get_code_len(data_len) <= max_code_len &&
        mcs.get_symb_len(data_len) <= max_symb_len)
      break;
  }
  return index;
}

// ------- MANDATE --------

void modem_sender2_impl::mandate_t::set(float max_latency_s,
                                        float max_throughput_bps,
                                        bool is_file_mandate,
                                        unsigned int resend_count) {
  this->max_latency_s = max_latency_s < 0.0f
                            ? MAX_LATENCY_S
                            : std::min(max_latency_s, MAX_LATENCY_S);

  this->max_throughput_bps =
      max_throughput_bps < 0.0f
          ? MAX_THROUGHPUT_BPS
          : std::min(max_throughput_bps, MAX_THROUGHPUT_BPS);

  this->is_file_mandate = is_file_mandate;
  this->resend_count = resend_count;
}

const modem_sender2_impl::mandate_t &
modem_sender2_impl::get_mandate(int flowid) const {
  auto iter = mandates.find(flowid);
  return iter != mandates.end() ? iter->second : default_mandate;
}

void modem_sender2_impl::set_mandate(int flowid, float max_latency_s,
                                     float max_throughput_bps,
                                     bool is_file_mandate,
                                     unsigned int resend_count) {
  mandate_t &mandate = mandates[flowid];
  mandate.set(max_latency_s, max_throughput_bps, is_file_mandate, resend_count);

  for (flow_t &flow : flows)
    if (flow.flowid == flowid)
      flow.set_mandate(mandate);
};

// ------- LINK ARQ -------

int modem_sender2_impl::arq_increment_seqno(int seqno) {
  assert(0 <= seqno && seqno <= 255);

  if (seqno <= 0 || seqno >= 255)
    return 1;
  else
    return seqno + 1;
}

int modem_sender2_impl::arq_seqno_difference(int seqno1, int seqno2, int min) {
  assert(0 <= seqno1 && seqno1 <= 255);
  assert(0 <= seqno2 && seqno2 <= 255);
  assert(-255 <= min && min <= 255);

  int diff = (seqno1 - seqno2) % 255;
  if (diff < 0)
    diff += 255;

  if (diff >= min + 255)
    diff -= 255;
  else if (diff < min)
    diff += 255;

  assert(min <= diff && diff <= min + 254);
  return diff;
}

modem_sender2_impl::arq_dst_t::sent_t
modem_sender2_impl::arq_create_sent_seqno(time_point_t now, int destination,
                                          int flowid) {
  arq_dst_t &arq_dst = arq_dst_map[destination];

  arq_dst_t::sent_t entry;
  // store nonzero
  entry.seqno = arq_dst.next_seqno != 0 ? arq_dst.next_seqno : 255;
  entry.time = now;
  entry.flowid = flowid;
  entry.marker = next_packet_marker;
  entry.rescheduled = false;
  entry.acknowledged = false;

  arq_dst.sent.push_back(entry);
  if (arq_dst.sent.size() > 255)
    arq_dst.sent.pop_front();

  if (debug_arq) {
    std::stringstream str;
    str << alias() << ": arq " << std::fixed << std::setprecision(3)
        << get_time_point_double(now) << " create_sent_seqno dst "
        << destination << " seqno " << entry.seqno << " flowid " << entry.flowid
        << " marker " << entry.marker << std::endl;
    std::cout << str.str();
  }

  // but send zero to reset receiver
  entry.seqno = arq_dst.next_seqno;

  arq_dst.next_seqno = arq_increment_seqno(arq_dst.next_seqno);
  next_packet_marker += 1;

  return entry;
};

void modem_sender2_impl::arq_reschedule_missing(time_point_t now,
                                                int destination,
                                                int missing_seqno) {
  arq_dst_t &arq_dst = arq_dst_map[destination];

  assert(missing_seqno != 0);
  if (arq_dst.next_seqno == 0)
    return;

  int dist = arq_seqno_difference(arq_dst.next_seqno, missing_seqno, 0);
  if (dist <= 0 || dist > arq_dst.sent.size())
    return;

  arq_dst_t::sent_t &entry = arq_dst.sent[arq_dst.sent.size() - dist];
  assert(entry.seqno == missing_seqno);
  if (entry.rescheduled)
    return;
  entry.rescheduled = true;

  for (flow_t &flow : flows) {
    if (flow.flowid == entry.flowid && flow.destination == destination) {
      if (debug_arq) {
        std::stringstream str;
        str << alias() << ": arq " << std::fixed << std::setprecision(3)
            << get_time_point_double(now) << " reschedule_missing dst "
            << destination << " seqno " << entry.seqno << " flowid "
            << entry.flowid << " marker " << entry.marker << std::endl;
        std::cout << str.str();
      }

      flow.mark_lost_waiting(now, debug_arq, entry.marker);
      return;
    }
  }
};

modem_sender2_impl::arq_dst_t::arq_dst_t() {
  roundtrip_pos = 0;
  median_roundtrip = std::chrono::milliseconds(INIT_ROUND_TRIP_MS);
  for (int i = 0; i < ROUNDTRIP_HISTORY; i++)
    roundtrips[i] = median_roundtrip;
}

void modem_sender2_impl::arq_reschedule_recent(time_point_t now,
                                               int destination,
                                               int next_expected_seqno) {
  arq_dst_t &arq_dst = arq_dst_map[destination];

  assert(next_expected_seqno != 0);
  if (arq_dst.next_seqno == 0)
    return;

  int dist = arq_seqno_difference(arq_dst.next_seqno, next_expected_seqno, 0);

  // update the max round trip duration if not lost
  if (0 <= dist && dist < arq_dst.sent.size()) {
    arq_dst_t::sent_t &entry = arq_dst.sent[arq_dst.sent.size() - 1 - dist];
    assert(arq_increment_seqno(entry.seqno) == next_expected_seqno);

    if (!entry.acknowledged) {
      entry.acknowledged = true;
      duration_t r =
          now - entry.time + std::chrono::milliseconds(EXTRA_ROUND_TRIP_MS);

      arq_dst.roundtrip_pos = (arq_dst.roundtrip_pos + 1) % ROUNDTRIP_HISTORY;
      arq_dst.roundtrips[arq_dst.roundtrip_pos] = r;

      std::vector<duration_t> temp;
      for (int i = 0; i < ROUNDTRIP_HISTORY; i++)
        temp.push_back(arq_dst.roundtrips[i]);
      std::sort(temp.begin(), temp.end());
      arq_dst.median_roundtrip = temp[ROUNDTRIP_HISTORY / 2];

      if (debug_arq) {
        long d1 = std::chrono::duration_cast<std::chrono::milliseconds>(
                      now - entry.time)
                      .count();
        long d2 = std::chrono::duration_cast<std::chrono::milliseconds>(
                      arq_dst.median_roundtrip)
                      .count();
        std::stringstream str;
        str << alias() << ": arq " << std::fixed << std::setprecision(3)
            << get_time_point_double(now) << " dst " << destination
            << " roundtrip " << d1 << " median " << d2 << std::endl;
        std::cout << str.str();
      }
    }
  }

  // nothing can be rescheduled
  if (dist <= 0 || dist > arq_dst.sent.size())
    return;

  for (int i = arq_dst.sent.size() - dist; i < arq_dst.sent.size(); i++) {
    arq_dst_t::sent_t &entry = arq_dst.sent[i];
    if (entry.rescheduled || entry.time + arq_dst.median_roundtrip > now)
      continue;
    entry.rescheduled = true;

    for (flow_t &flow : flows) {
      if (flow.flowid == entry.flowid && flow.destination == destination) {
        if (debug_arq) {
          std::stringstream str;
          str << alias() << ": arq " << std::fixed << std::setprecision(3)
              << get_time_point_double(now) << " reschedule_recent dst "
              << destination << " seqno " << entry.seqno << " flowid "
              << entry.flowid << " marker " << entry.marker << std::endl;
          std::cout << str.str();
        }

        flow.mark_lost_waiting(now, debug_arq, entry.marker);
        break;
      }
    }
  }
}

void modem_sender2_impl::arq_process_rcvd_seqno(time_point_t now, int src,
                                                int received_seqno) {
  arq_src_t &arq_src = arq_src_map[src];

  std::stringstream str;
  if (debug_arq) {
    str << alias() << ": arq " << std::fixed << std::setprecision(3)
        << get_time_point_double(now) << " process_rcvd_seqno src " << src;
  }

  // first time synchronizing
  if (arq_src.next_seqno == 0 || received_seqno == 0) {
    arq_src.rcvd.clear();
    if (received_seqno == 0)
      received_seqno = 255;
    arq_src.next_seqno = received_seqno;
  }

  int dist = arq_seqno_difference(received_seqno, arq_src.next_seqno, -20);

  // we have missing packets
  if (dist > 0) {
    if (debug_arq)
      str << " missing";

    do {
      arq_src_t::rcvd_t entry;
      entry.seqno = arq_src.next_seqno;
      entry.time = now;
      entry.missing = true;
      entry.verified = false;
      entry.report_count = 0;

      arq_src.rcvd.push_back(entry);
      if (arq_src.rcvd.size() > 100) // might confuse the sender, and no point
        arq_src.rcvd.pop_front();

      arq_src.next_seqno = arq_increment_seqno(arq_src.next_seqno);

      if (debug_arq)
        str << " " << entry.seqno;
    } while (--dist > 0);

    assert(arq_src.next_seqno == received_seqno);
  }

  // we are plugging in wholes
  else if (-20 <= dist && dist < 0 && -dist <= arq_src.rcvd.size()) {
    // Careful with signed and unsigned arithmetic (was rcvd.size() + dist >= 0)
    arq_src_t::rcvd_t &entry = arq_src.rcvd[arq_src.rcvd.size() + dist];
    assert(entry.seqno == received_seqno);

    entry.missing = false;
    if (debug_arq) {
      double elapsed =
          get_time_point_double(now) - get_time_point_double(entry.time);
      str << " plugged " << received_seqno << " delay " << std::fixed
          << std::setprecision(3) << elapsed;
    }
  }

  // normal or missing or first case
  if (dist == 0) {
    assert(arq_src.next_seqno == received_seqno);

    arq_src_t::rcvd_t entry;
    entry.seqno = received_seqno;
    entry.time = now;
    entry.missing = false;
    entry.verified = false;
    entry.report_count = 0;

    arq_src.rcvd.push_back(entry);
    if (arq_src.rcvd.size() > 255)
      arq_src.rcvd.pop_front();

    arq_src.next_seqno = arq_increment_seqno(received_seqno);

    if (debug_arq)
      str << " rcvd " << received_seqno;
  }

  if (debug_arq) {
    str << std::endl;
    std::cout << str.str();
  }
}

void modem_sender2_impl::arq_collect_missing_seqnos(time_point_t now) {
  now -= std::chrono::milliseconds(MAX_LATE_MESSAGE_TIME_MS);

  int count = 0;
  std::stringstream str;
  if (debug_arq)
    str << alias() << ": arq " << std::fixed << std::setprecision(3)
        << get_time_point_double(now) << " collect_missing";

  for (auto iter = arq_src_map.begin(); iter != arq_src_map.end(); iter++) {
    for (auto iter2 = iter->second.rcvd.begin();
         iter2 != iter->second.rcvd.end(); iter2++) {
      if (iter2->missing && !iter2->verified && iter2->time < now) {
        iter2->verified = true;

        if (debug_arq) {
          count += 1;
          str << " (" << iter->first << " " << iter2->seqno << ")";
        }
      }
    }
  }

  if (debug_arq && count > 0) {
    str << std::endl;
    std::cout << str.str();
  }
}

std::vector<uint8_t>
modem_sender2_impl::arq_generate_missing_report(int max_size) {
  std::vector<uint8_t> report;

  // report one last received seqno number
  int best_arq_report = std::numeric_limits<int>::max();
  int best_arq_source = 0;
  int best_arq_seqno = 0;
  for (auto iter = arq_src_map.begin(); iter != arq_src_map.end(); iter++) {
    if (iter->second.report_count < best_arq_report) {
      best_arq_report = iter->second.report_count;
      best_arq_source = iter->first;
      best_arq_seqno = iter->second.next_seqno;
    }
  }

  // create last received seqno report
  if (max_size < 2)
    return report;
  report.push_back(best_arq_source);
  report.push_back(best_arq_seqno);

  // select missing seqnos
  std::unordered_map<int, std::vector<int>> selected_map;
  int selected_seqnos = 0;

  for (int c = 1; c <= MAX_MISSING_REPORTS; c++) {
    for (auto iter = arq_src_map.begin(); iter != arq_src_map.end(); iter++) {
      for (auto iter2 = iter->second.rcvd.begin();
           iter2 != iter->second.rcvd.end(); iter2++) {
        if (iter2->missing && iter2->verified && iter2->report_count < c) {
          // skip if already selected
          auto iter3 = selected_map.find(iter->first);
          if (iter3 != selected_map.end() &&
              std::find(iter3->second.begin(), iter3->second.end(),
                        iter2->seqno) != iter3->second.end())
            continue;

          // add if we have enough space
          if (2 * (selected_map.size() + 1 - selected_map.count(iter->first)) +
                  (selected_seqnos + 1) - 1 <=
              max_size - report.size()) {
            selected_map[iter->first].push_back(iter2->seqno);
            selected_seqnos += 1;

            iter2->report_count += 1;
            if (iter2->report_count >= MAX_MISSING_REPORTS)
              iter2->missing = false;
          }
        }
      }
    }
  }

  // create missing seqno report
  for (auto iter = selected_map.begin(); iter != selected_map.end(); iter++) {
    if (iter->first != 0) {
      report.push_back(iter->first);
      for (int seqno : iter->second)
        if (seqno != 0)
          report.push_back(seqno);
      report.push_back(0);
    }
  }

  // remove last zeros
  while (!report.empty() && report.back() == 0)
    report.pop_back();
  assert(report.size() <= max_size);

  return report;
}

// ------- FLOWS -------

modem_sender2_impl::flow_t::flow_t(int source, int destination, int flowid,
                                   const mandate_t &mandate,
                                   modem_sender2_impl *parent)
    : source(source), destination(destination), flowid(flowid), parent(parent) {
  assert(parent != NULL);
  set_mandate(mandate);

  // set initial random delay
  int delay = parent->rand_delay_gen(parent->rand_engine) * 5;
  random_delay = std::chrono::milliseconds(delay);
}

void modem_sender2_impl::flow_t::set_mandate(const mandate_t &mandate) {
  this->mandate = mandate;

  max_bytes_sent = std::ceil(mandate.max_throughput_bps / 8);
  max_latency =
      std::chrono::milliseconds((int)std::floor(mandate.max_latency_s * 1000));

  dirty = true;
  selected = NULL;
}

void modem_sender2_impl::flow_t::mark_packet_dropped(
    std::deque<packet_t>::iterator pos) {
  if (pos->state != STATE_SENT && pos->state != STATE_DROPPED) {
    assert(pos->state == STATE_WAITING || pos->state == STATE_SCHEDULED);
    if (pos->sent_count == 0)
      unsent_bytes -= pos->bytes_len;

    pos->state = STATE_DROPPED;
    packets_dropped += 1;
    bytes_dropped += pos->bytes_len;

    dirty = true;
    selected = NULL;
  }
}

int modem_sender2_impl::flow_t::get_selected_mcs_boost() {
  assert(selected != NULL && selected->state == STATE_SCHEDULED);

  if (mcs_flow_boost > 0 && ++mcs_boost_count > MAX_MCS_BOOST_COUNT) {
    mcs_boost_count = 0;
    mcs_flow_boost -= 1;
  }

  selected->mcs_packet_boost = mcs_flow_boost;
  return mcs_flow_boost;
}

void modem_sender2_impl::flow_t::mark_selected_sent(int arq_packet_marker,
                                                    time_point_t now) {
  assert(selected != NULL && selected->state == STATE_SCHEDULED);
  if (selected->sent_count == 0)
    unsent_bytes -= selected->bytes_len;

  selected->last_sent = now;
  selected->sent_count += 1;
  if (selected->sent_count > mandate.resend_count)
    selected->state = STATE_SENT;
  else
    selected->state = STATE_WAITING;

  selected->arq_packet_marker = arq_packet_marker;
  packets_sent += 1;
  bytes_sent += selected->bytes_len;

  throttle.push_back(std::make_pair(now, selected->bytes_len));
  throttle_sent_bytes += selected->bytes_len;

  time_point_t cutoff = now - std::chrono::milliseconds(1000);
  while (!throttle.empty()) {
    if (throttle.front().first < cutoff) {
      throttle_sent_bytes -= throttle.front().second;
      throttle.pop_front();
    } else
      break;
  }

  dirty = true;
  selected = NULL;
}

void modem_sender2_impl::flow_t::mark_lost_waiting(time_point_t now,
                                                   bool debug_arq,
                                                   int arq_packet_marker) {
  for (packet_t &packet : packets)
    if (packet.arq_packet_marker == arq_packet_marker &&
        (packet.state == STATE_SENT || packet.state == STATE_WAITING)) {

      if (packet.state == STATE_SENT && packet.sent_count == 1)
        unsent_bytes += packet.bytes_len;

      packet.state = STATE_WAITING;
      if (packet.sent_count > 0)
        packet.sent_count -= 1;
      packet.arq_packet_marker = -1;

      if (packet.mcs_packet_boost >= mcs_flow_boost) {
        mcs_flow_boost =
            std::min(MAX_MCS_BOOST_LEVEL, packet.mcs_packet_boost + 1);
        mcs_boost_count = 0;
      } else if (packet.mcs_packet_boost == mcs_flow_boost - 1)
        mcs_boost_count = 0;

      packets_requeued += 1;
      bytes_requeued += packet.bytes_len;

      dirty = true;
      selected = NULL;

      if (debug_arq) {
        long elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(
                           now - packet.time)
                           .count();

        std::stringstream str;
        str << parent->alias() << ": arq rescheduled " << std::fixed
            << std::setprecision(3) << get_time_point_double(now) << " flowid "
            << flowid << " delay " << elapsed << " boost pkt "
            << packet.mcs_packet_boost << " flow " << mcs_flow_boost
            << " count " << mcs_boost_count << std::endl;
        std::cout << str.str();
      }

      // choose another random delay slot if too many collisions
      if (packets_requeued % RANDOM_DELAY_TRIGGER_COUNT == 0) {
        int delay = parent->rand_delay_gen(parent->rand_engine) * 5;
        random_delay = std::chrono::milliseconds(delay);

        if (debug_arq) {
          std::stringstream str;
          str << parent->alias() << ": arq " << std::fixed
              << std::setprecision(3) << get_time_point_double(now)
              << " random_delay " << delay << " for " << flowid << std::endl;
          std::cout << str.str();
        }
      }

      return;
    }
}

void modem_sender2_impl::flow_t::add_packet(time_point_t time, pmt::pmt_t data,
                                            int bytes_len) {
  packets.emplace_back(time, data, bytes_len);
  packet_t &packet = packets.back();
  packet.state = STATE_WAITING;

  packets_load += 1;
  bytes_load += packet.bytes_len;
  queued_bytes += packet.bytes_len;
  unsent_bytes += packet.bytes_len;

  dirty = true;
  selected = NULL;
}

void modem_sender2_impl::flow_t::remove_front_packet() {
  assert(!packets.empty());
  mark_packet_dropped(packets.begin());
  queued_bytes -= packets.front().bytes_len;
  packets.pop_front();
}

void modem_sender2_impl::flow_t::remove_old_packets(time_point_t now) {
  /**
   * We need to keep old sent data to properly calculate the achieved
   * throughput within all measurement period.
   */
  time_point_t old = now - max_latency - std::chrono::milliseconds(1050);
  while (!packets.empty()) {
    if (packets.front().time < old)
      remove_front_packet();
    else
      break;
  }

  old = now - max_latency + std::chrono::milliseconds(MINIMUM_ONEWAY_TIME_MS);
  std::deque<packet_t>::iterator pos = packets.begin();
  while (pos != packets.end() && pos->time < old)
    mark_packet_dropped(pos++);
}

void modem_sender2_impl::flow_t::schedule_next_packet(time_point_t now) {
  if (!dirty)
    return;

  time_point_t cutoff = now - std::chrono::milliseconds(1000);
  while (!throttle.empty()) {
    if (throttle.front().first < cutoff) {
      throttle_sent_bytes -= throttle.front().second;
      throttle.pop_front();
    } else
      break;
  }

  dirty = false;
  selected = NULL;
  if (packets.empty()) {
    queued_max_wait = 0.0f;
    return;
  }

  if (mandate.is_file_mandate) {
    if (throttle_sent_bytes >= max_bytes_sent) {
      dirty = true;
      return;
    } else {
      unsigned int min_sent_count = std::numeric_limits<unsigned int>::max();
      for (const packet_t &packet : packets) {
        if (packet.state == STATE_WAITING || packet.state == STATE_SCHEDULED)
          min_sent_count = std::min(min_sent_count, packet.sent_count);
      }

      for (packet_t &packet : packets) {
        if (packet.sent_count == min_sent_count &&
            (packet.state == STATE_WAITING ||
             packet.state == STATE_SCHEDULED)) {
          packet.state = STATE_SCHEDULED;
          selected = &packet;
          break;
        }
      }
    }
  } else {
    for (packet_t &packet : packets)
      if (packet.state == STATE_SCHEDULED)
        packet.state = STATE_WAITING;

    std::deque<packet_t>::iterator pos = packets.begin();
    int available_history =
        std::chrono::duration_cast<std::chrono::milliseconds>(now - pos->time)
            .count();

    // use 980 ms windows, and last interval should go 10 ms past now
    const int period_length_ms = 980;
    if (available_history < period_length_ms)
      available_history = period_length_ms;

    time_point_t measurement_period =
        now -
        std::chrono::milliseconds(
            (available_history / period_length_ms) * period_length_ms - 10);

    while (measurement_period < now) {
      int waiting_in_period = verify_period(
          pos, measurement_period,
          measurement_period + std::chrono::milliseconds(period_length_ms),
          now);

      // dirty if last period still has waiting (unscheduled) packets
      dirty = waiting_in_period != 0;

      measurement_period += std::chrono::milliseconds(period_length_ms);
    }
  }

  // wait if selected is too recent
  if (selected != NULL && selected->time + random_delay > now) {
    dirty = true;
    selected = NULL;
    return;
  }

  // wait if we have sent the same packet in the last 50 milliseconds
  if (selected != NULL && selected->sent_count > 0 &&
      selected->last_sent + std::chrono::milliseconds(50) > now) {
    dirty = true;
    selected = NULL;
    return;
  }
}

int modem_sender2_impl::flow_t::verify_period(
    std::deque<packet_t>::iterator &pos, time_point_t time1, time_point_t time2,
    time_point_t now) {
  // go backwards with pos and find the first thing strictly after time1
  while (pos != packets.begin() && (--pos)->time > time1)
    ;

  while (pos != packets.end() && pos->time <= time1)
    pos++;

  if (pos == packets.end())
    return 0;

  std::deque<packet_t>::iterator pos2 = pos;
  long bytes_in_interval = 0;
  int waiting_in_interval = 0;

  while (pos2 != packets.end() && pos2->time <= time2) {
    if (pos2->state == STATE_SENT || pos2->state == STATE_SCHEDULED)
      bytes_in_interval += pos2->bytes_len;
    else if (pos2->state == STATE_WAITING)
      waiting_in_interval += 1;

    pos2++;
  }

  /*
   * Prefer those packets where there is more than 250 millisecond
   * time left and among those prefer those that have the least time left.
   * If all packets have less than 250 millisec life left, then prefer
   * those that have the most time left (they might arrive in time)
   */
  time_point_t cutoff =
      now - max_latency + std::chrono::milliseconds(URGENT_CUTOFF_TIME_MS);

  while (pos2 != pos && bytes_in_interval < max_bytes_sent) {
    pos2--;
    if (pos2->state == STATE_WAITING) {
      pos2->state = STATE_SCHEDULED;
      bytes_in_interval += pos2->bytes_len;
      waiting_in_interval -= 1;

      bool pos2_is_better;
      if (selected == NULL)
        pos2_is_better = true;

      // prefer those which have lower sent count
      else if (pos2->sent_count != selected->sent_count)
        pos2_is_better = pos2->sent_count < selected->sent_count;

      // if both are urgent, then prefer the leter one, it can make it
      else if (selected->time < cutoff && pos2->time < cutoff)
        pos2_is_better = pos2->time > selected->time;

      // otherwise prefer the earlier one (urgent or not)
      else
        pos2_is_better = pos2->time < selected->time;

      if (pos2_is_better)
        selected = &*pos2;
    }
  }

  return waiting_in_interval;
}

modem_sender2_impl::flow_t &
modem_sender2_impl::find_flow(int source, int destination, int flowid) {
  for (flow_t &flow : flows) {
    if (flow.source == source && flow.destination == destination &&
        flow.flowid == flowid)
      return flow;
  }

  const mandate_t &mandate = get_mandate(flowid);
  flows.emplace_back(source, destination, flowid, mandate, this);
  return flows.back();
}

void modem_sender2_impl::remove_flow(size_t index) {
  assert(0 <= index && index < flows.size());

  if (index + 1 < flows.size())
    flows[index] = std::move(flows.back());

  flows.pop_back();
}

void modem_sender2_impl::enqueue(time_point_t now, pmt::pmt_t msg) {
  if (!pmt::is_pair(msg) || !pmt::is_u8vector(pmt::cdr(msg))) {
    std::cout << "####### modem_sender2: invalid message format\n";
    return;
  }

  pmt::pmt_t data = pmt::cdr(msg);
  if (pmt::length(data) > max_data_len - 4) {
    std::cout << "####### modem_sender2: input message is too long\n";
    return;
  }

  size_t data_len = 0;
  const uint8_t *data_ptr = pmt::u8vector_elements(data, data_len);
  if (data_len < mmt_header_t::len()) {
    std::cout << "####### modem_sender2: message is too short\n";
    return;
  }

  mmt_header_t mmt;
  mmt.load(data_ptr);

  if (std::find(blocked_flowids.begin(), blocked_flowids.end(), mmt.flowid) !=
      blocked_flowids.end())
    return;

  flow_t &flow = find_flow(mmt.src, mmt.dst, mmt.flowid);

  assert(max_queue_size >= 1);
  if (flow.packets.size() >= max_queue_size)
    flow.remove_front_packet();

  int bytes_len =
      mmt_header_t::get_zip_payload_len(data_ptr, data_len, payload_mode);

  int seqno = -1;
  time_point_t mgen = get_mgen_timestamp(data_ptr, data_len, now, seqno);
  flow.add_packet(mgen, data, bytes_len);

  if (debug_scheduler) {
    double mgen2 = get_time_point_double(mgen);
    double now2 = get_time_point_double(now);

    std::stringstream str;
    str << alias() << ": enqueue flow " << mmt.flowid << " seqno " << seqno
        << std::fixed << std::setprecision(3) << " mgen " << mgen2 << " now "
        << now2 << " elapsed " << (now2 - mgen2) << std::endl;
    std::cout << str.str();
  }
}

modem_sender2_impl::time_point_t
modem_sender2_impl::get_mgen_timestamp(const uint8_t *data_ptr, size_t data_len,
                                       time_point_t now, int &seqno) {
  if (data_len < mmt_header_t::len())
    return now;
  mmt_header_t mmt;
  mmt.load(data_ptr);
  data_ptr += mmt_header_t::len();
  data_len -= mmt_header_t::len();
  assert(data_len >= 0);

  eth_header_t eth;
  const int eth_zip_len = eth.eth_zip_load(mmt, data_ptr, data_len);
  if (eth_zip_len < 0)
    return now;
  data_ptr += eth_zip_len;
  data_len -= eth_zip_len;
  assert(data_len >= 0);
  const int eth_raw_len = eth.eth_raw_len();
  assert(eth_raw_len >= 0);

  ip4_header_t ip4;
  const int ip4_zip_len = ip4.ip4_zip_load1(mmt, data_ptr, data_len);
  if (ip4_zip_len < 0)
    return now;
  data_ptr += ip4_zip_len;
  data_len -= ip4_zip_len;
  assert(data_len >= 0);
  const int ip4_raw_len = ip4.ip4_raw_len();
  assert(ip4_raw_len >= 0);

  // not UDP
  if (ip4.prot != 0x11 || (ip4.frag & 0x1fff) != 0)
    return now;

  udp_header_t udp;
  const int udp_zip_len = udp_header_t::udp_zip_len(mmt);
  if (udp_zip_len < 0 || data_len < udp_zip_len)
    return now;
  udp.udp_zip_load1(mmt, data_ptr);
  data_ptr += udp_zip_len;
  data_len -= udp_zip_len;
  assert(data_len >= 0);
  const int udp_raw_len = udp_header_t::udp_raw_len();
  assert(udp_raw_len >= 0);

  mgn_header_t mgn; // we do not depend on ip4_zip_load2
  const int mgn_zip_len = mgn.mgn_zip_load(mmt, ip4, data_ptr, data_len);
  if (mgn_zip_len < 0)
    return now;
  data_ptr += mgn_zip_len;
  data_len -= mgn_zip_len;
  assert(data_len >= 0);
  const int mgn_raw_len = mgn.mgn_raw_len();
  assert(mgn_raw_len >= 0);

  if (!mgn.valid)
    return now;

  seqno = mgn.mgenSeqno;

  time_point_t mgen_time(std::chrono::seconds(mgn.txTimeSeconds));
  mgen_time += std::chrono::microseconds(mgn.txTimeMicroseconds);

  // if mgen_time is in the past and within two second
  if (now - std::chrono::milliseconds(2000) <= mgen_time && mgen_time <= now)
    return mgen_time;

  return now;
}

bool modem_sender2_impl::is_second_more_important(time_point_t now,
                                                  const flow_t &first,
                                                  const flow_t &second) const {
  assert(first.selected != NULL && second.selected != NULL);

  // our broadcasts are the most important
  if (first.flowid == MAC_BROADCAST_FLOWID)
    return false;
  else if (second.flowid == MAC_BROADCAST_FLOWID)
    return true;

  time_point_t first_deadline = first.selected->time + first.max_latency;
  time_point_t second_deadline = second.selected->time + second.max_latency;
  time_point_t cutoff_time =
      now + std::chrono::milliseconds(URGENT_CUTOFF_TIME_MS);

  // if both are urgent, then prefer the later one (that can make it)
  if (first_deadline < cutoff_time && second_deadline < cutoff_time)
    return second_deadline > first_deadline;

  // if one of them urgent, then prefer the urgent one
  if (first_deadline < cutoff_time || second_deadline < cutoff_time)
    return second_deadline < first_deadline;

  // if neither is urgent, then prefer the smaller load
  float first_bytes = first.mandate.max_throughput_bps != MAX_THROUGHPUT_BPS
                          ? first.max_bytes_sent
                          : first.avg_bytes_load;

  float second_bytes = second.mandate.max_throughput_bps != MAX_THROUGHPUT_BPS
                           ? second.max_bytes_sent
                           : second.avg_bytes_load;

  return second_bytes < first_bytes;
}

bool modem_sender2_impl::is_second_more_important2(time_point_t now,
                                                   const flow_t &first,
                                                   const flow_t &second) const {
  assert(first.selected != NULL && second.selected != NULL);

  // our broadcasts are the most important
  if (first.flowid == MAC_BROADCAST_FLOWID)
    return false;
  else if (second.flowid == MAC_BROADCAST_FLOWID)
    return true;

  // prefer lower max_latency (think about high load with large latency)
  if (second.max_latency != first.max_latency)
    return second.max_latency < first.max_latency;

  // prefer continuous mandates over file mandates
  if (second.mandate.is_file_mandate != first.mandate.is_file_mandate)
    return first.mandate.is_file_mandate;

  // for continus mandate ...
  if (!second.mandate.is_file_mandate) {
    time_point_t first_deadline = first.selected->time + first.max_latency;
    time_point_t second_deadline = second.selected->time + second.max_latency;

    // prefer the more urgent one, or
    if (second_deadline != first_deadline)
      return second_deadline < first_deadline;
  }

  // prefer the lower number of unsent bytes
  return second.unsent_bytes < first.unsent_bytes;
}

pmt::pmt_t modem_sender2_impl::dequeue(time_point_t now) {
  selected_flow = NULL;
  for (flow_t &flow : flows) {
    flow.remove_old_packets(now);
    flow.schedule_next_packet(now);

    if (flow.selected != NULL) {
      if (selected_flow == NULL ||
          is_second_more_important2(now, *selected_flow, flow))
        selected_flow = &flow;
    }
  }

  if (selected_flow == NULL)
    return pmt::get_PMT_NIL();

  if (debug_scheduler) {
    double mgen2 = get_time_point_double(selected_flow->selected->time);
    double now2 = get_time_point_double(now);

    std::stringstream str;
    str << alias() << ": dequeue flow " << selected_flow->flowid << std::fixed
        << std::setprecision(3) << " mgen " << mgen2 << " now " << now2
        << " elapsed " << (now2 - mgen2) << std::endl;
    std::cout << str.str();
  }

  pmt::pmt_t data = selected_flow->selected->data;
  return data;
}

} /* namespace marmote3 */
} /* namespace gr */
