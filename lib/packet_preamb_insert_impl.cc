/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "packet_preamb_insert_impl.h"
#include <gnuradio/io_signature.h>
#include <marmote3/constants.h>
#include <volk/volk.h>

namespace gr {
namespace marmote3 {

packet_preamb_insert::sptr
packet_preamb_insert::make(const std::vector<gr_complex> &preamble,
                           bool mcs_config, float power, header_t header,
                           bool postamble, int max_output_len) {
  return gnuradio::get_initial_sptr(new packet_preamb_insert_impl(
      preamble, mcs_config, power, header, postamble, max_output_len));
}

packet_preamb_insert_impl::packet_preamb_insert_impl(
    const std::vector<gr_complex> &preamble, bool mcs_config, float power,
    header_t header, bool postamble, int max_output_len)
    : tagged_stream_block2("packet_preamb_insert",
                           gr::io_signature::make(1, 1, sizeof(gr_complex)),
                           gr::io_signature::make(1, 1, sizeof(gr_complex)),
                           max_output_len),
      preamble(preamble), mcs_config(mcs_config), power(power), header(header),
      encoder(fec_conv_encoder::get_standard_polys(2, 9), 9),
      postamble(postamble) {
  if (header != HDR_NONE && header != HDR_SRC_MCS_LEN_CRC)
    throw std::invalid_argument("packet_preamb_insert: invalid header type");
}

packet_preamb_insert_impl::~packet_preamb_insert_impl() {}

const float sqrt_half = std::sqrt(1.0f / 2);

int packet_preamb_insert_impl::work(int noutput_items,
                                    gr_vector_int &ninput_items,
                                    gr_vector_const_void_star &input_items,
                                    gr_vector_void_star &output_items) {
  int pwr = power;
  if (mcs_config) {
    if (!has_input_long(0, PMT_PACKET_MCS)) {
      std::cout << "####### packet_preamb_insert: MCS is not set\n";
      return 0;
    }
    pwr += get_mcscheme(get_input_long(0, PMT_PACKET_MCS, -1)).ext_power;
  }
  float ampl = std::pow(10.0f, 0.05f * pwr);

  int input_len = ninput_items[0];
  const gr_complex *in = (const gr_complex *)input_items[0];
  gr_complex *out = (gr_complex *)output_items[0];

  int output_len = preamble.size() + input_len;
  if (header == HDR_SRC_MCS_LEN_CRC)
    output_len += 40;

  if (output_len > noutput_items) {
    std::cout << "####### packet_preamb_insert: packet too long\n";
    return 0;
  }

  if (postamble) {
    volk_32f_s32f_multiply_32f((float *)out, (float *)in, ampl, input_len * 2);
    out += input_len;

    volk_32f_s32f_multiply_32f((float *)out, (float *)preamble.data(), ampl,
                               preamble.size() * 2);
    out += preamble.size();
  }

  if (header == HDR_SRC_MCS_LEN_CRC) {
    int src = get_input_long(0, PMT_PACKET_SRC, -1);
    int mcs = get_input_long(0, PMT_PACKET_MCS, -1);
    int len = get_input_long(0, PMT_DATA_LEN, -1);

    if (src < 0 || src >= 256 || mcs < 0 || mcs >= 32 || len < 0 ||
        len >= 2048) {
      std::cout << "####### packet_preamb_insert: invalid header parameters\n";
      return 0;
    }

    uint8_t data_bytes[4];
    data_bytes[0] = src;
    data_bytes[1] = (mcs << 3) + (len >> 8);
    data_bytes[2] = len;
    data_bytes[3] = data_bytes[0] + data_bytes[1] + data_bytes[2] + 73;

    uint8_t data_bits[32];
    for (int i = 0; i < 32; i++)
      data_bits[i] = (data_bytes[i / 8] >> (i % 8)) & 0x1;

    assert(encoder.get_code_len(32) == 80);
    uint8_t code_bits[80];
    encoder.encode(data_bits, 32, code_bits);

    float symbols[80];
    for (int i = 0; i < 80; i++)
      symbols[i] = code_bits[(11 * i) % 80] ? -sqrt_half : sqrt_half;

    volk_32f_s32f_multiply_32f((float *)out, symbols, ampl, 80);
    out += 40;
  }

  if (!postamble) {
    volk_32f_s32f_multiply_32f((float *)out, (float *)preamble.data(), ampl,
                               preamble.size() * 2);
    out += preamble.size();

    volk_32f_s32f_multiply_32f((float *)out, (float *)in, ampl, input_len * 2);
    out += input_len;
  }

  assert(output_len == out - (gr_complex *)output_items[0]);
  return output_len;
}

void packet_preamb_insert_impl::set_power(float pwr) { power = pwr; }

float packet_preamb_insert_impl::get_power() const { return power; }

} /* namespace marmote3 */
} // namespace gr
