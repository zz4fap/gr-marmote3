/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "modem_bert_receiver_impl.h"
#include <gnuradio/io_signature.h>
#include <marmote3/constants.h>
#include <sstream>

namespace gr {
namespace marmote3 {

modem_bert_receiver::sptr modem_bert_receiver::make(int seed) {
  return gnuradio::get_initial_sptr(new modem_bert_receiver_impl(seed));
}

modem_bert_receiver_impl::modem_bert_receiver_impl(int seed)
    : tagged_stream_block2("modem_bert_receiver",
                           gr::io_signature::make(1, 1, sizeof(uint8_t)),
                           gr::io_signature::make(0, 0, 0), 0),
      engine(seed) {
  total_frames = 0;
  correct_frames = 0;
  total_bits = 0;
  bit_errors = 0;
}

modem_bert_receiver_impl::~modem_bert_receiver_impl() {}

void modem_bert_receiver_impl::extend_packet(int length) {
  if (packet.size() < length) {
    std::uniform_int_distribution<> rand_byte(0, 255);
    while (packet.size() < length)
      packet.push_back(rand_byte(engine));
  }
}

bool modem_bert_receiver_impl::stop() {
  std::stringstream s;
  s << alias() << ": total frames " << total_frames << ", good "
    << correct_frames << ", ber " << get_ber() << ", fer " << get_fer()
    << std::endl;
  std::cout << s.str();
  return block::stop();
}

double modem_bert_receiver_impl::get_ber() const {
  return double(bit_errors) / total_bits;
}

double modem_bert_receiver_impl::get_fer() const {
  return 1.0 - double(correct_frames) / total_frames;
}

int modem_bert_receiver_impl::work(int noutput_items,
                                   gr_vector_int &ninput_items,
                                   gr_vector_const_void_star &input_items,
                                   gr_vector_void_star &output_items) {
  const uint8_t *in = (const uint8_t *)input_items[0];
  int len = ninput_items[0];
  extend_packet(len);

  int errs = 0;
  for (int i = 0; i < len; i++) {
    uint8_t v = *(in++) ^ packet[i];
    errs += __builtin_popcount(v);
  }

  total_frames += 1;
  total_bits += 8 * len;
  bit_errors += errs;
  if (!errs)
    ++correct_frames;

  return 0;
}

} /* namespace marmote3 */
} /* namespace gr */
