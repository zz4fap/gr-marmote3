/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_MARMOTE3_PACKET_PREAMB_EQUALIZER_IMPL_H
#define INCLUDED_MARMOTE3_PACKET_PREAMB_EQUALIZER_IMPL_H

#include <gnuradio/fft/fft.h>
#include <gnuradio/filter/fir_filter.h>
#include <marmote3/fec_conv_xcoder.h>
#include <marmote3/packet_preamb_equalizer.h>

namespace gr {
namespace marmote3 {

class packet_preamb_equalizer_impl : public packet_preamb_equalizer {
private:
  const int preamble_samp; // number of preamble samples (not symbols)
  const int padding_samp;  // number of cyclic padding samples (not symbols)
  const int samp_per_symb; // number of symbols per sample
  const bool postamble;
  const int payload_len;
  const float energy_limit;
  const header_t header;
  const bool save_taps;
  const bool frame_debug;
  std::vector<gr_complex> preamble_full; // in samples
  std::vector<gr_complex> preamble_fft;  // in samples
  std::vector<gr_complex> preamble_dat;  // in symbols
  float preamble_energy;

  fft::fft_complex forward;
  fft::fft_complex backward;

  std::vector<gr_complex> taps;
  filter::kernel::fir_filter_ccc fir;

  fec_conv_decoder decoder;

  gr::thread::mutex mutex;
  uint32_t total_packets = 0;
  uint32_t header_crc_error = 0;
  uint32_t invalid_mcs_index = 0;
  uint32_t invalid_data_len = 0;
  uint32_t frame_too_short = 0;
  uint32_t frame_too_long = 0;
  uint32_t frame_truncated = 0;

public:
  packet_preamb_equalizer_impl(const std::vector<gr_complex> &preamble,
                               int samp_per_symb, int padding_len,
                               header_t header, bool postamble,
                               int max_output_len, float energy_limit,
                               bool save_taps, bool frame_debug);
  ~packet_preamb_equalizer_impl();

  int work(int noutput_items, gr_vector_int &ninput_items,
           gr_vector_const_void_star &input_items,
           gr_vector_void_star &output_items) override;

  void collect_block_report(ModemReport *modem_report) override;

private:
  int work_header_none(const gr_complex *input, int input_len,
                       gr_complex *output, int output_len);
  int work_header_src_mcs_len_crc(const gr_complex *input, int input_len,
                                  gr_complex *output, int output_len);

  int find_sync_offset(const gr_complex *samples);
  void analyze_preamble(const gr_complex *samples);
  void decode_header(const gr_complex *samples, uint8_t *bytes, int bytes_len);
};

} // namespace marmote3
} // namespace gr

#endif /* INCLUDED_MARMOTE3_PACKET_PREAMB_EQUALIZER_IMPL_H */
