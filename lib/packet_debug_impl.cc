/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "packet_debug_impl.h"
#include <chrono>
#include <gnuradio/io_signature.h>
#include <sstream>

namespace gr {
namespace marmote3 {

packet_debug::sptr packet_debug::make(int itemsize, int trig_type,
                                      const std::string &trig_key,
                                      long trig_val, int print_len) {
  return gnuradio::get_initial_sptr(new packet_debug_impl(
      itemsize, trig_type, trig_key, trig_val, print_len));
}

packet_debug_impl::packet_debug_impl(int itemsize, int trig_type,
                                     const std::string &trig_key, long trig_val,
                                     int print_len)
    : tagged_stream_block2("packet_debug",
                           gr::io_signature::make(1, 1, itemsize),
                           gr::io_signature::make(0, 0, 0), 0),
      itemsize(itemsize), trig_type(trig_type),
      trig_key(pmt::string_to_symbol(trig_key)), trig_val(trig_val),
      print_len(print_len) {
  if (itemsize != 1 && itemsize != 4 && itemsize != 8)
    throw std::invalid_argument("packet_debug: invalid item size");

  if (print_len < -1)
    throw std::invalid_argument(
        "packet_debug: print length must be -1, 0 or positive");

  if (trig_type < 0 || trig_type > 2)
    throw std::invalid_argument("packet_debug: invalid trigger type");

  if (trig_type == 2 && trig_val <= 0)
    throw std::invalid_argument(
        "packet_debug: invalid trigger value for modulo type");
}

packet_debug_impl::~packet_debug_impl() {}

std::string get_time_of_day() {
  using namespace std;
  using namespace std::chrono;

  system_clock::duration t = system_clock::now().time_since_epoch();

  typedef duration<int, ratio_multiply<hours::period, ratio<24>>::type> days;
  t -= duration_cast<days>(t);

  hours h = duration_cast<hours>(t);
  t -= h;

  minutes m = duration_cast<minutes>(t);
  t -= m;

  seconds s = duration_cast<seconds>(t);
  t -= s;

  milliseconds f = duration_cast<milliseconds>(t);
  t -= f;

  microseconds g = duration_cast<microseconds>(t);
  t -= g;

  std::ostringstream r;
  r << setfill('0') << setw(2) << h.count() << ":" << setw(2) << m.count()
    << ":" << setw(2) << s.count() << "." << setw(3) << f.count() << ","
    << setw(3) << g.count();
  return r.str();
}

int packet_debug_impl::work(int noutput_items, gr_vector_int &ninput_items,
                            gr_vector_const_void_star &input_items,
                            gr_vector_void_star &output_items) {
  if (trig_type == 0 ||
      (trig_type == 1 && get_input_long(0, trig_key, -1) == trig_val) ||
      (trig_type == 2 && get_input_long(0, trig_key, -1) % trig_val == 0)) {
    std::ostringstream s;

    s << alias() << " " << get_time_of_day() << " " << input_dicts[0]
      << std::endl;

    if (print_len != 0) {
      int len = ninput_items[0];
      if (print_len >= 0 && print_len < len)
        len = print_len;

      s << "[";

      if (itemsize == 4) {
        const float *in = (const float *)input_items[0];
        for (int i = 0; i < len; i++) {
          if (i != 0)
            s << " ";
          s << in[i];
        }
      } else if (itemsize == 8) {
        const gr_complex *in = (const gr_complex *)input_items[0];
        for (int i = 0; i < len; i++) {
          if (i != 0)
            s << " ";
          s << in[i];
        }
      } else {
        const unsigned char *in = (const unsigned char *)input_items[0];
        for (int i = 0; i < len; i++) {
          s << std::right << std::setfill('0');
          if (i != 0)
            s << " ";
          s << std::hex << std::setw(2) << (unsigned int)in[i];
        }
      }

      s << "]" << std::endl;
    }

    std::cout << s.str();
  }

  return noutput_items;
}

} /* namespace marmote3 */
} /* namespace gr */
