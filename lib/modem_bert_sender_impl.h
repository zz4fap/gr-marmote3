/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_MARMOTE3_MODEM_BERT_SENDER_IMPL_H
#define INCLUDED_MARMOTE3_MODEM_BERT_SENDER_IMPL_H

#include <marmote3/modem_bert_sender.h>
#include <vector>

namespace gr {
namespace marmote3 {

class modem_bert_sender_impl : public modem_bert_sender {
private:
  const int num_packets;
  const int max_packet_len;
  std::vector<uint8_t> packet;

  uint32_t packet_id;

public:
  modem_bert_sender_impl(int seed, int num_packets, int max_packet_len);
  ~modem_bert_sender_impl();

  int work(int noutput_items, gr_vector_int &ninput_items,
           gr_vector_const_void_star &input_items,
           gr_vector_void_star &output_items);
};

} // namespace marmote3
} // namespace gr

#endif /* INCLUDED_MARMOTE3_MODEM_BERT_SENDER_IMPL_H */
