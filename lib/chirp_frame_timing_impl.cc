/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "chirp_frame_timing_impl.h"
#include <cassert>
#include <gnuradio/io_signature.h>
#include <volk/volk.h>

namespace gr {
namespace marmote3 {

chirp_frame_timing::sptr chirp_frame_timing::make(int chirp_len, int fft_len,
                                                  int vlen, float noise_level) {
  return gnuradio::get_initial_sptr(
      new chirp_frame_timing_impl(chirp_len, fft_len, vlen, noise_level));
}

chirp_frame_timing_impl::chirp_frame_timing_impl(int chirp_len, int fft_len,
                                                 int vlen, float noise_level)
    : gr::block("chirp_frame_timing",
                gr::io_signature::make(1, 1, sizeof(float) * fft_len),
                gr::io_signature::make(1, 1, sizeof(float) * fft_len)),
      chirp_len(chirp_len), fft_len(fft_len), vlen(vlen),
      vlen_times_fft(vlen * fft_len), vlen_times_chirp(vlen * chirp_len),
      chirp_div_fft(chirp_len / fft_len), scale(fft_len),
      noise_level(std::max(noise_level * chirp_len, 1e-9f)), selector(false) {
  if (fft_len <= 0)
    throw std::invalid_argument(
        "chirp_frame_timing: FFT length must be positive");

  if (chirp_len <= fft_len || chirp_len % fft_len != 0)
    throw std::invalid_argument("chirp_frame_timing: chirp length must "
                                "be a proper multiple of FFT");

  if (vlen <= 0)
    throw std::invalid_argument(
        "chirp_frame_timing: vector length must be positive");

  int total = (vlen_times_fft + vlen * chirp_div_fft) * 2 * sizeof(float);
  if (posix_memalign(&memory, 16, total) != 0)
    throw std::bad_alloc();
  std::memset(memory, 0, total);

  state[0].chirp = (float *)memory;
  state[1].chirp = state[0].chirp + vlen_times_fft;
  state[0].noise = state[1].chirp + vlen_times_fft;
  state[1].noise = state[0].noise + vlen * chirp_div_fft;

  float step = ((float)fft_len) / chirp_div_fft;
  bins.push_back(0);
  for (int i = 1; i < chirp_div_fft; i++)
    bins.push_back((int)(0.5 + step * i));
  bins.push_back(fft_len);
  if (false) {
    for (int i = 0; i < bins.size(); i++)
      std::cout << "bin " << bins[i] << std::endl;
  }

  set_output_multiple(vlen);
  set_relative_rate(1.0f / chirp_div_fft);
}

chirp_frame_timing_impl::~chirp_frame_timing_impl() {
  free(memory);
  memory = NULL;
}

void chirp_frame_timing_impl::set_noise_level(float level) {
  noise_level = std::max(level * chirp_len, 1e-9f);
}

void chirp_frame_timing_impl::forecast(int noutput_items,
                                       gr_vector_int &ninput_items_required) {
  int blocks = noutput_items / vlen;
  ninput_items_required[0] = vlen * chirp_div_fft * blocks;
}

inline void chirp_frame_timing_impl::load_noise_levels(const float *in,
                                                       state_t curr) {
  for (int i = 0; i < chirp_div_fft; i++) {
    float *noise = curr.noise + i;
    for (int j = 0; j < vlen; j++) {
      volk_32f_accumulator_s32f(noise, in, fft_len);
      noise += chirp_div_fft;
      in += fft_len;
    }
  }
}

inline void chirp_frame_timing_impl::update_chirp_levels(const float *in,
                                                         state_t prev,
                                                         state_t curr) {
  std::memset(curr.chirp, 0, vlen_times_fft * sizeof(float));

  for (int s = 0; s < chirp_div_fft; s++) {
    float *prev_chirp = prev.chirp;
    float *curr_chirp = curr.chirp;

    for (int i = 0; i < vlen; i++) {
      if (s > 0)
        volk_32f_x2_add_32f(curr_chirp, curr_chirp, in, bins[s]);

      if (s < chirp_div_fft - 1) {
        int b = bins[s + 1];
        volk_32f_x2_add_32f(prev_chirp + b, prev_chirp + b, in + b,
                            fft_len - b);
      }

      in += fft_len;
      prev_chirp += fft_len;
      curr_chirp += fft_len;
    }
  }
}

inline void chirp_frame_timing_impl::write_norm_levels(float *out, state_t prev,
                                                       state_t curr) {
  float *prev_chirp = prev.chirp;
  float *prev_noise = prev.noise;
  float *curr_noise = curr.noise;

  for (int i = 0; i < vlen; i++) {
    float sum = curr_noise[0];
    for (int j = 0; j < chirp_div_fft; j++)
      sum += prev_noise[j];
    volk_32f_s32f_multiply_32f(out, prev_chirp,
                               scale / std::max(sum, noise_level), bins[1]);

    for (int j = 1; j < chirp_div_fft; j++) {
      sum += curr_noise[j];
      sum -= prev_noise[j - 1];
      int b = bins[j];
      volk_32f_s32f_multiply_32f(out + b, prev_chirp + b,
                                 scale / std::max(sum, noise_level),
                                 bins[j + 1] - b);
    }

    prev_chirp += fft_len;
    out += fft_len;
    prev_noise += chirp_div_fft;
    curr_noise += chirp_div_fft;
  }
}

int chirp_frame_timing_impl::general_work(
    int noutput_items, gr_vector_int &ninput_items,
    gr_vector_const_void_star &input_items, gr_vector_void_star &output_items) {
  const float *in = (const float *)input_items[0];
  float *out = (float *)output_items[0];

  assert(noutput_items % vlen == 0);
  for (int n = 0; n < noutput_items; n += vlen) {
    selector = selector != 0 ? 0 : 1;
    state_t prev = state[selector];
    state_t curr = state[1 - selector];

    load_noise_levels(in, curr);
    update_chirp_levels(in, prev, curr);
    write_norm_levels(out, prev, curr);

    in += vlen_times_chirp;
    out += vlen_times_fft;
  }

  consume_each(noutput_items * chirp_div_fft);
  return noutput_items;
}

} /* namespace marmote3 */
} /* namespace gr */
