/* -*- c++ -*- */
/*
 * Copyright 2018 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "modem_monitor_impl.h"
#include <algorithm>
#include <chrono>
#include <gnuradio/block_detail.h>
#include <gnuradio/buffer.h>
#include <gnuradio/config.h>
#include <gnuradio/io_signature.h>
#include <gnuradio/prefs.h>
#include <google/protobuf/text_format.h>
#include <marmote3/marmote3.pb.h>
#include <sstream>

namespace gr {
namespace marmote3 {

modem_monitor::sptr modem_monitor::make(int report_rate,
                                        const std::string &report_addr,
                                        int print_mode,
                                        const std::string &command_addr) {
  return gnuradio::get_initial_sptr(new modem_monitor_impl(
      report_rate, report_addr, print_mode, command_addr));
}

modem_monitor_impl::modem_monitor_impl(int report_rate,
                                       const std::string &report_addr,
                                       int print_mode,
                                       const std::string &command_addr)
    : gr::block("modem_monitor", gr::io_signature::make(0, 0, 0),
                gr::io_signature::make(0, 0, 0)),
      enable_perf_report(
          prefs::singleton()->get_bool("PerfCounters", "on", false)),
      sequence_no(0), context(1), report_rate(report_rate),
      report_addr(report_addr), print_mode(print_mode),
      report_socket(context, ZMQ_PUB), report_thread(NULL),
      command_addr(command_addr), command_socket(context, ZMQ_PULL),
      command_thread(NULL) {
  if (report_rate < 0)
    throw std::out_of_range("modem_monitor: invalid rate");

  if (report_rate > 0) {
    std::stringstream str;
    str << alias() << ": reporting on " << report_addr << " at " << report_rate
        << " Hz";
#ifdef GR_PERFORMANCE_COUNTERS
    str << " with perf report " << (enable_perf_report ? "on" : "off");
#endif
    str << std::endl;
    std::cout << str.str();

    report_socket.bind(report_addr.c_str());
  }

  {
    std::stringstream str;
    str << alias() << ": listening on " << command_addr << std::endl;
    std::cout << str.str();

    int timeout = 100; // millisecs
    command_socket.setsockopt(ZMQ_RCVTIMEO, &timeout, sizeof(timeout));
    command_socket.bind(command_addr.c_str());
  }

  {
    std::stringstream str;
    str << alias();
#ifdef NDEBUG
    str << ": assert is disabled";
#else
    str << ": assert is enabled";
#endif
    str << std::endl;
    std::cout << str.str();
  }

  // this is just a hack to get the start/stop events
  message_port_register_in(pmt::mp("unused"));
  message_port_register_out(pmt::mp("unused"));
}

modem_monitor_impl::~modem_monitor_impl() {
  stop();
  command_socket.close();
  report_socket.close();
}

bool modem_monitor_impl::start() {
  if (!report_thread && report_rate > 0) {
    report_thread = boost::shared_ptr<boost::thread>(
        new boost::thread(boost::bind(&modem_monitor_impl::report_work, this)));
  }

  if (!command_thread) {
    command_thread = boost::shared_ptr<boost::thread>(new boost::thread(
        boost::bind(&modem_monitor_impl::command_work, this)));
  }

  return block::start();
}

bool modem_monitor_impl::stop() {
  if (command_thread) {
    command_thread->interrupt();
    command_thread->join();
    command_thread = NULL;
  }

  if (report_thread) {
    report_thread->interrupt();
    report_thread->join();
    report_thread = NULL;
  }

  return block::stop();
}

void modem_monitor_impl::report_work() {
  assert(report_rate > 0);

  while (!boost::this_thread::interruption_requested()) {
    double delay = std::fmod(get_time() * report_rate, 1.0);
    if (delay > 0.0) {
      boost::this_thread::sleep_for(
          boost::chrono::duration<double>((1.0 - delay) / report_rate));
    }

    ModemReport modem_report;
    collect_modem_report(modem_report);

    std::string msg1;
    modem_report.SerializeToString(&msg1);
    zmq::message_t msg2(msg1.size());
    std::memcpy(msg2.data(), msg1.c_str(), msg1.size());
    bool success = report_socket.send(msg2);

    if (print_mode == 1) {
      google::protobuf::TextFormat::Printer printer;
      printer.SetSingleLineMode(true);
      printer.SetUseShortRepeatedPrimitives(true);
      printer.SetUseUtf8StringEscaping(true);
      std::string printed;
      printer.PrintToString(modem_report, &printed);

      std::stringstream str;
      str << alias() << ": report " << printed << std::endl;
      std::cout << str.str();
    } else if (print_mode == 2) {
      unsigned int load = 0;
      unsigned int sent = 0;
      unsigned int drop = 0;
      unsigned int requ = 0;
      unsigned int queue = 0;
      for (const auto &flow : modem_report.modem_sender().flows()) {
        load += flow.packets_load();
        sent += flow.packets_sent();
        drop += flow.packets_dropped();
        requ += flow.packets_requeued();
        queue += flow.queued_packets();
      }

      unsigned int rcvd = 0;
      unsigned int miss = 0;
      unsigned int fail = 0;
      for (const auto &flow : modem_report.modem_receiver().flows()) {
        if (flow.crc()) {
          rcvd += flow.packets();
          miss += flow.packets_missing();
        } else
          fail += flow.packets();
      }

      const auto &equ = modem_report.preamble_equalizer();

      unsigned int serializer_frms = 0;
      unsigned int serializer_drop = 0;
      for (const auto &flow : modem_report.subcarrier_serializer().flows()) {
        serializer_frms += flow.frames();
        serializer_drop += flow.dropped();
      }

      std::stringstream str;
      str << alias() << ": time " << std::setprecision(6) << std::fixed
          << modem_report.time() << " seq " << modem_report.sequence_no()
          << " load " << load << " sent " << sent << " drop " << drop
          << " requ " << requ << " queue " << queue << " frms "
          << serializer_frms << " rcvd " << rcvd << " miss " << miss << " fail "
          << fail << " errs (" << serializer_drop << ","
          << equ.header_crc_error() << "," << equ.invalid_mcs_index() << ","
          << equ.invalid_data_len() << "," << equ.frame_too_short() << ","
          << equ.frame_too_long() << "," << equ.frame_truncated() << ")"
          << std::endl;
      std::cout << str.str();
    } else if (print_mode == 3) {
      std::stringstream str;
      str << alias() << ": time " << std::setprecision(6) << std::fixed
          << modem_report.time() << " samples "
          << modem_report.single_peak_probe().samples();

      for (const auto &carrier : modem_report.single_peak_probe().carriers())
        str << " avg " << carrier.average_db() << " max "
            << carrier.maximum_db() << " papr "
            << (carrier.maximum_db() - carrier.average_db());

      str << std::endl;
      std::cout << str.str();
    } else if (print_mode == 4) {
      unsigned int rcvd = 0;
      unsigned int fail = 0;
      for (const auto &flow : modem_report.modem_receiver().flows()) {
        if (flow.crc())
          rcvd += flow.packets();
        else
          fail += flow.packets();
      }

      const auto &equ = modem_report.preamble_equalizer();

      std::stringstream str;
      str << alias() << ": time " << std::setprecision(6) << std::fixed
          << modem_report.time() << " total " << equ.total_packets() << " crc "
          << equ.header_crc_error() << " mcs " << equ.invalid_mcs_index()
          << " len " << equ.invalid_data_len() << " short "
          << equ.frame_too_short() << " long " << equ.frame_too_long()
          << " trnc " << equ.frame_truncated() << " rcvd " << rcvd << " fail "
          << fail << std::endl;
      std::cout << str.str();
    }

    if (!success) {
      std::stringstream str;
      str << alias() << ": zmq sent failed" << std::endl;
      std::cout << str.str();
    }
  }
}

void modem_monitor_impl::command_work() {
  while (!boost::this_thread::interruption_requested()) {
    zmq::message_t msg1;
    bool valid = command_socket.recv(&msg1);
    if (!valid)
      continue;

    std::string msg2(static_cast<const char *>(msg1.data()), msg1.size());
    process_serialized_command(msg2);
  }
}

void modem_monitor_impl::register_monitored(modem_monitored *monitored) {
  if (!monitored)
    throw std::invalid_argument("modem_monitor: null pointer");

  {
    gr::thread::scoped_lock lock(monitored_mutex);
    if (std::find(monitored_list.begin(), monitored_list.end(), monitored) !=
        monitored_list.end())
      throw std::invalid_argument("modem_monitor: duplicate registering");

    monitored_list.push_back(monitored);
  }

  std::stringstream str;
  str << alias() << ": registered " << monitored->get_alias() << std::endl;
  std::cout << str.str();
}

void modem_monitor_impl::unregister_monitored(modem_monitored *monitored) {
  gr::thread::scoped_lock lock(monitored_mutex);
  std::remove(monitored_list.begin(), monitored_list.end(), monitored);
}

double modem_monitor_impl::get_time() {
  std::chrono::system_clock::duration now =
      std::chrono::system_clock::now().time_since_epoch();
  return std::chrono::duration_cast<std::chrono::duration<double>>(now).count();
}

void modem_monitor_impl::collect_modem_report(ModemReport &modem_report) {
  modem_report.set_time(get_time());
  modem_report.set_sequence_no(++sequence_no);

  /*
   * TODO: same story as below, but we protect agains ourselves. Note, that
   * we detect lockups from this thread (in peak vector probe), so if somehow
   * we get into a deadlock on this lock, then we will not restart the modem.
   */
  std::vector<modem_monitored *> list;
  {
    gr::thread::scoped_lock lock(monitored_mutex);
    list = monitored_list;
  }

  for (int i = 0; i < list.size(); i++) {
    modem_monitored *monitored = list[i];
#ifdef GR_PERFORMANCE_COUNTERS
    if (enable_perf_report)
      monitored->collect_perf_report(&modem_report);
#endif
    monitored->collect_block_report(&modem_report);
  }
}

std::string modem_monitor_impl::collect_serialized_report() {
  ModemReport modem_report;
  collect_modem_report(modem_report);

  std::string str;
  modem_report.SerializeToString(&str);
  return str;
}

void modem_monitor_impl::process_serialized_command(
    const std::string &command) {
  ModemCommand modem_command;
  if (!modem_command.ParseFromString(command)) {
    std::cout << "####### could not parse protobuf command\n";
    return;
  }

  /*
   * TODO: do something better. We might call into python from here but
   * python might try to call us (register/unreg), so we do release the
   * lock and hope that the objects do not vanish while we process them.
   * Better to crash (at unregister, which we never do) than to lock up.
   */
  std::vector<modem_monitored *> list;
  {
    gr::thread::scoped_lock lock(monitored_mutex);
    list = monitored_list;
  }

  for (int i = 0; i < list.size(); i++) {
    modem_monitored *monitored = list[i];
    monitored->process_block_command(&modem_command);
  }
}

void modem_monitored::register_monitor(const modem_monitor::sptr &monitor) {
  gr::thread::scoped_lock lock(monitor_mutex);

  if (this->monitor) {
    this->monitor->unregister_monitored(this);
    this->monitor = NULL;
  }

  if (monitor) {
    this->monitor = monitor;
    monitor->register_monitored(this);
  }
}

void modem_monitored::unregister_monitor() {
  gr::thread::scoped_lock lock(monitor_mutex);
  if (monitor) {
    monitor->unregister_monitored(this);
    monitor = NULL;
  }
}

gr::block *modem_monitored::get_this_block() { return NULL; }

std::string modem_monitored::get_alias() {
  gr::block *block = get_this_block();
  return block != NULL ? block->alias() : "unknown";
}

void modem_monitored::monitor_work() {
  gr::block *block = get_this_block();
  if (block == NULL)
    return;

  gr::block_detail_sptr detail = block->detail();
  int ninputs = detail->ninputs();
  int noutputs = detail->noutputs();

  gr::thread::scoped_lock lock(mutex);
  work_counter += 1;

  if (ninputs != input_items.size())
    input_items.resize(ninputs);

  for (int i = 0; i < ninputs; i++) {
    int num;
    {
      gr::buffer_reader_sptr reader = detail->input(i);
      boost::unique_lock<boost::mutex> guard(*reader->mutex());
      num = reader->items_available();
    }
    input_items[i].update(num);
  }

  if (noutputs != output_space.size())
    output_space.resize(noutputs);

  for (int i = 0; i < noutputs; i++) {
    int num;
    {
      gr::buffer_sptr buffer = detail->output(i);
      boost::unique_lock<boost::mutex> guard(*buffer->mutex());
      num = buffer->space_available();
    }
    output_space[i].update(num);
  }
}

void modem_monitored::collect_block_report(ModemReport *modem_report) {}

void modem_monitored::collect_perf_report(ModemReport *modem_report) {
  gr::block *block = get_this_block();
  if (block == NULL)
    return;

  PerformanceCounter *counter = modem_report->add_performance_counters();
  counter->set_alias(block->alias());

  counter->set_pc_work_time_total(block->pc_work_time_total());
  counter->set_pc_work_time_avg(block->pc_work_time_avg());
  counter->set_pc_noutput_items(block->pc_noutput_items());
  counter->set_pc_noutput_items_avg(block->pc_noutput_items_avg());
  counter->set_pc_nproduced(block->pc_nproduced());
  counter->set_pc_nproduced_avg(block->pc_nproduced_avg());

  gr::block_detail_sptr detail = block->detail();
  for (int i = 0; i < detail->ninputs(); i++) {
    counter->add_pc_input_buffers_full_avg(
        detail->pc_input_buffers_full_avg(i));
    counter->add_input_nitems_read(detail->nitems_read(i));

    gr::buffer_reader_sptr reader = detail->input(i);
    counter->add_input_buff_size(reader->max_possible_items_available() + 1);
    counter->add_input_item_size(reader->get_sizeof_item());
  }

  for (int i = 0; i < detail->noutputs(); i++) {
    counter->add_pc_output_buffers_full_avg(
        detail->pc_output_buffers_full_avg(i));
    counter->add_output_nitems_written(detail->nitems_written(i));

    gr::buffer_sptr buffer = detail->output(i);
    counter->add_output_buff_size(buffer->bufsize());
    counter->add_output_item_size(buffer->get_sizeof_item());
  }

  block->reset_perf_counters();

  gr::thread::scoped_lock lock(mutex);
  counter->set_work_counter(work_counter);
  work_counter = 0;

  for (int i = 0; i < input_items.size(); i++) {
    counter->add_input_items_min(input_items[i].min);
    counter->add_input_items_max(input_items[i].max);
    input_items[i].reset();
  }

  for (int i = 0; i < output_space.size(); i++) {
    counter->add_output_space_min(output_space[i].min);
    counter->add_output_space_max(output_space[i].max);
    input_items[i].reset();
  }
}

void modem_monitored::process_block_command(const ModemCommand *modem_command) {
}

} // namespace marmote3
} // namespace gr
