/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "header_compressor_impl.h"
#include <gnuradio/io_signature.h>
#include <marmote3/marmote3.pb.h>
#include <sstream>

namespace gr {
namespace marmote3 {

header_compressor::sptr header_compressor::make(int mode, bool print_headers) {
  return gnuradio::get_initial_sptr(
      new header_compressor_impl(mode, print_headers));
}

header_compressor_impl::header_compressor_impl(int mode, bool print_headers)
    : gr::block("header_compressor", gr::io_signature::make(0, 0, 0),
                gr::io_signature::make(0, 0, 0)),
      mode(mode), print_headers(print_headers), in_port(pmt::mp("in")),
      out_port(pmt::mp("out")) {
  if (mode < 0 || mode > 4)
    throw std::out_of_range("header_compressor: invalid mode");

  message_port_register_in(in_port);
  message_port_register_out(out_port);
  set_msg_handler(in_port,
                  boost::bind(&header_compressor_impl::receive, this, _1));
}

header_compressor_impl::~header_compressor_impl() {}

void header_compressor_impl::receive(pmt::pmt_t msg) {
  if (!pmt::is_pair(msg) || !pmt::is_u8vector(pmt::cdr(msg))) {
    std::cout << "####### header_compressor: invalid message format\n";
    return;
  }

  pmt::pmt_t meta = pmt::car(msg);
  pmt::pmt_t data = pmt::cdr(msg);
  pmt::pmt_t data2 = pmt::get_PMT_NIL();

  size_t data_len = 0;
  const uint8_t *data_ptr = pmt::u8vector_elements(data, data_len);

  mgen_seqno = -1;
  if (mode == 0)
    data2 = process_mode0(data_ptr, data_len);
  else if (mode == 1)
    data2 = process_mode1(data_ptr, data_len);
  else if (mode == 2)
    data2 = process_mode2(data_ptr, data_len);
  else if (mode == 3)
    data2 = process_mode3(data_ptr, data_len);
  else if (mode == 4)
    data2 = process_mode4(data_ptr, data_len);

  if (pmt::is_u8vector(data2)) {
    size_t data2_len = pmt::length(data2);
    {
      gr::thread::scoped_lock lock(mutex);
      valid_messages += 1;
      uncompressed_bytes += data_len;
      compressed_bytes += data2_len;
    }
    message_port_pub(out_port, pmt::cons(meta, data2));
  } else {
    gr::thread::scoped_lock lock(mutex);
    invalid_messages += 1;
  }
}

pmt::pmt_t header_compressor_impl::process_mode0(const uint8_t *data_ptr,
                                                 int data_len) {
  mmt_header_t mmt;
  int mmt_len = mmt_header_t::len();

  pmt::pmt_t data2 = pmt::make_u8vector(mmt_len + data_len, 0);
  size_t data2_len(0);
  uint8_t *data2_ptr = pmt::u8vector_writable_elements(data2, data2_len);

  mmt.save(data2_ptr);
  std::memcpy(data2_ptr + mmt_len, data_ptr, data_len);

  process_header(mmt, data_ptr, data_len);
  return data2;
}

pmt::pmt_t header_compressor_impl::process_mode1(const uint8_t *data_ptr,
                                                 int data_len) {
  const uint8_t *raw_ptr = data_ptr;
  const int raw_len = data_len;

  eth_header_t eth;
  int eth_len = eth.eth_raw_load(data_ptr, data_len);
  if (eth_len < 0)
    return pmt::get_PMT_NIL();
  data_ptr += eth_len;
  data_len -= eth_len;

  mmt_header_t mmt;
  int mmt_len = mmt_header_t::len();
  eth_len = eth.eth_zip_save1(mmt);
  assert(eth_len >= 0);

  pmt::pmt_t data2 = pmt::make_u8vector(mmt_len + eth_len + data_len, 0);
  size_t data2_len(0);
  uint8_t *data2_ptr = pmt::u8vector_writable_elements(data2, data2_len);

  mmt.save(data2_ptr);
  eth.eth_zip_save2(mmt, data2_ptr + mmt_len);
  std::memcpy(data2_ptr + mmt_len + eth_len, data_ptr, data_len);

  process_header(mmt, raw_ptr, raw_len);
  return data2;
}

pmt::pmt_t header_compressor_impl::process_mode2(const uint8_t *data_ptr,
                                                 int data_len) {
  const uint8_t *raw_ptr = data_ptr;
  const int raw_len = data_len;

  eth_header_t eth;
  int eth_len = eth.eth_raw_load(data_ptr, data_len);
  if (eth_len < 0)
    return pmt::get_PMT_NIL();
  data_ptr += eth_len;
  data_len -= eth_len;

  ip4_header_t ip4;
  int ip4_len = ip4.ip4_raw_load(data_ptr, data_len);
  if (ip4_len < 0)
    return pmt::get_PMT_NIL();
  data_ptr += ip4_len;
  data_len -= ip4_len;

  mmt_header_t mmt;
  int mmt_len = mmt_header_t::len();
  eth_len = eth.eth_zip_save1(mmt);
  assert(eth_len >= 0);
  ip4_len = ip4.ip4_zip_save1(mmt, data_len);
  assert(ip4_len >= 0);

  pmt::pmt_t data2 =
      pmt::make_u8vector(mmt_len + eth_len + ip4_len + data_len, 0);
  size_t data2_len(0);
  uint8_t *data2_ptr = pmt::u8vector_writable_elements(data2, data2_len);

  mmt.save(data2_ptr);
  eth.eth_zip_save2(mmt, data2_ptr + mmt_len);
  ip4.ip4_zip_save2(mmt, data2_ptr + mmt_len + eth_len);
  std::memcpy(data2_ptr + mmt_len + eth_len + ip4_len, data_ptr, data_len);

  process_header(mmt, raw_ptr, raw_len);
  return data2;
}

pmt::pmt_t header_compressor_impl::process_mode3(const uint8_t *data_ptr,
                                                 int data_len) {
  const uint8_t *raw_ptr = data_ptr;
  const int raw_len = data_len;

  eth_header_t eth;
  int eth_len = eth.eth_raw_load(data_ptr, data_len);
  if (eth_len < 0)
    return pmt::get_PMT_NIL();
  data_ptr += eth_len;
  data_len -= eth_len;

  ip4_header_t ip4;
  int ip4_len = ip4.ip4_raw_load(data_ptr, data_len);
  if (ip4_len < 0)
    return pmt::get_PMT_NIL();
  data_ptr += ip4_len;
  data_len -= ip4_len;

  mmt_header_t mmt;
  int mmt_len = mmt_header_t::len();
  eth_len = eth.eth_zip_save1(mmt);
  assert(eth_len >= 0);
  ip4_len = ip4.ip4_zip_save1(mmt, data_len);
  assert(ip4_len >= 0);

  udp_header_t udp;
  int udp_len = 0;
  if (ip4.prot == 0x11 && (ip4.frag & 0x1fff) == 0) {
    udp_len = udp.udp_raw_load(data_ptr, data_len);
    if (udp_len < 0)
      return pmt::get_PMT_NIL();
    data_ptr += udp_len;
    data_len -= udp_len;

    udp.udp_zip_save1(mmt, ip4, data_ptr, data_len);
    udp_len = udp_header_t::udp_zip_len(mmt);
    assert(udp_len >= 0);
  }

  tcp_header_t tcp;
  int tcp_len = 0;
  if (ip4.prot == 0x06 && (ip4.frag & 0x1fff) == 0) {
    tcp_len = tcp.tcp_raw_load(data_ptr, data_len);
    if (tcp_len < 0)
      return pmt::get_PMT_NIL();
    data_ptr += tcp_len;
    data_len -= tcp_len;

    tcp_len = tcp.tcp_zip_save1(mmt, ip4, data_ptr, data_len);
    assert(tcp_len >= 0);
  }

  assert(data_len >= 0);
  pmt::pmt_t data2 = pmt::make_u8vector(
      mmt_len + eth_len + ip4_len + udp_len + tcp_len + data_len, 0);
  size_t data2_len(0);
  uint8_t *data2_ptr = pmt::u8vector_writable_elements(data2, data2_len);

  mmt.save(data2_ptr);
  eth.eth_zip_save2(mmt, data2_ptr + mmt_len);
  ip4.ip4_zip_save2(mmt, data2_ptr + mmt_len + eth_len);
  if (ip4.prot == 0x11 && (ip4.frag & 0x1fff) == 0)
    udp.udp_zip_save2(mmt, data2_ptr + mmt_len + eth_len + ip4_len);
  if (ip4.prot == 0x06 && (ip4.frag & 0x1fff) == 0)
    tcp.tcp_zip_save2(mmt, data2_ptr + mmt_len + eth_len + ip4_len);
  std::memcpy(data2_ptr + mmt_len + eth_len + ip4_len + udp_len + tcp_len,
              data_ptr, data_len);

  process_header(mmt, raw_ptr, raw_len);
  return data2;
}

pmt::pmt_t header_compressor_impl::process_mode4(const uint8_t *data_ptr,
                                                 int data_len) {
  const uint8_t *raw_ptr = data_ptr;
  const int raw_len = data_len;

  eth_header_t eth;
  const int eth_raw_len = eth.eth_raw_load(data_ptr, data_len);
  if (eth_raw_len < 0)
    return pmt::get_PMT_NIL();
  data_ptr += eth_raw_len;
  data_len -= eth_raw_len;
  assert(data_len >= 0);

  ip4_header_t ip4;
  const int ip4_raw_len = ip4.ip4_raw_load(data_ptr, data_len);
  if (ip4_raw_len < 0)
    return pmt::get_PMT_NIL();
  data_ptr += ip4_raw_len;
  data_len -= ip4_raw_len;
  assert(data_len >= 0);

  mmt_header_t mmt;
  const int mmt_zip_len = mmt_header_t::len();

  const int eth_zip_len = eth.eth_zip_save1(mmt);
  assert(eth_zip_len >= 0);

  const int ip4_zip_len = ip4.ip4_zip_save1(mmt, data_len);
  assert(ip4_zip_len >= 0);

  udp_header_t udp;
  int udp_zip_len = 0;
  if (ip4.prot == 0x11 && (ip4.frag & 0x1fff) == 0) {
    const int udp_raw_len = udp.udp_raw_load(data_ptr, data_len);
    if (udp_raw_len < 0)
      return pmt::get_PMT_NIL();
    data_ptr += udp_raw_len;
    data_len -= udp_raw_len;
    assert(data_len >= 0);

    udp.udp_zip_save1(mmt, ip4, data_ptr, data_len);
    udp_zip_len = udp_header_t::udp_zip_len(mmt);
    assert(udp_zip_len >= 0);
  }

  tcp_header_t tcp;
  int tcp_zip_len = 0;
  if (ip4.prot == 0x06 && (ip4.frag & 0x1fff) == 0) {
    const int tcp_raw_len = tcp.tcp_raw_load(data_ptr, data_len);
    if (tcp_raw_len < 0)
      return pmt::get_PMT_NIL();
    data_ptr += tcp_raw_len;
    data_len -= tcp_raw_len;
    assert(data_len >= 0);

    tcp_zip_len = tcp.tcp_zip_save1(mmt, ip4, data_ptr, data_len);
    assert(tcp_zip_len >= 0);
  }

  mgn_header_t mgn;
  const int mgn_raw_len = mgn.mgn_raw_load(ip4, mmt.flowid, data_ptr, data_len);
  assert(mgn_raw_len >= 0);
  data_ptr += mgn_raw_len;
  data_len -= mgn_raw_len;
  assert(data_len >= 0);

  const int mgn_zip_len = mgn.mgn_zip_save1(mmt, ip4);
  assert(mgn_zip_len >= 0);

  pmt::pmt_t data2 =
      pmt::make_u8vector(mmt_zip_len + eth_zip_len + ip4_zip_len + udp_zip_len +
                             tcp_zip_len + mgn_zip_len + data_len,
                         0);
  size_t data2_len(0);
  uint8_t *data2_ptr = pmt::u8vector_writable_elements(data2, data2_len);

  mmt.save(data2_ptr);
  eth.eth_zip_save2(mmt, data2_ptr + mmt_zip_len);
  ip4.ip4_zip_save2(mmt, data2_ptr + mmt_zip_len + eth_zip_len);
  if (ip4.prot == 0x11 && (ip4.frag & 0x1fff) == 0)
    udp.udp_zip_save2(mmt, data2_ptr + mmt_zip_len + eth_zip_len + ip4_zip_len);
  if (ip4.prot == 0x06 && (ip4.frag & 0x1fff) == 0)
    tcp.tcp_zip_save2(mmt, data2_ptr + mmt_zip_len + eth_zip_len + ip4_zip_len);
  mgn.mgn_zip_save2(mmt, data2_ptr + mmt_zip_len + eth_zip_len + ip4_zip_len +
                             udp_zip_len + tcp_zip_len);
  std::memcpy(data2_ptr + mmt_zip_len + eth_zip_len + ip4_zip_len +
                  udp_zip_len + tcp_zip_len + mgn_zip_len,
              data_ptr, data_len);

  if (mgn_raw_len > 0)
    mgen_seqno = mgn.mgenSeqno;
  process_header(mmt, raw_ptr, raw_len);
  return data2;
}

void header_compressor_impl::process_header(const mmt_header_t &mmt,
                                            const uint8_t *raw_ptr,
                                            int raw_len) {
  if (print_headers && observed_headers.insert(mmt).second) {
    std::stringstream str;
    str << alias() << ": new mmt " << mmt << " seqno " << mgen_seqno
        << " packet 0x" << std::right << std::setfill('0') << std::hex;
    for (int i = 0; i < raw_len; i++)
      str << std::setw(2) << (unsigned int)raw_ptr[i];
    str << std::endl;
    std::cout << str.str();
  }
}

void header_compressor_impl::collect_block_report(ModemReport *modem_report) {
  HeaderCompression *report = modem_report->mutable_header_compressor();
  gr::thread::scoped_lock lock(mutex);

  report->set_valid_messages(valid_messages);
  report->set_invalid_messages(invalid_messages);
  report->set_uncompressed_bytes(uncompressed_bytes);
  report->set_compressed_bytes(compressed_bytes);

  valid_messages = 0;
  invalid_messages = 0;
  uncompressed_bytes = 0;
  compressed_bytes = 0;
}

} /* namespace marmote3 */
} /* namespace gr */
