/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "block_sum_vxx_impl.h"
#include <gnuradio/io_signature.h>
#include <volk/volk.h>

namespace gr {
namespace marmote3 {

block_sum_vxx::sptr block_sum_vxx::make(int width, int decim, int length) {
  return gnuradio::get_initial_sptr(
      new block_sum_vxx_impl(width, decim, length));
}

block_sum_vxx_impl::block_sum_vxx_impl(int width, int decim, int length)
    : gr::sync_decimator(
          "block_sum_vxx", gr::io_signature::make(1, 1, sizeof(float) * width),
          gr::io_signature::make(1, 1, sizeof(float) * width), decim),
      width(width), decim(decim), length(length) {
  if (width <= 0)
    throw std::invalid_argument("block_sum_vxx: block width must be positive");

  if (decim <= 0)
    throw std::invalid_argument("block_sum_vxx: decimation must be positive");

  if (length <= 0)
    throw std::invalid_argument("block_sum_vxx: block length must be positive");

  // if decimation is larger than length then we need no history
  set_history(std::max(length - decim + 1, 1));
}

block_sum_vxx_impl::~block_sum_vxx_impl() {}

int block_sum_vxx_impl::work(int noutput_items,
                             gr_vector_const_void_star &input_items,
                             gr_vector_void_star &output_items) {
  const float *in = (const float *)input_items[0];
  float *out = (float *)output_items[0];

  for (int n = 0; n < noutput_items; n++) {
    if (length <= 1)
      std::memcpy(out, in, sizeof(float) * width);
    else {
      volk_32f_x2_add_32f(out, in, in + width, width);
      const float *in2 = in + 2 * width;
      for (int i = 2; i < length; i++) {
        volk_32f_x2_add_32f(out, out, in2, width);
        in2 += width;
      }
    }
    in += width * decim;
    out += width;
  }

  return noutput_items;
}

} /* namespace marmote3 */
} /* namespace gr */
